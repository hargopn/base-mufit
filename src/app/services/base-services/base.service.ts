import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { AppError } from '../../pages/common/app-error';
import { NotFoundError } from '../../pages/common/not-found-error';
import { BadInput } from '../../pages/common/bad-input';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';
import {LocalStorageService, SessionStorageService} from 'ngx-webstorage';

@Injectable()
export class BaseService {

	constructor(private http: Http, private localStorageService:LocalStorageService) { }
		getData(url) {
		    return this.http.get(url)
		      	.map(response => response.json())
		      	.catch(this.handleError);
		  }
		getDataParam(url,params) {
		    return this.http.get(url,{ params: params })
		      	.map(response => response.json())
		      	.catch(this.handleError);
		  }
		createData(url,data){
			return this.http.post(url,data)
				.map(response => response.json())
		      	.catch(this.handleError)
		}
		createDataParam(url,data,params){
			return this.http.post(url,data,{params:params})
				.map(response => response.json())
		      	.catch(this.handleError)
		}
		updateData(url,data){
			return this.http.put(url+'/'+data.id,data)
				.map(response => response.json())
		      	.catch(this.handleError)
		}
		updateDataParam(url,data,id,params){
			return this.http.put(url,data,{params:params})
				.map(response => response.json())
		      	.catch(this.handleError)
		}
		deleteData(url,data){
			return this.http.delete(url + '/' + data)
		      	.map(response => response.json())
		      	.catch(this.handleError)
		}
		deleteDataParam(url,params){
			return this.http.delete(url , {params:params})
		      	.map(response => response.json())
		      	.catch(this.handleError)
		}
		getToken(){
			return this.localStorageService.retrieve('token');
		}

	
	  	private handleError(error: Response) {
	    if (error.status === 400) {
	    	return Observable.throw(new BadInput(error.json()))
		}
		
		if (error.status === 401) {
			localStorage.clear();
	    	location.reload();
	    }

	if (error.status === 404) {
			return Observable.throw(new NotFoundError());
	    }
	    	return Observable.throw(new AppError(error));
	  }

}
