export class Configuration {
  // ini untuk dev
  // public static get BASE_URL(): string { return "http://dev.nostratech.com:20024/api/"; };
  //ini untuk sit
  // public static get BASE_URL(): string { return "http://dev.nostratech.com:20025/api/"; };
  public static get BASE_URL(): string { return "https://api.mufit.id/api/"; };
  //public static get BASE_URL(): string { return "http://dev.nostratech.com:20023/api/"; };
  public static get REGISTRATION(): string { return "client-registration/submit-client"; };
  public static get SENDEMAIL(): string { return "client-registration/send-email"; };
  public static get ACTIVATECLIENT(): string { return "client-registration/activate-client"; };
  public static get REGISTRATION_CONFIRMATION(): string { return "registration/confirmation"; };
  public static get SENDEMAILFORGOT(): string { return "forgot-password/send-email"; };
  public static get CHANGEFORGOTPASSWORD(): string { return "forgot-password/"; };
  public static get LOGIN(): string { return "auth/login"; };
  public static get LOGOUT(): string { return "auth/logout"; };
  public static get PERSON(): string { return "person"; };
  public static get SPECIALITY(): string { return "speciality/list-of-speciality"; };
  public static get URL_CREATE_TRAINER(): string { return "trainer/create-trainer"; };
  public static get DETAIL_USER(): string { return "profiles/get-detail-customer";};
  public static get TRAINER(): string { return "trainer/create-trainer";};
  public static get TRAINER_LIST(): string { return "trainer/get-list"; };
  public static get TRAINER_DETAIL(): string { return "trainer/get-detail"; };
  public static get DETAIL_TRAINER(): string{ return "trainer/get-detail";};
  public static get LIST_USER(): string { return "profiles/get-list-customer"; };
  public static get UPDATE_TRAINER(): string { return "trainer/update-trainer"; };
  public static get DETAIL_BOOKING(): string {return "booking/get-detail-booking"};
  public static get CREATE_ADMIN(): string {return "profiles/create-admin"};
  public static get EDIT_BOOKING(): string { return "booking/edit-booking" };
  public static get DELETE_BOOKING(): string { return "booking/delete-booking" };
  public static get LIST_SHIFT(): string { return "trainer/get-shift-trainer" };
  public static get LIST_BOOKING(): string { return "booking/get-list-booking" };
  public static get CREATE_BOOKING(): string { return "booking/create-booking" };
  public static get GET_ADVANCED_LIST(): string { return "trainer/get-advanced-list" };
  public static get URL_GMAPS(): string { return "https://maps.google.com/maps/api/geocode/json"; };
  public static get ADMIN_DETAIL(): string { return "profiles/get-detail-admin"; };
  public static get UPDATE_ADMIN(): string { return "profiles/update-admin"; };
  public static get LIST_ADMIN(): string { return "profiles/get-list-admin" };
  public static get RATING(): string { return "rating/approval-rating" };
  public static get LIST_RATING(): string { return "rating/get-list-rating"}
  public static get UPLOAD_IMG(): string { return "image/upload"; };
  public static get UPDATE_USER(): string { return "profiles/update-customer" };
  public static get GET_BANK(): string { return "bank/get-list"; };
  public static get VERIFIKASI_PEMBAYARAN(): string { return "booking/verification-payment-by-admin"; };
  public static get CREATE_USER(): string { return "profiles/create-customer"; };
  public static get CREATE_COVER(): string { return "cover/create-cover" };
  public static get LIST_COVER(): string { return "cover/get-list-cover" };
  public static get DELETE_COVER(): string { return "cover/delete-cover" };
  public static get CHANGE_PASS(): string { return "forgot-password/change-password"};
  public static get ADD_SPECIALITY(): string { return "speciality/create-speciality"};
  public static get DELETE_SPECIALITY(): string { return "speciality/delete-speciality"};
  public static get APROVAL_RATING(): string { return "rating/approval-rating"}
  // public static get GET_BERANDA(): string { return ""; };
  public static get GET_DETAIL_SHIFT(): string { return "trainer/get-detail-shift" }
  public static get ADD_RATING(): string { return "rating/add-rating" }
  public static get CREATE_ADS(): string { return "media/create-media" };
  public static get CREATE_VOUCHER(): string {return "voucher/add-voucher"}; 
  public static get LIST_VOUCHER(): string {return "voucher/get-list-voucher"};  
  public static get DETAIL_VOUCHER(): string {return "voucher/get-detail-voucher"};
  public static get DELETE_VOUCHER(): string {return "voucher/delete-voucher"};  
  public static get EDIT_VOUCHER(): string {return "voucher/edit-voucher"};  
  public static get CHECK_VOUCHER(): string {return "voucher/check-voucher"};    
  
    
   
}
