import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { BaseService } from './base-services/base.service';
import { Configuration } from './base-services/configuration';
import { AuthService } from './base-services/auth.service';
import {LocalStorageService, SessionStorageService} from 'ngx-webstorage';

@Injectable()
export class MapsService extends BaseService {

  constructor(http: Http, auth: AuthService,localStorageService:LocalStorageService) { 
    super(http,localStorageService);
    auth.isAuthenticated();
  }

  getLocation(data1, data2){
    let request = {"latlng": data1 + "," + data2,"key":'AIzaSyDGN4BvluiHX9wrr2UkUjXd0thl-mJScKk'}
    return this.getDataParam(Configuration.URL_GMAPS, request);
  }
}
