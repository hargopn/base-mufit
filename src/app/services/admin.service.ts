import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { BaseService } from './base-services/base.service';
import { Configuration } from './base-services/configuration';
import { AuthService } from './base-services/auth.service';
import { LocalStorageService, SessionStorageService } from 'ngx-webstorage';

@Injectable()
export class AdminService extends BaseService {

  constructor(http: Http, auth: AuthService, localStorageService: LocalStorageService) {
    super(http, localStorageService);
    auth.isAuthenticated();
  }

  getPaginate(data1, data2, data3) {
    let token = this.getToken();
    let request = { "access_token": token, "page": data1, "limit": data2, "search": data3 }
    return this.getDataParam(Configuration.BASE_URL + Configuration.LIST_ADMIN, request);
  }

  getAdminData(id) {
    let token = this.getToken();
    let request = { "access_token": token, "id": id }
    return this.getDataParam(Configuration.BASE_URL + Configuration.ADMIN_DETAIL, request);
  }

  updateAdmin(data, id) {
    let token = this.getToken();
    let request = { "access_token": token }
    return this.updateDataParam(Configuration.BASE_URL + Configuration.UPDATE_ADMIN, data, id, request)
  }
  addAdmin(data) {
    let token = this.getToken();
    let request = { "access_token": token }
    return this.createDataParam(Configuration.BASE_URL + Configuration.CREATE_ADMIN, data, request);
  }

  uploadImg(data){
    let token = this.getToken();
    let request = { "type": "trainer" }
    return this.createDataParam(Configuration.BASE_URL + Configuration.UPLOAD_IMG,data,request)
  }

  addCover(data) {
    let token = this.getToken();
    let request = { "access_token": token }
    return this.createDataParam(Configuration.BASE_URL + Configuration.CREATE_COVER, data, request);
  }

  getListCover(data1, data2, data3) {
    let token = this.getToken();
    let request = { "access_token": token, "page": data1, "limit": data2, "search": data3 }
    return this.getDataParam(Configuration.BASE_URL + Configuration.LIST_COVER, request);
  }

  deleteCover(id){
    let token = this.getToken();
    let request = { "access_token": token, "id": id }
    return this.deleteDataParam(Configuration.BASE_URL + Configuration.DELETE_COVER, request);
  }

  addAds(data) {
    let token = this.getToken();
    let request = { "access_token": token }
    return this.createDataParam(Configuration.BASE_URL + Configuration.CREATE_ADS, data, request);
  }

}
