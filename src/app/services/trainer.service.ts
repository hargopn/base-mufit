import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { BaseService } from './base-services/base.service';
import { Configuration } from './base-services/configuration';
import { AuthService } from './base-services/auth.service';
import { LocalStorageService, SessionStorageService } from 'ngx-webstorage';
@Injectable()
export class TrainerService extends BaseService {

    constructor(http: Http, auth: AuthService, localStorageService: LocalStorageService) {
        super(http, localStorageService);
        auth.isAuthenticated();
    }

    addContact(data) {
        return this.createData(Configuration.BASE_URL, data);
    }
    getSpecialities() {
        let token = this.getToken();
        let request = { "access_token": token }
        return this.getDataParam(Configuration.BASE_URL + Configuration.SPECIALITY, request);
    }
    getBank() {
        let token = this.getToken();
        let request = { "access_token": token }
        return this.getDataParam(Configuration.BASE_URL + Configuration.GET_BANK, request);
    }
    getDetailTrainer(data) {
        let token = this.getToken();
        let request = { "access_token": token,"id":data }
        return this.getDataParam(Configuration.BASE_URL + Configuration.TRAINER_DETAIL, request);
    }
    addTrainer(data) {
        let token = this.getToken();
        let request = { "access_token": token }
        return this.createDataParam(Configuration.BASE_URL + Configuration.URL_CREATE_TRAINER, data, request);
    }

    getPaginate(data1, data2, data3) {
        let token = this.getToken();
        let request = {"access_token":token,"page":data1,"limit":data2, "search":data3}
        return this.getDataParam(Configuration.BASE_URL + Configuration.TRAINER_LIST, request);
    }

    getTrainerById(id){
      let token = this.getToken();
      let request= {"access_token": token, "id": id}
      return this.getDataParam(Configuration.BASE_URL + Configuration.TRAINER_DETAIL, request);
    }

    updateTrainer( data,  id){
      let token = this.getToken();
      let request = {"access_token": token}
      return this.updateDataParam(Configuration.BASE_URL + Configuration.UPDATE_TRAINER,data,id, request)
    }

    updateTrainerX(data){
      let token = this.getToken();
      let request = {"access_token": token}
      return this.updateData(Configuration.BASE_URL + Configuration.UPDATE_TRAINER,  request);
    }
    getShiftTrainer() {
        let token = this.getToken();
        let request = { "access_token": token}
        return this.getDataParam(Configuration.BASE_URL + Configuration.LIST_SHIFT, request);
    }
    uploadImg(data){
      let token = this.getToken();
      let request = { "type": "trainer" }
      return this.createDataParam(Configuration.BASE_URL + Configuration.UPLOAD_IMG,data,request)
    }

    getDetailShiftTrainer(id, day, book_date){
        let token = this.getToken();
        let request= { "access_token": token, "id": id, "day": day, "book_date": book_date }
        return this.getDataParam(Configuration.BASE_URL + Configuration.GET_DETAIL_SHIFT, request);
    }


}
