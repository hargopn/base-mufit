import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { BaseService } from './base-services/base.service';
import { Configuration } from './base-services/configuration';
import { AuthService } from './base-services/auth.service';
import {LocalStorageService, SessionStorageService} from 'ngx-webstorage';
@Injectable()
export class SpecialityService extends BaseService {

  constructor(http: Http, auth: AuthService, localStorageService: LocalStorageService) {
      super(http, localStorageService);
      auth.isAuthenticated();
  }

  getListSpeciality(){
    let token = this.getToken();
    let request = { "access_token": token }
    return this.getDataParam(Configuration.BASE_URL + Configuration.SPECIALITY, request);
  }

  addSpeciality(data){
    let token = this.getToken();
    let request = { "access_token": token}
    return this.createDataParam(Configuration.BASE_URL + Configuration.ADD_SPECIALITY,data, request);
  }

  uploadImg(data){
    let token = this.getToken();
    let request = { "type": "dashboard" }
    return this.createDataParam(Configuration.BASE_URL + Configuration.UPLOAD_IMG,data,request)
  }
  deleteSpeciality(id, force){
    let token = this.getToken();
    let request = { "access_token": token, "id": id, "force": force}
    return this.deleteDataParam(Configuration.BASE_URL + Configuration.DELETE_SPECIALITY,request);
  }

  // deleteSpecialityX(id){
  //   let token = this.getToken();
  //   let request = { "access_token": token, "id": id, "result": "true" }
  //   return this.deleteDataParam(Configuration.BASE_URL + Configuration.DELETE_SPECIALITY, request)
  // }
}
