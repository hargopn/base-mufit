import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { BaseService } from './base-services/base.service';
import { Configuration } from './base-services/configuration';
import { AuthService } from './base-services/auth.service';
import {LocalStorageService, SessionStorageService} from 'ngx-webstorage';

@Injectable()
export class AuthenticationService extends BaseService {

	constructor(http: Http, auth: AuthService,localStorageService:LocalStorageService) {
    	super(http,localStorageService);
    	auth.isAuthenticated();
    }
    
    createClient(data) {
        return this.createData(Configuration.BASE_URL + Configuration.REGISTRATION, data);
    };

    sendEmail(id) {
        return this.getData(Configuration.BASE_URL + Configuration.SENDEMAIL + "?id=" + id);
    }

    activateAcc(url) {
        return this.getData(Configuration.BASE_URL + Configuration.ACTIVATECLIENT + "/" + url);
    }

    registrationAcc(data) {
        return this.createData(Configuration.BASE_URL + Configuration.REGISTRATION_CONFIRMATION , data);
    }

    login(data) {
        return this.createData(Configuration.BASE_URL + Configuration.LOGIN, data);
    }
    logout(data) {
        return this.createData(Configuration.BASE_URL + Configuration.LOGOUT, data);
    }

    sendEmailForgot(data) {
        return this.createData(Configuration.BASE_URL + Configuration.SENDEMAILFORGOT, data);
    }

    changePassword(data) {
        return this.createData(Configuration.BASE_URL + Configuration.CHANGE_PASS, data);
    }
}
