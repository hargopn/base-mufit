import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { BaseService } from './base-services/base.service';
import { Configuration } from './base-services/configuration';
import { AuthService } from './base-services/auth.service';
import { LocalStorageService, SessionStorageService } from 'ngx-webstorage';

@Injectable()
export class BookingService extends BaseService {

  constructor(http: Http, auth: AuthService, localStorageService: LocalStorageService) {
    super(http, localStorageService);
    auth.isAuthenticated();
  }

  getPaginate(data1, data2, data3) {
    let token = this.getToken();
    let request = { "access_token": token, "page": data1, "limit": data2, "search": data3 }
    return this.getDataParam(Configuration.BASE_URL + Configuration.LIST_BOOKING, request);
  }
  getDetailBooking(id) {
    let token = this.getToken();
    let request = { "access_token": token, "id": id }
    return this.getDataParam(Configuration.BASE_URL + Configuration.DETAIL_BOOKING, request);
  }

  editBooking(data) {
    let token = this.getToken();
    let request = { "access_token": token }
    return this.createDataParam(Configuration.BASE_URL + Configuration.EDIT_BOOKING, data, request);
  }

  deleteBooking(id){
    let token = this.getToken();
    let request = { "access_token": token, "id": id }
    return this.deleteDataParam(Configuration.BASE_URL + Configuration.DELETE_BOOKING,request);
  }

  createBooking(data) {
    let token = this.getToken();
    let request = { "access_token": token }
    return this.createDataParam(Configuration.BASE_URL + Configuration.CREATE_BOOKING, data, request);
  }

  getListForBooking(page, limit, day, speciality, gender, price_min, price_max, time_start, time_end, date){
    let token = this.getToken();
    let request = { "access_token": token, "page":page, "limit":limit, "day":day, "speciality":speciality, "gender":gender, "price_min":price_min, "price_max":price_max, "time_start":time_start, "time_end":time_end, "book_date":date }
    return this.getDataParam(Configuration.BASE_URL + Configuration.GET_ADVANCED_LIST, request);
  }

  createVerifikasiPembayaran(data) {
    let token = this.getToken();
    let request = { "access_token": token }
    return this.createDataParam(Configuration.BASE_URL + Configuration.VERIFIKASI_PEMBAYARAN, data, request);
  }
  submitRating(data) {
    let token = this.getToken();
    let request = { "access_token": token }
    return this.createDataParam(Configuration.BASE_URL + Configuration.ADD_RATING, data, request);
  }

}
