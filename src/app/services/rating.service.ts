import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { BaseService } from './base-services/base.service';
import { Configuration } from './base-services/configuration';
import { AuthService } from './base-services/auth.service';
import { LocalStorageService, SessionStorageService } from 'ngx-webstorage';
@Injectable()
export class RatingService extends BaseService {

  constructor(http: Http, auth: AuthService, localStorageService: LocalStorageService) {
    super(http, localStorageService);
    auth.isAuthenticated();
  }

  getPaginate(status) {
    let token = this.getToken();
    let request = { "access_token": token, "status": status }
    return this.getDataParam(Configuration.BASE_URL + Configuration.LIST_RATING, request);
  }

  approvalRating( id, status){
    let token = this.getToken();
    let request = { "access_token": token,"id": id, "status": status }
    return this.getDataParam(Configuration.BASE_URL + Configuration.APROVAL_RATING, request);
  }
}
