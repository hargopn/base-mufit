import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { BaseService } from './base-services/base.service';
import { Configuration } from './base-services/configuration';
import { AuthService } from './base-services/auth.service';
import {LocalStorageService, SessionStorageService} from 'ngx-webstorage';
@Injectable()
export class UserService extends BaseService {

	constructor(http: Http, auth: AuthService,localStorageService:LocalStorageService) {
    	super(http,localStorageService);
    	auth.isAuthenticated();
	}

	addUser(data) {
    let token = this.getToken();
    let request = { "access_token": token }
    return this.createDataParam(Configuration.BASE_URL + Configuration.CREATE_USER, data, request);
  }

  getUserData(data1){
    let token = this.getToken();
    let request = {"access_token":token,"id":data1}
    return this.getDataParam(Configuration.BASE_URL + Configuration.DETAIL_USER, request);
}
  getPaginate(data1, data2, data3){
    let token = this.getToken();
    let request = {"access_token":token,"page":data1,"limit":data2,"search":data3}
    return this.getDataParam(Configuration.BASE_URL + Configuration.LIST_USER, request);

  }

	uploadImg(data){
    let token = this.getToken();
    let request = { "type": "trainer" }
    return this.createDataParam(Configuration.BASE_URL + Configuration.UPLOAD_IMG,data,request)
	}
  updateUser(data, id){
    let token = this.getToken();
    let request = {"access_token": token}
    return this.updateDataParam(Configuration.BASE_URL + Configuration.UPDATE_USER, data, id, request)
  }
}
