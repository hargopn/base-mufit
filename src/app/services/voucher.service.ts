import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { BaseService } from './base-services/base.service';
import { Configuration } from './base-services/configuration';
import { AuthService } from './base-services/auth.service';
import { LocalStorageService, SessionStorageService } from 'ngx-webstorage';
@Injectable()
export class VoucherService extends BaseService {

    constructor(http: Http, auth: AuthService, localStorageService: LocalStorageService) {
        super(http, localStorageService);
        auth.isAuthenticated();
    }

    addVoucher(data) {
        let token = this.getToken();
        let request = { "access_token": token }
        return this.createDataParam(Configuration.BASE_URL + Configuration.CREATE_VOUCHER, data, request);
    }
    getList(data1, data2, data3, data4) {
        let token = this.getToken();
        let request = { "access_token": token, "page": data1, "limit": data2, "type": data3, "code":data4 }
        return this.getDataParam(Configuration.BASE_URL + Configuration.LIST_VOUCHER, request);
    }
    getDetailVoucher(id) {
        let token = this.getToken();
        let request = { "access_token": token, "id": id }
        return this.getDataParam(Configuration.BASE_URL + Configuration.DETAIL_VOUCHER, request);
    }
    checkVoucher(code) {
        let token = this.getToken();
        let request = { "access_token": token, "code": code}
        return this.getDataParam(Configuration.BASE_URL + Configuration.CHECK_VOUCHER, request);
    }
    editVoucher(data, id){
        let token = this.getToken();
        let request = {"access_token": token}
        return this.updateDataParam(Configuration.BASE_URL + Configuration.EDIT_VOUCHER, data, id, request)
      }
    deleteVoucher(id) {
        let token = this.getToken();
        let request = { "access_token": token, "id": id }
        return this.deleteDataParam(Configuration.BASE_URL + Configuration.DELETE_VOUCHER, request);
    }

}