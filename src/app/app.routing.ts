import {Routes,CanActivate} from '@angular/router';
import {AdminComponent} from './layout/admin/admin.component';
import {AuthComponent} from './layout/auth/auth.component';
import {AuthGuardService as AuthGuard } from './services/base-services/auth-guard.service';
export const AppRoutes: Routes = [ {
    path: '',
    component: AuthComponent,
    children: [
      // {
      //   path: 'authentication',
      //   loadChildren: './pages/authentication/authentication.module#AuthenticationModule'
      // },
      {
        path: '',
        redirectTo: 'login',
        pathMatch: 'full'
      },
      {
        path: 'maintenance/offline-ui',
        loadChildren: './pages/maintenance/offline-ui/offline-ui.module#OfflineUiModule'
      }, {
        path: 'login',
        loadChildren: './pages/authentication/login/login.module#LoginModule'
      }, {
        path: 'forgot-password/:url_code',
        loadChildren: './pages/authentication/change-password/change-password.module#ChangePasswordModule'
      }, {
        path: 'success-change-password',
        loadChildren: './pages/authentication/success-change-password/success-change-password.module#SuccessChangePasswordModule'
      }, {
        path: 'activation-account/:url_code',
        loadChildren: './pages/authentication/activation-account/activation-account.module#ActivationAccountModule'
      }
    ]
  },
  {
    path: '',
    component: AdminComponent,
    children: [ {
        path: 'animations',
        loadChildren: './pages/animations/animations.module#AnimationsModule'
      }, {
        path: 'maintenance/error',
        loadChildren: './pages/maintenance/error/error.module#ErrorModule'
      }, {
        path: 'maintenance/coming-soon',
        loadChildren: './pages/maintenance/coming-soon/coming-soon.module#ComingSoonModule'
      }, {
        path: 'dashboard',
        loadChildren: './pages/beranda/beranda.module#BerandaModule',
        canActivate : [AuthGuard]
      }, {
        path: 'role',
        loadChildren: './pages/trainer/role.module#RoleModule',
        canActivate : [AuthGuard]
      }, {
        path: 'trainer',
        loadChildren: './pages/trainer/trainer.module#TrainerModule',
        canActivate : [AuthGuard]
      }, {
        path: 'booking',
        loadChildren: './pages/booking/booking.module#BookingModule',
        canActivate : [AuthGuard]
      }, {
        path: 'content',
        loadChildren: './pages/content/content.module#ContentModule',
        canActivate : [AuthGuard]
      }, {
        path: 'users',
        loadChildren: './pages/users/users.module#UsersModule',
        canActivate : [AuthGuard]
      }, {
        path: 'admin',
        loadChildren: './pages/admin/admin.module#AdminModule',
        canActivate: [AuthGuard]
      }, {
        path: 'speciality',
        loadChildren: './pages/speciality/speciality.module#SpecialityModule',
        canActivate: [AuthGuard]
      }, {
        path: 'voucher',
        loadChildren: './pages/voucher/voucher.module#VoucherModule',
        canActivate: [AuthGuard]
      }
    ]
  }
];
