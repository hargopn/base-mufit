import {Injectable} from '@angular/core';

export interface BadgeItem {
  type: string;
  value: string;
}

export interface ChildrenItems {
  state: string;
  target?: boolean;
  name: string;
  type?: string;
  children?: ChildrenItems[];
}

export interface MainMenuItems {
  state: string;
  short_label?: string;
  main_state?: string;
  target?: boolean;
  name: string;
  type: string;
  icon: string;
  badge?: BadgeItem[];
  children?: ChildrenItems[];
}

export interface Menu {
  label: string;
  main: MainMenuItems[];
}

const MENUITEMS = [

  {
    label: 'Admin Dashboard',
    main: [
      {
        state: 'dashboard',
        sub_state:null,
        short_label: 'F',
        name: 'Dashboard',
        type: 'link',
        icon: 'ti-home'
      },
      {
        state: 'booking',
        sub_state:'list',
        short_label: 'F',
        name: 'Bookings',
        type: 'link',
        icon: 'ti-menu-alt'
      },
      {
        state: 'voucher',
        sub_state: 'list',
        short_label: 'F',
        name: 'Payment',
        type: 'link',
        icon: 'ti-ticket'
      },
      {
        state: 'trainer',
        sub_state:'list',
        short_label: 'F',
        name: 'Professionals',
        type: 'link',
        icon: 'ti-crown'
      },
      {
        state: 'admin',
        sub_state: 'list',
        short_label: 'F',
        name: 'Establishments',
        type: 'link',
        icon: 'ti-id-badge'
      },
      {
        state: 'trainer',
        sub_state:'rating-review',
        short_label: 'F',
        name: ' Rating & Reviews ',
        type: 'link',
        icon: 'ti-star'
      },
      {
        state: 'users',
        sub_state:'list',
        short_label: 'F',
        name: 'Users',
        type: 'link',
        icon: 'ti-user'
      },
      {
        state: 'role',
        sub_state:'add',
        short_label: 'F',
        name: 'Admin & Roles',
        type: 'link',
        icon: 'ti-layers'
      },
      // {
      //   state: 'content',
      //   sub_state:'list',
      //   short_label: 'F',
      //   name: 'Content Maker',
      //   type: 'link',
      //   icon: 'ti-pencil'
      // },
      
      
      
      // {
      //   state: 'speciality',
      //   sub_state: 'list',
      //   short_label: 'F',
      //   name: 'Trainer Speciality',
      //   type: 'link',
      //   icon: 'ti-medall'
      // },
      
    ]
  }
];

@Injectable()
export class MenuItems {
  getAll(): Menu[] {
    return MENUITEMS;
  }

  /*add(menu: Menu) {
    MENUITEMS.push(menu);
  }*/
}
