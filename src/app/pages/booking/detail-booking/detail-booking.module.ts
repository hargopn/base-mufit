import { AdminService } from './../../../services/admin.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {SharedModule} from '../../../shared/shared.module';
import { MapsService } from './../../../services/maps.service';
import {DataTableModule} from 'angular2-datatable';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { DetailBookingComponent } from './detail-booking.component';
import { TrainerService } from '../../../services/trainer.service';
import { AgmCoreModule } from '@agm/core';
import { FormWizardModule } from 'angular2-wizard';




export const berandaRoutes: Routes = [
  {
    path: '',
    component: DetailBookingComponent,
      data: {
    breadcrumb: 'Booking Edit',
    icon: 'icofont-social-blogger bg-c-green',
      status: false
  } }
];



@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(berandaRoutes),
    SharedModule,
    FormsModule,
    FormWizardModule,
    DataTableModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCSZRlW4nBzGe7oGUH5UCun8aXSyJc3d4E'
    }),
    ReactiveFormsModule
  ],
  declarations: [DetailBookingComponent],
  providers: [TrainerService,MapsService, AdminService]
})


export class DetailBookingModule {}
