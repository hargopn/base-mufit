import { AdminService } from './../../../services/admin.service';
import { BookingService } from './../../../services/booking.service';
import { MapsService } from './../../../services/maps.service';
import { Detail } from './../../../class/detail';
import { Trainer } from './../../../class/trainer';
import { TrainerService } from './../../../services/trainer.service';
import { Component, OnInit, Input,ViewEncapsulation  } from '@angular/core';
import { NgbDatepickerConfig, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { ActivatedRoute } from '@angular/router';
import { Booking } from './../../../class/booking';
import { Router } from '@angular/router';
import * as moment from 'moment';
import swal from 'sweetalert2';
import { SnotifyService, SnotifyPosition, SnotifyToastConfig } from 'ng-snotify';
import {NgbRatingConfig} from '@ng-bootstrap/ng-bootstrap';

import {transition, trigger, style, animate} from '@angular/animations';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-detail-booking',
  templateUrl: './detail-booking.component.html',
  styleUrls: ['./detail-booking.component.css',
  '../../../../../node_modules/sweetalert2/dist/sweetalert2.min.css'],
  encapsulation: ViewEncapsulation.None,
  animations: [
    trigger('fadeInOutTranslate', [
      transition(':enter', [
        style({opacity: 0}),
        animate('400ms ease-in-out', style({opacity: 1}))
      ]),
      transition(':leave', [
        style({transform: 'translate(0)'}),
        animate('400ms ease-in-out', style({opacity: 0}))
      ])
    ])
  ]
})
export class DetailBookingComponent implements OnInit {
  timeout = 3000;
  newTop = true;
  dockMax = 4;
  blockMax = 3;
  titleMaxLength = 30;
  bodyMaxLength = 100;

  model;
  public hari = new Array;

  public page: number = 1; //page
  public limit: number = 9; //itemsPerPage
  public totalItems: number; //elements

  public search = "";

  trainerbt: Trainer[];
  // detail: Detail[];
  @Input() detail: Detail;

  public previousPage: any;

  //MAPS
  lat: number = -6.21462;
  lng: number = 106.84513;

  public idTrainer: string;

  public startTime: any;
  public endTime: any;
  public id:any;

  today = new Date();
  thisYear = this.today.getFullYear();
  thisMonth = this.today.getMonth() + 1;
  thisDay = this.today.getDate();

  public DetailBooking = {
    "id":"",
    "customer_name": "",
    "customer_phone": "",
    "class_booking": "",
    "trainer_name": "",
    "longitude":"",
    "latitude":"",
    "trainer_phone": "",
    "date_booking": "",
    "bill": "",
    "address": "",
    "booking_speciality_list": [
      {
        "name":"",
        "price":"",
        "quantity":""
      }
    ],
    "trainer": "",
    "shift_id": "",
    "booking_shift_list": [
      {
        "start_time": "",
        "end_time": ""
        
      }
    ],
    "join_shift": true,
    "status_booking" : "",
    "photo_trainer" : "",
    "discount":"",
    "grand_total":""
  }

  public FormConfirmBooking = {
    "description": "",
    "id": "",
    "url_payment": "",
    "email": "",
    "version" : 0
  }
  public FormRating = {
  "booking_id": "",
  "rating_trainer": 0,
  "review": "",
  "email": "",
}

  showDialog = false;
  @Input() visible: boolean;

  booking: Booking[];
  public joinDesc : string;

  constructor(private route: ActivatedRoute, private bookingService: BookingService, private config: NgbDatepickerConfig,private MapsService: MapsService,private router: Router, private snotifyService: SnotifyService, private service:AdminService, private configRating: NgbRatingConfig) {
    configRating.max =5;
  }

  getConfig(): SnotifyToastConfig {
    this.snotifyService.setDefaults({
      global: {
        newOnTop: this.newTop,
        maxAtPosition: this.blockMax,
        maxOnScreen: this.dockMax,
      }
    });
    return {
      bodyMaxLength: this.bodyMaxLength,
      titleMaxLength: this.titleMaxLength,
      backdrop: -1,
      position: SnotifyPosition.centerTop,
      timeout: this.timeout,
      showProgressBar: false,
      closeOnClick: true,
      pauseOnHover: false
    };
  }

  ngOnInit() {
    // this.loadData();
    window.scrollTo(0,0);
    this.getAll();
  }
  // loadData() {
  //   this.bookingService.getPaginate(this.page - 1, this.limit, this.search).subscribe(posts => {
  //     this.booking = posts["result"];
  //     this.totalItems = posts["elements"];
  //     console.log(this.booking, this.totalItems);
  //   });
  // }
  getAll() {
    window.scrollTo(0, 0);
    const id = this.route.snapshot.paramMap.get('id');
    this.bookingService.getDetailBooking(id).subscribe(data => {
      this.DetailBooking = data.result;
      this.DetailBooking.date_booking = moment(this.DetailBooking.date_booking).format("MMMM Do YYYY");
      // this.trainerbt = posts["result"];
      // this.totalItems = posts["elements"];
      console.log(this.DetailBooking);

      if(this.DetailBooking.join_shift == false){
        console.log("ya ini false");
        this.joinDesc = "(There's a breaktime)"
      } else {
        console.log("ini true");
        this.joinDesc = "(No breaktime)"
      }
    });
  }

  clickDelete(){
    const id = this.route.snapshot.paramMap.get('id');
    
    this.bookingService.deleteBooking(id).subscribe(data => {
      console.log(data)
      if(data.message== 'OK'){

        this.router.navigate(['/booking/list'])
      }else {
        
      }
    });

  }

  // paginatio(page: number) {
  //   if (page !== this.previousPage) {
  //     this.previousPage = page;
  //     this.getAll();
  //   }
  // }


  //CREATE
  public counter: number = 0;

  increment() {
    this.counter += 1;
  }

  decrement() {
    if (this.counter >= 1) {
      this.counter -= 1;
    }
  }

  openSwalR(){
    swal({
      title: 'Are you sure?',
      text: 'You wont be able to revert',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!', 
    }).then(()=> {
      const id = this.route.snapshot.paramMap.get('id');      
      this.bookingService.deleteBooking(id).subscribe(data =>{
        console.log(data)
        if(data.message== 'OK'){
          swal(
            'Deleted!',
            'Your file has been deleted.',
            'success'
          );
  
          this.router.navigate(['/booking/list'])
        }
      })
      
    }).catch(swal.noop); 
  }

  onStepChanged(step) {

  }
  openMyModal(event) {
    document.querySelector('#' + event).classList.add('md-show');
  }

  closeMyModal(event) {
    ((event.target.parentElement.parentElement).parentElement).classList.remove('md-show');
  }

  openBasicModal(event) {
    this.showDialog = !this.showDialog;
    setTimeout(() => {
      document.querySelector('#' + event).classList.add('md-show');
    }, 25);
  }

  closeBasicModal(event) {
    ((event.target.parentElement.parentElement).parentElement).classList.remove('md-show');
    setTimeout(() => {
      this.visible = false;
      this.showDialog = !this.showDialog;
    }, 300);
  }

  public fileBukti:any;

  checkFileExtBukti() {
    let name = this.fileBukti.name.split(".");
    let ext = name[name.length - 1];
    if(!(ext == 'jpg' || ext == 'JPG' || ext == 'png' || ext == 'PNG' || ext == 'jpeg' || ext == 'JPEG')) {
      return true;
    } else {
      return false;
    }
  }

  uploadBukti(event){
    this.fileBukti = event.target.files[0];
    if(this.checkFileExtBukti()){
      this.snotifyService.error('Image must be in .jpg / .png / .jpeg', this.getConfig());
      this.fileBukti = null;
    }else{
      let fd = new FormData();
      fd.append('file', this.fileBukti);
      this.service.uploadImg(fd).subscribe( post => {
        // this.FormConfirmBooking.url_payment = post["result"];
        console.log(post.message)
        if(post.message != "ERROR"){
          this.FormConfirmBooking.url_payment = post["result"];
          console.log(this.FormConfirmBooking.url_payment)
        } else {
          this.snotifyService.error(post.result, this.getConfig());
        }
      });
    }
  }

  imgBukti:File;

  clickSubmit(){
    // if (this.imgBukti.type.match('image.*')) {
    //   console.log("is an image");
    //   console.log("Show type of image: ", this.imgBukti.type.split("/")[1]);
    // }
    this.FormConfirmBooking.id = this.DetailBooking.id
    if(this.FormConfirmBooking.url_payment == ""){
      this.snotifyService.error("Please upload Proof of Payment", this.getConfig());
    } else {
      this.bookingService.createVerifikasiPembayaran(this.FormConfirmBooking).subscribe(posts => {
        if(posts.message != "ERROR"){
          this.snotifyService.success('Success!', this.getConfig());

          setTimeout(() => {
            this.router.navigate(['/booking/list']);
          }, 4000);
        } else if(this.DetailBooking.status_booking == "PAID") {
          console.log(this.DetailBooking.status_booking)
          this.snotifyService.error("This booking has been Confirmed", this.getConfig());
        } else if(this.DetailBooking.status_booking == "BOOKED") {
          this.snotifyService.error("This booking has not been Approved", this.getConfig());          
        } else {
          this.snotifyService.error(posts.result, this.getConfig());
        }
        console.log(posts);
      });
    }
    
  }

  submitRating(){
    // if (this.imgBukti.type.match('image.*')) {
    //   console.log("is an image");
    //   console.log("Show type of image: ", this.imgBukti.type.split("/")[1]);
    // }
    this.FormRating.booking_id = this.DetailBooking.id
    console.log(this.FormRating)
    if(this.FormRating.rating_trainer == 0){
      this.snotifyService.error("Please input rating for this trainer", this.getConfig());
    }else {
      this.bookingService.submitRating(this.FormRating).subscribe(posts => {
        if(posts.message != "ERROR"){
          this.snotifyService.success('Success!', this.getConfig());

          setTimeout(() => {
            this.router.navigate(['/booking/list']);
          }, 4000);
        }
        else {
          this.snotifyService.error(posts.result, this.getConfig());
        }
        console.log(posts);
      });
    }
    
  }

  //Reactive Form
  form = new FormGroup({
    customerEmail : new FormControl('', [
      Validators.required,
      Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$'), 
      /* Validators.minLength(3),
      Validators.maxLength(50) */
    ])
  })
  
  get customerEmail(){
    return this.form.get('customerEmail');
  }
    

}

