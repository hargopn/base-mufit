import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {SharedModule} from '../../shared/shared.module';
import { CreateBookingComponent } from './create-booking/create-booking.component';
import { EditBookingComponent } from './edit-booking/edit-booking.component';
import { DetailBookingComponent } from './detail-booking/detail-booking.component';

export const UserRoutes: Routes = [
  {
    path: '',
    data: {
      breadcrumb: '',
      status: false
    },
    children: [
      {
        path: 'list',
        loadChildren: './list-booking/list-booking.module#ListBookingModule'
      },
      {
        path: 'add',
        loadChildren: './create-booking/create-booking.module#CreateBookingModule'
      },
      {
        path: 'edit/:id',
        loadChildren: './edit-booking/edit-booking.module#EditBookingModule'
      },
      {
        path: 'detail/:id',
        loadChildren: './detail-booking/detail-booking.module#DetailBookingModule'
      }
    ]
  }
];
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(UserRoutes),
    SharedModule
  ],
  declarations: []
})
export class BookingModule { }



