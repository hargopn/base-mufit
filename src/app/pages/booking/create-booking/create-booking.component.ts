import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { BookingService } from './../../../services/booking.service';
import { Detail } from './../../../class/detail';
import { Trainer } from './../../../class/trainer';
import { TrainerService } from './../../../services/trainer.service';
import { Component, OnInit, Input, Injectable } from '@angular/core';
import {NgbDateParserFormatter, NgbDatepickerConfig, NgbDateStruct} from '@ng-bootstrap/ng-bootstrap';
import { Speciality } from '../../../class/speciality';
import {NgbTimepickerConfig} from '@ng-bootstrap/ng-bootstrap';
import {NgbTimeStruct} from '@ng-bootstrap/ng-bootstrap';
import {SnotifyService, SnotifyPosition, SnotifyToastConfig} from 'ng-snotify';
import { MapsService } from '../../../services/maps.service';
import * as moment from 'moment';
import { isNumber, toInteger, padNumber } from '@ng-bootstrap/ng-bootstrap/util/util';
import { ElementRef, NgZone, ViewChild } from '@angular/core';
import { } from 'googlemaps';
import { MapsAPILoader } from '@agm/core';
import {} from '@types/googlemaps';
import { VoucherService } from './../../../services/voucher.service'; 
import { Voucher } from './../../../class/voucher';


//Format dd-mm-yyyy
@Injectable()
export class NgbDateCustomParserFormatter extends NgbDateParserFormatter {
  parse(value: string): NgbDateStruct {
    if (value) {
      const dateParts = value.trim().split('-');
      if (dateParts.length === 1 && isNumber(dateParts[0])) {
        return {day: toInteger(dateParts[0]), month: null, year: null};
      } else if (dateParts.length === 2 && isNumber(dateParts[0]) && isNumber(dateParts[1])) {
        return {day: toInteger(dateParts[0]), month: toInteger(dateParts[1]), year: null};
      } else if (dateParts.length === 3 && isNumber(dateParts[0]) && isNumber(dateParts[1]) && isNumber(dateParts[2])) {
        return {day: toInteger(dateParts[0]), month: toInteger(dateParts[1]), year: toInteger(dateParts[2])};
      }
    }
    return null;
  }

  format(date: NgbDateStruct): string {
    return date ?
        `${isNumber(date.day) ? padNumber(date.day) : ''}/${isNumber(date.month) ? padNumber(date.month) : ''}/${date.year}` :
        '';
  }
}

@Component({
  selector: 'app-create-booking',
  templateUrl: './create-booking.component.html',
  styleUrls: ['./create-booking.component.css']
})
export class CreateBookingComponent implements OnInit {
  public model : NgbDateStruct;
  public hari=new Array;
  public searchControl: FormControl;
  public page: number = 1; //page
  public limit: number = 10; //itemsPerPage
  public totalItems: number; //elements
  public specialiti= "";
  public gender = "";
  public daySearch: string;
  public pmin: number;
  public pmax: number;
  public time_min;
  public time_max;
  public dateSearch: number;
  public voucherValid = null;
  public tmin: NgbTimeStruct;
  public tmax: NgbTimeStruct;
  public amountTmp = 0;
  public search = "";
  public type="";
  public code="";

  public totalPriceTmp;


  trainerbt: Trainer[];
  // detail: Detail[];
  @Input() detail: Detail;
  speciality: Speciality[];
  
  public previousPage: any;

  //MAPS
  lat: number = -6.21462;
  lng: number = 106.84513;
  latMarker : number;
  lngMarker : number;
  public idTrainer:string;

  public startTime:any;
  public endTime:any;
  vouchers: Voucher[];  

  today = new Date();
  thisYear = this.today.getFullYear();
  thisMonth = this.today.getMonth()+1;
  thisDay = this.today.getDate();

  timeStart: NgbTimeStruct;
  timeEnd: NgbTimeStruct;

  public formCreateBooking = {
    "address" : "",
    "booking_shift_list" : [],
    "booking_speciality_list" : [],
    "customer_name" : "",
    "customer_phone" : "",
    "date_booking" : 0,
    "join_shift" : true,
    "latitude" : 0,
    "longitude" : 0,
    "trainer" : "",
    "voucher_code" : "",
  }

  @ViewChild("search")
  public searchElementRef: ElementRef;

  constructor(private mapsAPILoader: MapsAPILoader,private ngZone: NgZone, private TrainerService:TrainerService, private bookingService:BookingService, private config: NgbDatepickerConfig, configTime: NgbTimepickerConfig, private snotifyService : SnotifyService, private serviceMaps : MapsService, private router:Router, private service:VoucherService) { 
    // configTime.spinners = false;
  }

  timeout = 3000;
  newTop = true;
  dockMax = 4;
  blockMax = 3;
  titleMaxLength = 30;
  bodyMaxLength = 100;

  getConfig(): SnotifyToastConfig {
    this.snotifyService.setDefaults({
      global: {
        newOnTop: this.newTop,
        maxAtPosition: this.blockMax,
        maxOnScreen: this.dockMax,
      }
    });
    return {
      bodyMaxLength: this.bodyMaxLength,
      titleMaxLength: this.titleMaxLength,
      backdrop: -1,
      position: SnotifyPosition.centerTop,
      timeout: this.timeout,
      showProgressBar: false,
      closeOnClick: true,
      pauseOnHover: false
    };

  }

  ngOnInit() {
    window.scrollTo(0,0);
    this.getAll();
  }
  getAll() {
    this.config.minDate = {year: this.thisYear, month: this.thisMonth, day: this.thisDay};
    this.config.maxDate = {year: this.thisYear, month: this.thisMonth+1, day: this.thisDay};
    this.TrainerService.getSpecialities().subscribe(posts => this.speciality = posts["result"]);
  }

  trName : string;
  clickGetTrainer = false;

  getTrainer(){
    if(this.specialiti === ""){
      this.snotifyService.error('Please Select Speciality',this.getConfig());
    } else if(this.model === undefined){
      this.snotifyService.error('Please Select Date',this.getConfig());
      console.log(this.tmin);
    } else if(this.tmin === undefined || this.tmin === null){
      this.snotifyService.error('Please Input Start Time',this.getConfig());
    } else if(this.tmax === undefined || this.tmax === null){
      this.snotifyService.error('Please Input End Time',this.getConfig());
    } else if(this.tmin.hour > this.tmax.hour){
      this.snotifyService.error('Start Time cannot be greater than End Time',this.getConfig());
    } else if(this.pmax < this.pmin){
      this.snotifyService.error('Minimum Price value cannot be greater than Maximum Price value',this.getConfig());
    } else{
    this.time_min = this.tmin.hour + ":" + this.tmin.minute + ":" + this.tmin.second;
    this.time_max = this.tmax.hour + ":" + this.tmax.minute + ":" + this.tmax.second;
    console.log(this.time_min);

    var day = new Date(this.model.year, this.model.month-1, this.model.day, 0, 0, 0, 0);
    var days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
    let hariInt = day.getDay();
    let hari = days[hariInt];
    this.daySearch = hari;
    let tampungDate = this.model.year + "/" + this.model.month + "/" + this.model.day;
    let epochDate = new Date(tampungDate).getTime();
    this.dateSearch = epochDate;
    console.log("mom : " + this.dateSearch);
    console.log(this.daySearch);

    this.bookingService.getListForBooking(this.page - 1, this.limit, this.daySearch, this.specialiti, this.gender, this.pmin, this.pmax, this.time_min, this.time_max, this.dateSearch)
    .subscribe(posts=>{
      if(posts.message != 'ERROR'){
        console.log("masuk")
        if(posts.elements != 0){
        this.clickGetTrainer = true;
        this.trainerbt = posts.result;
        this.totalItems = posts.elements;

        }else {  
        this.clickGetTrainer = false;
        this.trainerbt = posts.result;
        this.totalItems = posts.elements;
          this.snotifyService.error('There is no trainer available',this.getConfig());

        }
      }
      console.log(posts)
      
      console.log("speciality : "+ this.specialiti);
      console.log("gender : " + this.gender);
      console.log(this.tmin);
      console.log(this.tmax);
      console.log("pmin pmax : " + this.pmin + " " + this.pmax);
      console.log(this.model);

      console.log(posts);
    });

    }
  }

  paginatio(page: number) {
    if (page !== this.previousPage) {
      this.previousPage = page;
      this.getAll();
    }
  }

  getSearch(query){
    this.search = query;
    this.getAll();
  }

  //CREATE
  public totalPrice : number = 0;
  public totalQty : number = 0;

  increment(index){
    /* if(this.totalQty < this.maxQty){
      this.snotifyService.success('Belum max',this.getConfig());
    } else if (this.maxQty == 0) {
      this.snotifyService.error('Tolong pilih shift dulu',this.getConfig());
    } else {
      this.snotifyService.error('Udah max',this.getConfig());
    } */


    if(this.totalQty < this.maxQty){
      //
      if(this.detail.trainer_speciality[index].value == true){
        console.log(this.detail.trainer_speciality[index])
        
        this.totalQty += 1;
        console.log("totalQty : " + this.totalQty);
        this.detail.trainer_speciality[index].quantity = this.detail.trainer_speciality[index].counter += 1;
        console.log("quan :"+this.detail.trainer_speciality[index].quantity);
        
        //Loop formCreateBooking.booking_speciality_listnya. Validasi jika id_trainer_speciality-nya sama kayak id yang di detail
        // kalau sama masukin quantity kedalam quantity sesuai id di dalam formCreateBooking
        for(var i=0;i<this.formCreateBooking.booking_speciality_list.length;i++){
          if(this.formCreateBooking.booking_speciality_list[i].id_trainer_speciality == this.detail.trainer_speciality[index].id){
            this.formCreateBooking.booking_speciality_list[i].quantity = this.detail.trainer_speciality[index].quantity;
          }
        }
        
        this.detail.trainer_speciality[index].totalPrice = this.detail.trainer_speciality[index].counter * this.detail.trainer_speciality[index].price;
        this.totalPrice = 0 ;
        for(let i=0;i<this.detail.trainer_speciality.length;i++){

          console.log(this.detail.trainer_speciality[i].totalPrice)
          if(this.detail.trainer_speciality[i].totalPrice == undefined){
            this.detail.trainer_speciality[i].totalPrice = 0;
          }
          this.totalPrice = this.totalPrice + this.detail.trainer_speciality[i].totalPrice;
          this.totalPriceTmp = this.totalPrice;
          console.log(this.totalPrice)
        }
      }else{
        this.snotifyService.error('Please select the class first',this.getConfig());
      }
      //
    } else if (this.maxQty == 0) {
      this.snotifyService.error('Please select the shift time and the class first',this.getConfig());
    } else {
      this.snotifyService.error('You have reached the maximum quantity',this.getConfig());
    }
  }
  
  decrement(index){
    if(this.detail.trainer_speciality[index].counter>=1){
      
      this.totalQty -= 1;
      console.log("totalQty : " + this.totalQty);
      // this.formCreateBooking.booking_speciality_list[0].quantity = this.detail.trainer_speciality[index].counter -= 1;
      this.detail.trainer_speciality[index].quantity = this.detail.trainer_speciality[index].counter -= 1;
      for(var i=0;i<this.formCreateBooking.booking_speciality_list.length;i++){
        if(this.formCreateBooking.booking_speciality_list[i].id_trainer_speciality == this.detail.trainer_speciality[index].id){
          this.formCreateBooking.booking_speciality_list[i].quantity = this.detail.trainer_speciality[index].quantity;
        }
      }

      this.detail.trainer_speciality[index].totalPrice = this.detail.trainer_speciality[index].counter * this.detail.trainer_speciality[index].price;
      this.totalPrice = 0 ;

      for(let i=0;i<this.detail.trainer_speciality.length;i++){

        console.log(this.detail.trainer_speciality[i].totalPrice)
        if(this.detail.trainer_speciality[i].totalPrice == undefined){
          this.detail.trainer_speciality[i].totalPrice = 0;
        }
        this.totalPrice = this.totalPrice + this.detail.trainer_speciality[i].totalPrice;
        console.log(this.totalPrice)
        this.totalPriceTmp = this.totalPrice;        
      }
    }
  }

  public maxQty = 0;

  checkedShift(event,shiftTime){
    console.log(event);
    console.log(shiftTime);
    

    let tampung = {
      "shift_id": shiftTime.id,
    };

    if(shiftTime.value == true){
      this.maxQty += shiftTime.shift_range;
      this.formCreateBooking.booking_shift_list.push(tampung);
      console.log(shiftTime.shift_range);
      console.log("max :" + this.maxQty);
    } else {

      // 
      this.totalQty = 0;
      this.totalPrice = 0;
      for(var j=0;j<this.detail.trainer_speciality.length;j++){
        this.detail.trainer_speciality[j].counter = 0;
        this.detail.trainer_speciality[j].totalPrice = 0;
      }
      for(var k=0;k<this.formCreateBooking.booking_speciality_list.length;k++){
        this.formCreateBooking.booking_speciality_list[k].quantity = 0;
      }

      this.maxQty -= shiftTime.shift_range;
      console.log("max :" + this.maxQty);
      let i = this.formCreateBooking.booking_shift_list.map(function(e) { return e.id; }).indexOf(shiftTime.id);
      this.formCreateBooking.booking_shift_list.splice(i,1)
    }
    console.log(this.formCreateBooking.booking_shift_list);

    // let tampungDay = this.model.day + "/" + this.model.month + "/" + this.model.year;
    let tampungDay = this.model.year + "/" + this.model.month + "/" + this.model.day;
    console.log(tampungDay);
    // let day = tampungDay.match(/(\d{2})\/(\d{2})\/(\d{4})/);
    let newDay = new Date(tampungDay).getTime()
    this.formCreateBooking.date_booking = newDay;
    console.log(this.formCreateBooking.date_booking)
  }

  checkedSpeciality(event,speciality,index){
    console.log(event);
    console.log(index);
    console.log(speciality)
    
    if(event.target.checked === true){
      let input ={
        "id_trainer_speciality": speciality.id,
        "quantity": 0
      }
      this.formCreateBooking.booking_speciality_list.push(input)
      console.log(this.formCreateBooking.booking_speciality_list)
    }else{
      let i = this.formCreateBooking.booking_speciality_list.map(function(e) { return e.id; }).indexOf(speciality.id);
      this.formCreateBooking.booking_speciality_list.splice(i,1);

      // this.totalPrice = this.totalPrice - this.detail.trainer_speciality[i].totalPrice;
      this.totalQty -= this.detail.trainer_speciality[index].counter;      
      this.detail.trainer_speciality[index].counter = 0;

      this.totalPrice = this.totalPrice - this.detail.trainer_speciality[index].totalPrice;
      this.detail.trainer_speciality[index].totalPrice = 0;
      console.log("totalQty : " + this.totalQty);
    }
  }

  locationChosen = false;
  fullAddress : string;

  
  

  onChooseLocation(event){
    this.latMarker = event.coords.lat;
    this.lngMarker = event.coords.lng;

    this.lat = event.coords.lat;
    this.lng = event.coords.lng;

    this.locationChosen = true;
    console.log(event);

    this.serviceMaps.getLocation(this.lat, this.lng).subscribe(posts => {
      console.log(posts);
      this.fullAddress = posts.results[0].formatted_address;
      this.formCreateBooking.address = this.fullAddress;
      this.formCreateBooking.latitude = this.lat;
      this.formCreateBooking.longitude = this.lng;
      
      console.log(this.fullAddress);
      console.log(this.formCreateBooking);
    });
  }

  onStepChanged(step) {

  }

  onStep1Next(event){
    
  }

  onStep2Next(event){
    window.scrollTo(0, 0);
    /* this.TrainerService.getDetailTrainer(this.idTrainer).subscribe(posts=> {
      this.detail = posts.result;

      for(let i=0;i<this.detail.trainer_speciality.length;i++){
        this.detail.trainer_speciality[i].counter = 0;
      } */

      /* let detailDay = this.detail.day;

      this.formCreateBooking.trainer = this.detail.id;
      console.log("Form : " + this.formCreateBooking.trainer);

      var days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];      
      let tampung;
      for(var i=0;i<detailDay.length;i++){
        console.log("harinya dia : " + detailDay[i].name);
        console.log(days.indexOf(detailDay[i].name));
        let input = days.indexOf(detailDay[i].name) + "||";
        
      }
      
      console.log(posts); */

      // customize default values of datepickers used by this component tree
      /* this.config.minDate = {year: this.thisYear, month: this.thisMonth, day: this.thisDay};
      this.config.maxDate = {year: this.thisYear, month: this.thisMonth+1, day: this.thisDay}; */
    // });
    

    this.TrainerService.getDetailShiftTrainer(this.idTrainer, this.daySearch, this.dateSearch).subscribe(posts =>{
      this.detail = posts.result;

      this.formCreateBooking.trainer = this.detail.id;      

      for(let i=0;i<this.detail.trainer_speciality.length;i++){
        this.detail.trainer_speciality[i].counter = 0;
      }

      this.searchControl = new FormControl();
    
    this.mapsAPILoader.load().then(() => {
      let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
        types: ["address"]
      });
      autocomplete.addListener("place_changed", () => {
        this.ngZone.run(() => {
          //get the place rnpm install @types/googlemapsesult
          let place: google.maps.places.PlaceResult = autocomplete.getPlace();
  
          //verify result
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }
          
          //set latitude, longitude and zoom
          this.latMarker = place.geometry.location.lat();
          this.lngMarker = place.geometry.location.lng();
          this.lat = place.geometry.location.lat();
          this.lng = place.geometry.location.lng();
        });
      });
    });
    })
    
  }

  onStep3Prev(event){
    this.totalPrice = 0;
    for(var i=0;i<this.formCreateBooking.booking_speciality_list.length;i++){
      this.formCreateBooking.booking_speciality_list.splice(this.formCreateBooking.booking_speciality_list[i]);
    }

    for(var i=0;i<this.formCreateBooking.booking_shift_list.length;i++){
      this.formCreateBooking.booking_shift_list.splice(this.formCreateBooking.booking_shift_list[i]);
    }
    this.formCreateBooking.date_booking = 0;

    this.totalQty = 0;
    this.maxQty = 0;    
  }

  onComplete(event){
    console.log(this.totalQty)
    console.log(this.maxQty)
    console.log(this.formCreateBooking)
    this.bookingService.createBooking(this.formCreateBooking).subscribe(posts => {
      this.snotifyService.success('This booking has been created',this.getConfig());
      setTimeout(() => {
        this.router.navigate(['/booking/list'])
      }, 4000);
    });
  }

  setDat(event, value){
    console.log(event);
    console.log(value);

    this.idTrainer = value.id;

    console.log(this.idTrainer);
  }

  getSelect(limitnya) {
    this.bookingService.getListForBooking(this.page - 1, this.limit, this.daySearch, this.specialiti, this.gender, this.pmin, this.pmax, this.time_min, this.time_max, this.dateSearch)
    .subscribe(posts=>{
      this.limit = limitnya;
      this.getTrainer();
    });
  }

  onChange(deviceValue) {
    this.getSelect(deviceValue);
  }

  checkedJoin(event, join){
    console.log(event);
    console.log(join);

    this.formCreateBooking.join_shift = true;
    console.log(this.formCreateBooking.join_shift)
    
  }

  checkedSeparate(event, separate){
    console.log(event);
    console.log(separate);

    this.formCreateBooking.join_shift = false;
    console.log(this.formCreateBooking.join_shift)
  }

  checkRadio(){
    if(this.idTrainer == undefined){
      return false;  
    } else{
      return true;
    }
  }

  inputTrue(){

    for(let i=0;i<this.formCreateBooking.booking_speciality_list.length;i++){
      var count = this.formCreateBooking.booking_speciality_list[i].quantity;
    }

    for(let i=0;i<this.formCreateBooking.booking_shift_list.length;i++){
      var cekShift = this.formCreateBooking.booking_shift_list[i];
    }

    if(this.formCreateBooking.date_booking == 0){
      return false;
    } else if(this.totalPrice == 0) {
      return false;
    } else if (this.formCreateBooking.address == "") {
      return false;
    } else if (cekShift == undefined){
      return false;
    } else if (count == 0) {
      return false;
    } else if(this.maxQty != this.totalQty){
      return false;
    }else {
      return true;
    }
  }

  // REACTIVE FORMS
  form = new FormGroup({
    customerName : new FormControl('', [
      Validators.required,
      Validators.minLength(3),
      Validators.maxLength(50),
      Validators.pattern("^[a-zA-Z\\s]+$")
    ]),
    phone : new FormControl('', [
      Validators.required,
      Validators.minLength(10),
      Validators.maxLength(13),      
      Validators.pattern('[0][8][0-9]{8,11}')
    ]),
  })

  get customerName(){
    return this.form.get('customerName');
  }

  get phone(){
    return this.form.get('phone');
  }

  /* public validate(evt) {
    var theEvent = evt || window.event;
    var key = theEvent.keyCode || theEvent.which;
    key = String.fromCharCode( key );
    var regex = /[0-9]|\./;
    if( !regex.test(key) ) {
      theEvent.returnValue = false;
      if(theEvent.preventDefault) theEvent.preventDefault();
    }
  } */

  isNumber(event){
    var keycode = event.keyCode;
    console.log(keycode);
    if(keycode>=48 && keycode<=57){
      return true;
    } else {
      return false;
    }
  }

  isLetter(event){
    var keycode = event.keyCode;
    console.log(keycode);

    if((keycode>=65 && keycode<=90) || (keycode>=97 && keycode<=122)){
      return true;
    } else {
      return false;
    }
  }
  // loadData() {
  //   this.service.getList(this.page - 1, this.limit, this.type, this.code).subscribe(posts => {
  //     this.vouchers = posts["result"];
  //     this.totalItems = posts["elements"];
  //     console.log(this.vouchers, this.totalItems);
  //     for(var i=0;i<this.vouchers.length;i++){
  //       // var formatDate = this.booking[i].date_booking.getUTCDate() + '-' + (date.getUTCMonth() + 1)+ '-' + date.getUTCFullYear()
  //       // console.log(this.booking[i].date_booking);
  //       // this.booking[i].date_booking = moment(this.booking[i].date_booking).format('LL');
  //       this.vouchers[i].end_date = moment(this.vouchers[i].end_date).format('DD/MM/YYYY');
  //     }
  //   });
  // }
  getSearch2(query){
    this.code = query;
    // this.loadData();
  }

  // 
  checkVoucher() {
    this.service.checkVoucher(this.formCreateBooking.voucher_code).subscribe(response => {
      console.log(response);
      this.totalPrice = this.totalPriceTmp;
      if(response.message != 'ERROR') {
        this.voucherValid = true;        
        if(response.result.type == 'nominal'){
          this.totalPrice -= response.result.value;
        } else {
          let diskon = this.totalPrice * (response.result.value / 100);
          this.totalPrice -= diskon;
        }
        
        // for(let i = 0; i < this.contractPriceList.length; i++) {
        //   this.contractPriceList[i].price -= response.result.discount; 
        // }
      } else {
        this.voucherValid = false;
        this.totalPrice = this.totalPriceTmp;
        // this.contractPriceList = this.contractPriceListTmp;
        // this.formKontrak.amount = this.amountTmp;
        // this.getPriceList();
      }
    })
  }

  /* resetPrice(){
    this.pmin = "";
    this.pmax = "";
  } */

}
