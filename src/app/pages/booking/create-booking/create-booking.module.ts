import { VoucherService } from './../../../services/voucher.service';
import { NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
import { MapsService } from './../../../services/maps.service';
import { TrainerService } from './../../../services/trainer.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreateBookingComponent, NgbDateCustomParserFormatter } from './create-booking.component';

import {RouterModule, Routes} from '@angular/router';
import {SharedModule} from '../../../shared/shared.module';
import {DataTableModule} from 'angular2-datatable';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormWizardModule } from 'angular2-wizard';
import { AgmCoreModule } from '@agm/core';
import {CurrencyMaskModule} from 'ng2-currency-mask';


export const createBookingRoutes: Routes = [
  {
    path: '',
    component: CreateBookingComponent,
      data: {
    breadcrumb: 'Booking List',
    icon: 'icofont-social-blogger bg-c-green',
      status: false
  } }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(createBookingRoutes),
    SharedModule,
    FormsModule,
    FormWizardModule,
    ReactiveFormsModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDGN4BvluiHX9wrr2UkUjXd0thl-mJScKk',
      libraries: ["places"]
      // apiKey: 'AIzaSyCY1qZ1yJ8c_aph1ltSMxC22VL3eD12XSg'
      
    }),
    CurrencyMaskModule
  ],
  declarations: [CreateBookingComponent],
  providers: [TrainerService,VoucherService, MapsService, {provide: NgbDateParserFormatter, useClass: NgbDateCustomParserFormatter}]
})
export class CreateBookingModule { }
