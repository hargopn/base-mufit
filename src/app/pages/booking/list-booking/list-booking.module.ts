import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListBookingComponent } from './list-booking.component';

import {RouterModule, Routes} from '@angular/router';
import {SharedModule} from '../../../shared/shared.module';

import {DataTableModule} from 'angular2-datatable';
import {FormsModule} from '@angular/forms';

import {ScrollToModule} from '@nicky-lenaers/ngx-scroll-to';
import { TrainerService } from '../../../services/trainer.service';


export const berandaRoutes: Routes = [
  {
    path: '',
    component: ListBookingComponent,
      data: {
    breadcrumb: 'Booking List',
    icon: 'icofont-social-blogger bg-c-green',
      status: false
  } }
];



@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(berandaRoutes),
    SharedModule,
    FormsModule,
    DataTableModule
  ],
  declarations: [ListBookingComponent],
  providers: [TrainerService]
})


export class ListBookingModule { }



