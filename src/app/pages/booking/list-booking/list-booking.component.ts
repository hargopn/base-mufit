import { Booking } from './../../../class/booking';
import { Component, OnInit } from '@angular/core';
import { BookingService } from '../../../services/booking.service';
import * as moment from 'moment';

@Component({
  selector: 'app-list-booking',
  templateUrl: './list-booking.component.html',
  styleUrls: ['./list-booking.component.css']
})
export class ListBookingComponent implements OnInit {
  public page: number = 1; //page
  public limit: number = 25; //itemsPerPage
  public totalItems: number; //elements

  public search="";

  public previousPage: any;

  booking: Booking[];

  constructor(private service:BookingService) { }

  ngOnInit() {
    window.scrollTo(0,0);
    this.loadData();
  }

  loadData() {
    this.service.getPaginate(this.page - 1, this.limit, this.search).subscribe(posts => {
      this.booking = posts["result"];
      this.totalItems = posts["elements"];
      console.log(this.booking, this.totalItems);
      for(var i=0;i<this.booking.length;i++){
        // var formatDate = this.booking[i].date_booking.getUTCDate() + '-' + (date.getUTCMonth() + 1)+ '-' + date.getUTCFullYear()
        // console.log(this.booking[i].date_booking);
        // this.booking[i].date_booking = moment(this.booking[i].date_booking).format('LL');
        this.booking[i].date_booking = moment(this.booking[i].date_booking).format('DD/MM/YYYY');
      }
    });
  }

  loadPage(page: number) {
    if (page !== this.previousPage) {
      this.previousPage = page;
      this.loadData();
    }
  }

  //Show limit list
  getSelect(limitnya) {
    this.service.getPaginate(this.page, limitnya, this.search).subscribe(posts => {
      this.limit = limitnya;
      this.loadData();
    });
  }

  onChange(deviceValue) {
    this.getSelect(deviceValue);
  }

  //Search
  getSearch(query){
    this.search = query;
    this.loadData();
  }

}
