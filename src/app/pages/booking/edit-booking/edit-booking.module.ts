import { MapsService } from './../../../services/maps.service';
import { TrainerService } from './../../../services/trainer.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EditBookingComponent } from './edit-booking.component';

import {RouterModule, Routes} from '@angular/router';
import {SharedModule} from '../../../shared/shared.module';

import {DataTableModule} from 'angular2-datatable';
import {FormsModule} from '@angular/forms';
import { FormWizardModule } from 'angular2-wizard';
import { AgmCoreModule } from '@agm/core';
import {SqueezeBoxModule} from 'squeezebox';


export const berandaRoutes: Routes = [
  {
    path: '',
    component: EditBookingComponent,
      data: {
    breadcrumb: 'Booking Edit',
    icon: 'icofont-social-blogger bg-c-green',
      status: false
  } }
];



@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(berandaRoutes),
    SharedModule,
    FormsModule,
    DataTableModule,
    SqueezeBoxModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCSZRlW4nBzGe7oGUH5UCun8aXSyJc3d4E'
    })
  ],
  declarations: [EditBookingComponent],
  providers: [TrainerService,MapsService]
})


export class EditBookingModule {}
