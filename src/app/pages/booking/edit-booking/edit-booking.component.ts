import { Detail } from './../../../class/detail';
import { Trainer } from './../../../class/trainer';
import { TrainerService } from './../../../services/trainer.service';
import { Component, OnInit, Input, ViewEncapsulation } from '@angular/core';
import { NgbDatepickerConfig, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { ActivatedRoute } from '@angular/router';
import { BookingService } from './../../../services/booking.service';
import { Booking } from './../../../class/booking';
import * as moment from 'moment';
import { MapsService } from '../../../services/maps.service';
import { Speciality } from '../../../class/speciality';
import { SnotifyService, SnotifyPosition, SnotifyToastConfig } from 'ng-snotify';
import { NgbTimepickerConfig } from '@ng-bootstrap/ng-bootstrap';
import { NgbTimeStruct } from '@ng-bootstrap/ng-bootstrap';
import { post } from 'selenium-webdriver/http';




@Component({
  selector: 'app-edit-booking',
  templateUrl: './edit-booking.component.html',
  styleUrls: ['./edit-booking.component.css']
})
export class EditBookingComponent implements OnInit {
  public model: NgbDateStruct;
  public hari = new Array;

  public page: number = 1; //page
  public limit: number = 9; //itemsPerPage
  public totalItems: number; //elements
  public specialiti: string;
  public gender: string;
  public daySearch: string;
  public pmin: number;
  public pmax: number;
  public time_min;
  public time_max;

  public search = "";

  trainerbt: Trainer[];
  // detail: Detail[];
  @Input() detail: Detail;
  speciality: Speciality[];


  public previousPage: any;

  //MAPS
  lat: number = -6.21462;
  lng: number = 106.84513;

  public idTrainer: string;

  public startTime: any;
  public endTime: any;

  today = new Date();
  thisYear = this.today.getFullYear();
  thisMonth = this.today.getMonth() + 1;
  thisDay = this.today.getDate();

  public formEditBooking = {
    "address": "",
    "booking_shift_list": [
      {
        "shift_id": ""
      }
    ],
    "customer_name": "",
    "customer_phone": "",
    "booking_speciality_list": [],
    "date_booking": "",
    "trainer": ""
  }

  public DetailBooking = {
    "customer_name": "",
    "customer_phone": "",
    "class_booking": "",
    "trainer_name": "",
    "trainer_phone": "",
    "date_booking": "",
    "bill": "",
    "address": "",
    "trainer": "",
    "shift_id": "",
    "booking_shift_list": [
      {
        "start_time": "",
        "end_time": ""

      }
    ],
    "booking_speciality_list": [
      {
        "name": "",
        "price": "",
        "quantity": ""
      }
    ],
  }
  public data: any;

  booking: Booking[];

  constructor(private TrainerService: TrainerService, private service: BookingService, private route: ActivatedRoute, private bookingService: BookingService, private config: NgbDatepickerConfig, private snotifyService: SnotifyService, private serviceMaps: MapsService) {

  }

  ngOnInit() {
    // this.loadData();
    // this.getAll();
    // this.getData();
    window.scrollTo(0,0);
    this.getSpecialities();

  }
  // getAll() {
   
  //   this.config.minDate = { year: this.thisYear, month: this.thisMonth, day: this.thisDay };
  //   this.config.maxDate = { year: this.thisYear, month: this.thisMonth + 1, day: this.thisDay };
  //   this.TrainerService.getSpecialities().subscribe(posts => this.speciality = posts["result"]);

  //   this.TrainerService.getDetailTrainer(this.idTrainer).subscribe(posts=>{
  //     this.detail = posts.result;
  //   })

  //   // var day = new Date(this.model.year, this.model.month-1, this.model.day, 0, 0, 0, 0);
  //   //   console.log(day.getDay());
  // }

  // getTrainer() {

  //   var day = new Date(this.model.year, this.model.month - 1, this.model.day, 0, 0, 0, 0);
  //   var days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
  //   let hariInt = day.getDay();
  //   let hari = days[hariInt];
  //   this.daySearch = hari;
  //   console.log(this.daySearch);

  //   this.bookingService.getListForBooking(this.page - 1, this.limit, this.daySearch, this.specialiti, this.gender, this.pmin, this.pmax, this.time_min, this.time_max)
  //     .subscribe(posts => {
  //       this.trainerbt = posts["result"];
  //       this.totalItems = posts["elements"];

  //       console.log("speciality : " + this.specialiti);
  //       console.log("gender : " + this.gender);
  //       console.log("pmin pmax : " + this.pmin + " " + this.pmax);
  //       console.log(this.model);
  //       console.log(posts);
  //     });

  // }

  // loadData() {
  //   this.service.getPaginate(this.page - 1, this.limit, this.search).subscribe(posts => {
  //     this.booking = posts["result"];
  //     this.totalItems = posts["elements"];
  //     console.log(this.booking, this.totalItems);
  //   });
  // }
  getData() {
    const id = this.route.snapshot.paramMap.get('id');
    this.bookingService.getDetailBooking(id).subscribe(post => {
      this.DetailBooking = post.result;
      this.DetailBooking.date_booking = moment(this.DetailBooking.date_booking).format("MMMM Do YYYY");

      for(let i = 0; i<this.data.length;i++){
        for(let x = 0; x<this.DetailBooking.booking_shift_list.length;x++){
          if(this.data[i].start_time === this.DetailBooking.booking_shift_list[x].start_time)
          {
            this.data[i].value = true;
          }
        }
      }
      // this.trainerbt = posts["result"];
      // this.totalItems = posts["elements"];
      console.log(this.DetailBooking);
      
    });
  }

  getSpecialities(){
    this.TrainerService.getDetailTrainer(this.idTrainer).subscribe(posts=> {
      this.detail = posts.result;
      for(let i=0;i<this.detail.trainer_speciality.length;i++){
        this.detail.trainer_speciality[i].counter = 0;
      }
    });
    this.TrainerService.getShiftTrainer().subscribe(post =>{
      this.data = post.result;
      console.log(this.data.id)
     for( let i =0; i< this.data.length; i ++){ 
       this.data[i].value;
    }
    this.getData();
    });
      
  }

  // onStep2Next(event){
  //   window.scrollTo(0, 0);
  //   this.TrainerService.getDetailTrainer(this.idTrainer).subscribe(posts=> {
  //     this.detail = posts.result;
  //     for(let i=0;i<this.detail.trainer_speciality.length;i++){
  //       this.detail.trainer_speciality[i].counter = 0;
  //     }
  //     let detailDay = this.detail.day;

  //     this.formEditBooking.trainer = this.detail.name;
  //     console.log("Form : " + this.formEditBooking.trainer);

  //     var days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];      
  //     let tampung;
  //     for(var i=0;i<detailDay.length;i++){
  //       console.log("harinya dia : " + detailDay[i].name);
  //       console.log(days.indexOf(detailDay[i].name));
  //       // let input = detailDay[i].name+" === "+days.indexOf(detailDay[i].name)
  //       let input = days.indexOf(detailDay[i].name) + "||";
        
  //     }
      
  //     console.log(posts);

  //     // customize default values of datepickers used by this component tree
  //     this.config.minDate = {year: this.thisYear, month: this.thisMonth, day: this.thisDay};
  //     this.config.maxDate = {year: this.thisYear, month: this.thisMonth+1, day: this.thisDay};
  //   });

    
  // }

  //CREATE
  public counter: number = 0;
  public totalPrice: number;


  increment(index) {
    if (this.detail.trainer_speciality[index].value == true) {
      console.log(this.detail.trainer_speciality[index])
      this.detail.trainer_speciality[index].counter += 1;
      // for(var i=0; i<this.detail.trainer_speciality.length; i++){
      // if(this.detail[i].trainer_speciality[i] == this.specialiti){
      this.totalPrice = this.detail.trainer_speciality[index].counter * this.detail.trainer_speciality[index].price;
      console.log(this.totalPrice)
      // }
      // }
    } else {
      alert("ga boleh di click")
    }
  }

  decrement(index) {
    if (this.detail.trainer_speciality[index].counter >= 1) {
      this.detail.trainer_speciality[index].counter -= 1;
    }
  }

  checkedShift(event, shiftTime) {
    console.log(event);
    console.log(shiftTime);
  }

  checkedSpeciality(event, shiftTime, index) {
    console.log(event);
    console.log(shiftTime);
    console.log(index);
  }

  locationChosen = false;
  fullAddress: string;

  onChooseLocation(event) {
    this.lat = event.coords.lat;
    this.lng = event.coords.lng;

    this.locationChosen = true;
    console.log(event);

    this.serviceMaps.getLocation(this.lat, this.lng).subscribe(posts => {
      console.log(posts);
      // this.fullAddress = posts.result["formatted_address"];
      this.fullAddress = posts.results[0].formatted_address;

      console.log(this.fullAddress);
    });
  }

}


