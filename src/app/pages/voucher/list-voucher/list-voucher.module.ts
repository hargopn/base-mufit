import { VoucherService } from './../../../services/voucher.service';
import { ListVoucherComponent } from './list-voucher.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {HttpModule} from '@angular/http';
import {SharedModule} from '../../../shared/shared.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CurrencyMaskModule} from 'ng2-currency-mask';
import {BrowserModule} from '@angular/platform-browser';

export const listVoucherRoutes: Routes = [
  {
    path: '',
    component:ListVoucherComponent,
    data: {
      breadcrumb: 'List Voucher',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(listVoucherRoutes),
    FormsModule,
    HttpModule,
    SharedModule,
    CurrencyMaskModule,
    ReactiveFormsModule
  ],
  declarations: [ListVoucherComponent],
  providers: [VoucherService],
  entryComponents: [],
  exports: [],
  bootstrap: []
})
export class ListVoucherModule { }
