import { Component, OnInit } from '@angular/core';
import { VoucherService } from './../../../services/voucher.service';
import { Voucher } from './../../../class/voucher';
import * as moment from 'moment';



@Component({
  selector: 'app-list-voucher',
  templateUrl: './list-voucher.component.html',
  styleUrls: ['./list-voucher.component.css']
})
export class ListVoucherComponent implements OnInit {
  public page: number = 1; //page
  public limit: number = 25; //itemsPerPage
  public totalItems: number; //elements

  public type="";
  public code="";
  

  public previousPage: any;

  vouchers: Voucher[];
  
  constructor(private service:VoucherService) { }
  selectedValue: string = '';

  public formVoucher = {
    "description" : "",
    "code" : "",
    "id" : "",
    "url_cover" : "",
    "version" : ""
  }

  ngOnInit() {
    window.scrollTo(0,0);
    this.loadData();
  }
  
  loadData() {
    this.service.getList(this.page - 1, this.limit, this.type, this.code).subscribe(posts => {
      this.vouchers = posts["result"];
      this.totalItems = posts["elements"];
      console.log(this.vouchers, this.totalItems);
      for(var i=0;i<this.vouchers.length;i++){
        // var formatDate = this.booking[i].date_booking.getUTCDate() + '-' + (date.getUTCMonth() + 1)+ '-' + date.getUTCFullYear()
        // console.log(this.booking[i].date_booking);
        // this.booking[i].date_booking = moment(this.booking[i].date_booking).format('LL');
        this.vouchers[i].end_date = moment(this.vouchers[i].end_date).format('DD/MM/YYYY');
      }
    });
  }

  loadPage(page: number) {
    if (page !== this.previousPage) {
      this.previousPage = page;
      this.loadData();
    }
  }

  //Show limit list
  getSelect(limitnya) {
    this.service.getList(this.page, limitnya, this.type, this.code).subscribe(posts => {
      this.limit = limitnya;
      this.loadData();
    });
  }

  onChange(deviceValue) {
    this.getSelect(deviceValue);
  }

  //Search
  getSearch(query){
    this.code = query;
    this.loadData();
  }
}
