import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {SharedModule} from '../../shared/shared.module';
import { CreateVoucherComponent } from './create-voucher/create-voucher.component';
import { ListVoucherComponent } from './list-voucher/list-voucher.component';
import { DetailVoucherComponent } from './detail-voucher/detail-voucher.component';
import { EditVoucherComponent } from './edit-voucher/edit-voucher.component';


export const UserRoutes: Routes = [
  {
    path: '',
    data: {
      breadcrumb: '',
      status: false
    },
    children: [
      {
        path: 'add',
        loadChildren: './create-voucher/create-voucher.module#CreateVoucherModule'
      },
      {
        path: 'list',
        loadChildren: './list-voucher/list-voucher.module#ListVoucherModule'
      },
      {
        path: 'detail/:id',
        loadChildren: './detail-voucher/detail-voucher.module#DetailVoucherModule'
      },
      {
        path: 'edit/:id',
        loadChildren: './edit-voucher/edit-voucher.module#EditVoucherModule'
      }
    ]
  }
];
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(UserRoutes),
    SharedModule
  ],
  declarations: []
})
export class VoucherModule { }
