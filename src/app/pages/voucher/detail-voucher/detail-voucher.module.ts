import { VoucherService } from './../../../services/voucher.service';
import { DetailVoucherComponent } from './detail-voucher.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {HttpModule} from '@angular/http';
import {SharedModule} from '../../../shared/shared.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CurrencyMaskModule} from 'ng2-currency-mask';
import {BrowserModule} from '@angular/platform-browser';

export const detailVoucherRoutes: Routes = [
  {
    path: '',
    component:DetailVoucherComponent,
    data: {
      breadcrumb: 'Detail Voucher',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(detailVoucherRoutes),
    FormsModule,
    HttpModule,
    SharedModule,
    CurrencyMaskModule,
    ReactiveFormsModule
  ],
  declarations: [DetailVoucherComponent],
  providers: [VoucherService],
  entryComponents: [],
  exports: [],
  bootstrap: []
})
export class DetailVoucherModule { }
