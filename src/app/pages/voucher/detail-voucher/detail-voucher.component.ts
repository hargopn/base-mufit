import { Voucher } from './../../../class/voucher';
import { VoucherService } from './../../../services/voucher.service';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { SnotifyService, SnotifyPosition, SnotifyToastConfig } from 'ng-snotify';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import * as moment from 'moment';
import { ActivatedRoute } from '@angular/router';
import swal from 'sweetalert2';
import { transition, trigger, style, animate } from '@angular/animations';

@Component({
  selector: 'app-detail-voucher',
  templateUrl: './detail-voucher.component.html',
  styleUrls: ['./detail-voucher.component.css',
    '../../../../../node_modules/sweetalert2/dist/sweetalert2.min.css'],
  encapsulation: ViewEncapsulation.None,
  animations: [
    trigger('fadeInOutTranslate', [
      transition(':enter', [
        style({ opacity: 0 }),
        animate('400ms ease-in-out', style({ opacity: 1 }))
      ]),
      transition(':leave', [
        style({ transform: 'translate(0)' }),
        animate('400ms ease-in-out', style({ opacity: 0 }))
      ])
    ])
  ]
})
export class DetailVoucherComponent implements OnInit {
  show1 = false;
  show2 = false;

  public id: any;
  canDelete = true;

  vouchers: Voucher[];



  constructor(private router: Router, private route: ActivatedRoute, private service: VoucherService) { }
  selectedValue: string = '';

  public formVoucher = {
    "active":true,
    "description": "",
    "code": "",
    "id": "",
    "end_date": "",
    "start_date": "",
    "url_cover": "",
    "version": "",
    "type": "",
    "value": "",
    "quantity": "",
    "list_user":[
      {
        "customer_name":"",
        "customer_phone":0,
        "redeem_date":"",
      }
    ],
  }

  ngOnInit() {
    window.scrollTo(0, 0);
    this.getAll();
    console.log('haha')
  }

  start;
  end;

  getAll() {
    window.scrollTo(0, 0);
    const id = this.route.snapshot.paramMap.get('id');
    this.service.getDetailVoucher(id).subscribe(data => {
      console.log(data);
      if(this.formVoucher.list_user.length > 0){
        this.canDelete = false;
      }
      this.formVoucher = data.result;
      this.formVoucher.start_date = moment(this.formVoucher.start_date).format("DD/MM/YYYY");
      this.formVoucher.end_date = moment(this.formVoucher.end_date).format("DD/MM/YYYY");
      for (var i = 0; i < this.formVoucher.list_user.length; i++) {
        console.log(this.formVoucher.list_user[i].redeem_date);
        this.formVoucher.list_user[i].redeem_date = moment(this.formVoucher.list_user[i].redeem_date).format("DD/MM/YYYY");
      }

      // this.formVoucher.start_date = moment(this.formVoucher.start_date).format("MMMM Do YYYY");      
      // this.trainerbt = posts["result"];
      // this.totalItems = posts["elements"];
      console.log(this.formVoucher.list_user.length);
    });
  }


  toggle1(event) {
    if (event.target.selected) {
      this.show1 = true;
    }
    else {
      this.show1 = false;
    }
  }
  toggle2(event) {
    if (event.target.selected) {
      this.show2 = true;
    }
    else {
      this.show2 = false;
    }
  }

  goToEditDetail() {

    const id = this.route.snapshot.paramMap.get('id');
    this.router.navigate(['/voucher/edit/' + id])
  }

  openSwalR() {
    swal({
      title: 'Are you sure?',
      text: 'You wont be able to revert',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
    }).then(() => {
      const id = this.route.snapshot.paramMap.get('id');
      this.service.deleteVoucher(id).subscribe(data => {
        console.log(data)
        if (data.message == 'OK') {
          swal(
            'Deleted!',
            'Your file has been deleted.',
            'success'
          );

          this.router.navigate(['/voucher/list'])
        }
      })

    }).catch(swal.noop);
  }

  isNumber(event) {
    var keycode = event.keyCode;
    console.log(keycode);
    if (keycode >= 48 && keycode <= 57) {
      return true;
    } else {
      return false;
    }
  }
}
