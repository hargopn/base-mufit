import { VoucherService } from './../../../services/voucher.service';
import { Component, OnInit, Input, ViewChild ,Injectable} from '@angular/core';
import { SnotifyService, SnotifyPosition, SnotifyToastConfig } from 'ng-snotify';
import { NgModel, NgForm, FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgbDateParserFormatter, NgbDatepickerConfig, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { NgbTimepickerConfig } from '@ng-bootstrap/ng-bootstrap';
import { NgbTimeStruct } from '@ng-bootstrap/ng-bootstrap';
import { isNumber, toInteger, padNumber } from '@ng-bootstrap/ng-bootstrap/util/util';

@Injectable()
export class NgbDateCustomParserFormatter extends NgbDateParserFormatter {
  parse(value: string): NgbDateStruct {
    if (value) {
      const dateParts = value.trim().split('-');
      if (dateParts.length === 1 && isNumber(dateParts[0])) {
        return { day: toInteger(dateParts[0]), month: null, year: null };
      } else if (dateParts.length === 2 && isNumber(dateParts[0]) && isNumber(dateParts[1])) {
        return { day: toInteger(dateParts[0]), month: toInteger(dateParts[1]), year: null };
      } else if (dateParts.length === 3 && isNumber(dateParts[0]) && isNumber(dateParts[1]) && isNumber(dateParts[2])) {
        return { day: toInteger(dateParts[0]), month: toInteger(dateParts[1]), year: toInteger(dateParts[2]) };
      }
    }
    return null;
  }

  format(date: NgbDateStruct): string {
    return date ?
      `${isNumber(date.day) ? padNumber(date.day) : ''}/${isNumber(date.month) ? padNumber(date.month) : ''}/${date.year}` :
      '';
  }
}

@Component({
  selector: 'app-create-voucher',
  templateUrl: './create-voucher.component.html',
  styleUrls: ['./create-voucher.component.css']
})
export class CreateVoucherComponent implements OnInit {
  show1 = false;
  show2 = false;

  constructor(private config: NgbDatepickerConfig, configTime: NgbTimepickerConfig,private service: VoucherService, private snotifyService: SnotifyService, private router: Router) { }
  timeout = 3000;
  newTop = true;
  dockMax = 4;
  blockMax = 3;
  titleMaxLength = 30;
  bodyMaxLength = 100;

  getConfig(): SnotifyToastConfig {
    this.snotifyService.setDefaults({
      global: {
        newOnTop: this.newTop,
        maxAtPosition: this.blockMax,
        maxOnScreen: this.dockMax,
      }
    });
    return {
      bodyMaxLength: this.bodyMaxLength,
      titleMaxLength: this.titleMaxLength,
      backdrop: -1,
      position: SnotifyPosition.centerTop,
      timeout: this.timeout,
      showProgressBar: false,
      closeOnClick: true,
      pauseOnHover: false
    };

  }

  selectedValue: string = '';

  public model1: NgbDateStruct;
  public model2: NgbDateStruct;

  value111;
  quantity111;

  public formVoucher = {
    "description": "",
    "code": "",
    "end_date": 0,
    "start_date": 0,
    "url_cover": "",
    "version": "",
    "quantity": "",
    "type": "",
    "value": 0,
  }
  today = new Date();
  thisYear = this.today.getFullYear();
  thisMonth = this.today.getMonth() + 1;
  thisDay = this.today.getDate();

  ngOnInit() {
    // this.getAll();

    // if(this.thisMonth == 12){
    //   this.thisYear = this.thisYear + 1;
    // } else {
    //   this.thisMonth = this.thisMonth + 1;
    // }

    this.config.minDate = { year: this.thisYear, month: this.thisMonth, day: this.thisDay };
    console.log(this.today);
    console.log(this.thisYear);
    console.log(this.thisMonth);
    console.log(this.thisDay);
  }

  getAll(){
    // this.config.minDate = { year: this.thisYear, month: this.thisMonth, day: this.thisDay + 1 };    
  }
  createVoucher() {
    this.config.minDate = { year: this.thisYear, month: this.thisMonth, day: this.thisDay + 1 };
    
    this.formVoucher.value = this.value111;
    this.formVoucher.quantity = this.quantity111;

    let tampungDate = this.model1.year + "/" + this.model1.month + "/" + this.model1.day;
    let epochDate = new Date(tampungDate).getTime();
    this.formVoucher.start_date = epochDate;

    let tampungDate2 = this.model2.year + "/" + this.model2.month + "/" + this.model2.day;
    let epochDate2 = new Date(tampungDate2).getTime();
    this.formVoucher.end_date = epochDate2;

    let request =
      {
        "code": this.formVoucher.code,
        "description": this.formVoucher.description,
        "end_date": this.formVoucher.end_date,
        "start_date": this.formVoucher.start_date,
        "quantity": this.formVoucher.quantity,
        "type": this.formVoucher.type,
        "value": this.formVoucher.value,
      }
    console.log(request)
    this.service.addVoucher(request).subscribe(data => {
      if (data.message != 'ERROR') {

        console.log(request);

        this.snotifyService.success('Add Voucher Success!', this.getConfig());
        setTimeout(() => {
          this.router.navigate(['/voucher/list'])
        }, 4000);
      } else {
        this.snotifyService.error(data.result, this.getConfig());
      }
      console.log(this.formVoucher)
    })
  }

  reset() {
    this.value111 = null;
    this.quantity111 = null;
  }

  toggle1(event) {
    if (event.target.selected) {
      this.show1 = true;
    }
    else {
      this.show1 = false;
    }
  }
  toggle2(event) {
    if (event.target.selected) {
      this.show2 = true;
    }
    else {
      this.show2 = false;
    }
  }

  isNumber(event) {
    var keycode = event.keyCode;
    console.log(keycode);
    if (keycode >= 48 && keycode <= 57) {
      return true;
    } else {
      return false;
    }
  }

  formCreate = new FormGroup({
    code: new FormControl('', [
      Validators.required,
      Validators.minLength(3),
      Validators.maxLength(50)
    ]),
    description: new FormControl('',[
      Validators.required,
      Validators.minLength(7),
      Validators.maxLength(50)
    ])
  })

  get code() {
    return this.formCreate.get('code');
  }
  get description(){
    return this.formCreate.get('code');
  }


}


