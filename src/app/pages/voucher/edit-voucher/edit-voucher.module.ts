import { EditVoucherComponent,NgbDateCustomParserFormatter } from './edit-voucher.component';
import { VoucherService } from './../../../services/voucher.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {HttpModule} from '@angular/http';
import {SharedModule} from '../../../shared/shared.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CurrencyMaskModule} from 'ng2-currency-mask';
import {BrowserModule} from '@angular/platform-browser';
import { NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';

export const editVoucherRoutes: Routes = [
  {
    path: '',
    component:EditVoucherComponent,
    data: {
      breadcrumb: 'Detail Voucher',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(editVoucherRoutes),
    FormsModule,
    HttpModule,
    SharedModule,
    CurrencyMaskModule,
    ReactiveFormsModule
  ],
  declarations: [EditVoucherComponent],
  providers: [VoucherService ,{provide: NgbDateParserFormatter, useClass: NgbDateCustomParserFormatter}],
  entryComponents: [],
  exports: [],
  bootstrap: []
})
export class EditVoucherModule { }
