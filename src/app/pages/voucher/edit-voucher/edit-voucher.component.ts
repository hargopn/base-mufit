import { formValidationRoutes } from './../../ui-elements/forms/form-validation/form-validation.module';
import { Voucher } from './../../../class/voucher';
import { VoucherService } from './../../../services/voucher.service';
import { Component, OnInit, Injectable } from '@angular/core';
import { SnotifyService, SnotifyPosition, SnotifyToastConfig } from 'ng-snotify';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import * as moment from 'moment';
import { ActivatedRoute } from '@angular/router';
import { NgbDateParserFormatter, NgbDatepickerConfig, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { NgbTimepickerConfig } from '@ng-bootstrap/ng-bootstrap';
import { NgbTimeStruct } from '@ng-bootstrap/ng-bootstrap';
import { isNumber, toInteger, padNumber } from '@ng-bootstrap/ng-bootstrap/util/util';

@Injectable()
export class NgbDateCustomParserFormatter extends NgbDateParserFormatter {
  parse(value: string): NgbDateStruct {
    if (value) {
      const dateParts = value.trim().split('-');
      if (dateParts.length === 1 && isNumber(dateParts[0])) {
        return { day: toInteger(dateParts[0]), month: null, year: null };
      } else if (dateParts.length === 2 && isNumber(dateParts[0]) && isNumber(dateParts[1])) {
        return { day: toInteger(dateParts[0]), month: toInteger(dateParts[1]), year: null };
      } else if (dateParts.length === 3 && isNumber(dateParts[0]) && isNumber(dateParts[1]) && isNumber(dateParts[2])) {
        return { day: toInteger(dateParts[0]), month: toInteger(dateParts[1]), year: toInteger(dateParts[2]) };
      }
    }
    return null;
  }

  format(date: NgbDateStruct): string {
    return date ?
      `${isNumber(date.day) ? padNumber(date.day) : ''}/${isNumber(date.month) ? padNumber(date.month) : ''}/${date.year}` :
      '';
  }
}

@Component({
  selector: 'app-edit-voucher',
  templateUrl: './edit-voucher.component.html',
  styleUrls: ['./edit-voucher.component.css']
})
export class EditVoucherComponent implements OnInit {
  public model1: NgbDateStruct;
  public model2: NgbDateStruct;

  value111;
  quantity111;

  constructor(private config: NgbDatepickerConfig, configTime: NgbTimepickerConfig, private router: Router, private snotifyService: SnotifyService, private route: ActivatedRoute, private service: VoucherService) { }
  timeout = 3000;
  newTop = true;
  dockMax = 4;
  blockMax = 3;
  titleMaxLength = 30;
  bodyMaxLength = 100;

  getConfig(): SnotifyToastConfig {
    this.snotifyService.setDefaults({
      global: {
        newOnTop: this.newTop,
        maxAtPosition: this.blockMax,
        maxOnScreen: this.dockMax,
      }
    });
    return {
      bodyMaxLength: this.bodyMaxLength,
      titleMaxLength: this.titleMaxLength,
      backdrop: -1,
      position: SnotifyPosition.centerTop,
      timeout: this.timeout,
      showProgressBar: false,
      closeOnClick: true,
      pauseOnHover: false
    };

  }


  end_date: any;
  start_date: any;



  public formVoucher = {
    "description": "",
    "code": "",
    "id": "",
    "end_date": 0,
    "start_date": 0,
    "version": "",
    "type": "",
    "value": "",
    "quantity": "",
  }
  public formVoucher2 = {
    "description": "",
    "code": "",
    "id": "",
    "end_date": "",
    "start_date": "",
    "version": "",
    "type": "",
    "value": "",
    "quantity": "",
  }

  today = new Date();
  thisYear = this.today.getFullYear();
  thisMonth = this.today.getMonth() + 1;
  thisDay = this.today.getDate();


  ngOnInit() {
    this.getAll();
  }
  getAll() {
    this.config.minDate = { year: this.thisYear, month: this.thisMonth, day: this.thisDay + 1 };
    const id = this.route.snapshot.paramMap.get('id');
    this.service.getDetailVoucher(id).subscribe(data => {
      console.log(data);

      this.formVoucher = data.result;
      this.formVoucher2 = data.result

      // this.formVoucher2.start_date = moment(this.formVoucher2.start_date).format("DD/MM/YYYY");
      this.formVoucher2.end_date = moment(this.formVoucher2.end_date).format("DD/MM/YYYY");

      // console.log(this.formVoucher);
    });
  }
  reset(){
    this.formVoucher.value = null;
    this.formVoucher.quantity = null;
  }

  editVoucher() {

    const id = this.route.snapshot.paramMap.get('id');

    // this.formVoucher.value = this.value111;
    // this.formVoucher.quantity = this.quantity111;


    // let tampungDate = parseFloat(this.model1.year + "/" + this.model1.month + "/" + this.model1.day).toFixed;
    // let epochDate = new Date(tampungDate).getTime();
    // this.formVoucher.start_date = epochDate;

    // let tampungDate2 = this.model2.year + "/" + this.model2.month + "/" + this.model2.day;
    // let epochDate2 = new Date(tampungDate2).getTime();
    // this.formVoucher.end_date = epochDate2;
    // let tampungDate = this.model1.year + "/" + this.model1.month + "/" + this.model1.day;
    // let epochDate = new Date(tampungDate).getTime();
    // this.formVoucher.start_date = epochDate;

    

    if (this.model2 === undefined) {
      this.snotifyService.error('Please Select Date', this.getConfig());
    }else if(this.formVoucher.value === null){
      this.snotifyService.error('Please fill the amount', this.getConfig());
    }else if(this.formVoucher.quantity === null){
      this.snotifyService.error('Please fill the quantity', this.getConfig());
    }else {
      let tampungDate2 = this.model2.year + "/" + this.model2.month + "/" + this.model2.day;
      let epochDate2 = new Date(tampungDate2).getTime();
      this.formVoucher.end_date = epochDate2;

      let request =
        {
          "code": this.formVoucher.code,
          "description": this.formVoucher.description,
          "end_date": this.formVoucher.end_date,
          "start_date": this.formVoucher.start_date,
          "quantity": this.formVoucher.quantity,
          "type": this.formVoucher.type,
          "value": this.formVoucher.value,
          "version": this.formVoucher.version,
          "id": this.formVoucher.id
        }
      console.log(request)
      this.service.editVoucher(request, id).subscribe(data => {
        if (data.message != 'ERROR') {

          console.log(request);

          this.snotifyService.success('Edit Voucher Success!', this.getConfig());
          setTimeout(() => {
            this.router.navigate(['/voucher/list'])
          }, 4000);
        } else {
          this.snotifyService.error(data.result, this.getConfig());
        }
        console.log(this.formVoucher)
      })
    }
  }

}
