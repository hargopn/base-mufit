import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {SharedModule} from '../../shared/shared.module';
import { EditComponent } from './edit/edit.component';
import { RatingReviewComponent } from './rating-review/rating-review.component';

export const UserRoutes: Routes = [
  {
    path: '',
    data: {
      breadcrumb: '',
      status: false
    },
    children: [
      {
        path: 'create',
        loadChildren: './create/create.module#CreateModule'
      },
      {
        path: 'rating-review',
        loadChildren: './rating-review/rating-review.module#RatingReviewModule'
      },
      {
        path: 'list',
        loadChildren: './list/list.module#ListModule'
      },
      {
        path: 'profile/:id',
        loadChildren: './profile/profile.module#ProfileModule'
      },
      {
        path: 'edit/:id',
        loadChildren: './edit/edit.module#EditModule'
      }
    ]
  }
];
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(UserRoutes),
    SharedModule
  ],
  declarations: []
})
export class TrainerModule { }
