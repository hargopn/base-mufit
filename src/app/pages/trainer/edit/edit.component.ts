import { Component, OnInit, Input } from '@angular/core';
import { NgModel, NgForm, FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Configuration } from '../../../services/base-services/configuration';
import { TrainerService } from '../../../services/trainer.service';
import { SnotifyService, SnotifyPosition, SnotifyToastConfig } from 'ng-snotify';
import { Router, ActivatedRoute } from '@angular/router';
import * as moment from 'moment';
import { NgbTimepickerConfig } from '@ng-bootstrap/ng-bootstrap';
import { ListSpeciality } from './../../../class/listSpeciality';
// import { ListSpeciality } from './../listSpeciality';


@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {

  public filePhoto: any;
  public detailTrainer = {
    "bank": {
      "id": "",
      "version": "",
      "name": ""
    },
    "no_rek": "",
    "address": "",
    "email": "",
    "gender": "",
    "name": "",
    "no_ktp": "",
    "no_npwp": "",
    "phone": "",
    "day": [],
    "trainer_speciality": [],
    "speciality_list": [],
    "schedule_list": [],
    "id": "",
    "version": "",
    "url_photo_trainer": "",
  }
  public formCreateTrainer = {
    "no_rek": "",
    "bank": "",
    "address": "",
    "email": "",
    "gender": "",
    "name": "",
    "no_ktp": "",
    "no_npwp": "",
    "phone": "",
    "schedule_list": [],
    "speciality_list": []
  }
  public formCreateTrainerRequest = {
    "no_rek": "",
    "bank": "",
    "address": "",
    "email": "",
    "gender": "",
    "name": "",
    "no_ktp": "",
    "no_npwp": "",
    "phone": "",
    "schedule_list": [],
    "speciality_list": []
  }

  public listDay = [
    {
      "name": "Monday", "value": false
    },
    {
      "name": "Tuesday", "value": false
    },
    {
      "name": "Wednesday", "value": false
    },
    {
      "name": "Thursday", "value": false
    },
    {
      "name": "Friday", "value": false
    },
    {
      "name": "Saturday", "value": false
    },
    {
      "name": "Sunday", "value": false
    }]

  shift_range: any;
  endTime: any;
  startTime: any;
  price: number;
  public value1 = false;
  public valueDetail = false;
  public data: any;
  public id: any;
  _ref: any;
  public speciality: any;
  public shift: any;
  public bank: any;



  timeout = 3000;
  newTop = true;
  dockMax = 4;
  blockMax = 3;
  titleMaxLength = 30;
  bodyMaxLength = 100;





  @Input() lSpeciality: ListSpeciality;

  constructor(
    private trainerService: TrainerService,
    private snotifyService: SnotifyService,
    private route: ActivatedRoute,
    private router: Router,
    private config: NgbTimepickerConfig
  ) {
    this.config.seconds = false;
    this.config.spinners = true;
  }

  ngOnInit() {
    window.scrollTo(0,0);
    this.getSpecialities();
    this.getBank();
  }

  getAll() {
    const id = this.route.snapshot.paramMap.get('id');
    this.trainerService.getTrainerById(id)
      .subscribe(posts => {
        this.detailTrainer = posts.result;
        if(this.detailTrainer.url_photo_trainer=='' ){
          this.detailTrainer.url_photo_trainer = 'https://file.mufit.id/trainer/2dded528-779f-4ab6-9804-283611af19de.png';
        }
        console.log(this.detailTrainer.trainer_speciality)
        console.log(this.data)
        for (let i = 0; i < this.data.length; i++) {

          let j = this.detailTrainer.trainer_speciality.map(function (e) { return e.name; }).indexOf(this.data[i].name);
          console.log(j)
          if (j !== -1) {
            this.data[i].value = true;
          }
          // if()
        }
        for (let j = 0; j < this.listDay.length; j++) {
          for (let y = 0; y < this.detailTrainer.day.length; y++) {
            for (let z = 0; z < this.detailTrainer.day[y].shift.length; z++) {
              if (this.listDay[j].name === this.detailTrainer.day[y].name) {
                console.log(this.listDay[j].name)
                this.listDay[j].value = true;
              }
            }
          }
        }


      })



  }
  uploadPhotoTrainer(event) {
    this.filePhoto = event.target.files[0];
    if (this.checkFileExtTr()) {
      this.snotifyService.error('Image must be in .jpg / .png / .jpeg', this.getConfig());
      this.filePhoto = null;
    } else {
      let fd = new FormData();
      fd.append('file', this.filePhoto);
      this.trainerService.uploadImg(fd).subscribe(post => {
        if (post.message != 'ERROR') {
          this.detailTrainer.url_photo_trainer = post["result"];
        } else {
          this.snotifyService.error(post.result, this.getConfig());
        }
      });
    }
  }

  checkFileExtTr() {
    let nameX = this.filePhoto.name.split(".");
    let ext = nameX[nameX.length - 1];
    if (!(ext == 'jpg' || ext == 'JPG' || ext == 'png' || ext == 'PNG' || ext == 'jpeg' || ext == 'JPEG')) {
      return true;
    } else {
      return false;
    }
  }

  getSpecialities() {
    this.trainerService.getSpecialities()
      .subscribe(posts => {
        this.data = posts.result;
        for (let i = 0; i < this.data.length; i++) {
          this.data[i].value = false;
        }
        this.getAll();
      })
  }

  isNumber(event){
    var keycode = event.keyCode;
    console.log(keycode);
    if(keycode>=48 && keycode<=57){
      return true;
    } else {
      return false;
    }
  }

  getBank() {
    this.trainerService.getBank().subscribe(posts => {
      this.bank = posts.result;
    })
  }

  checkedSpeciality(value, name) {
    console.log(value)

    console.log(name)


    if (value.target.checked === true) {
      let input = {
        "id": name.id,
        "price": name.price,
        "version": name.version,
        "name": name.name
      }
      this.detailTrainer.trainer_speciality.push(input)
      console.log(name.name)
      console.log(name.value)
      // }else if(this.data.value === true){

    } else if (this.data.value === false) {

      for (let i = 0; i < this.data.length; i++) {
        for (let x = 0; x < this.detailTrainer.trainer_speciality.length; x++) {
          this.speciality = this.detailTrainer.trainer_speciality;
          if (this.data[i].name === this.detailTrainer.trainer_speciality[x].name) {
            console.log(this.data[i].name)
            console.log(this.data[i].value)
            if (this.data[i].value === false) {
              this.speciality.splice(this.data[i], 1);
            }
          }
        }
      }


    } else {

      let i = this.detailTrainer.trainer_speciality.map(function (e) { return e.name; }).indexOf(name.name);
      console.log(i)
      if (i != -1) {
        this.detailTrainer.trainer_speciality.splice(i, 1)

      }
      console.log(this.data)
    }
    console.log(this.detailTrainer)
  }

  removeObjectX(index): void {

    this.detailTrainer.day.splice(index, 1);
  }

  removeObject(value1, value2, test): void {
    for (let i = 0; i < this.detailTrainer.day.length; i++) {
      if (this.detailTrainer.day[i].name === test.name) {
        let j = this.detailTrainer.day[i].shift.map(function (e) { return e.start_time; }).indexOf(value1);
        let k = this.detailTrainer.day[i].shift.map(function (e) { return e.end_time; }).indexOf(value2);
        if (j === k) {
          this.detailTrainer.day[i].shift.splice(j, 1);
        }
      }
    }
  }

  // checked(value, name) {
  //   if (name === "All") {
  //     console.log(this.value1)
  //     if (this.value1 === true) {
  //       if (this.formCreateTrainer.schedule_list.length < 1) {
  //         for (let z = 0; z < this.listDay.length; z++) {

  //           let input = {
  //             "day": this.listDay[z].name,
  //             "shift": []
  //           };
  //           this.listDay[z].value = true;
  //           this.formCreateTrainer.schedule_list.push(input)
  //         }
  //       }
  //       else {
  //         for (let i = 0; i < this.listDay.length; i++) {
  //           let j = this.formCreateTrainer.schedule_list.map(function (e) { return e.day; }).indexOf(this.listDay[i].name);
  //           console.log(j)
  //           if (j == -1) {
  //             let input = {
  //               "day": this.listDay[i].name,
  //               "shift": []
  //             };
  //             this.listDay[i].value = true;
  //             this.formCreateTrainer.schedule_list.push(input)
  //           }

  //         }
  //       }
  //     } else {
  //       for (let i = 0; i < this.listDay.length; i++) {
  //         this.listDay[i].value = false;

  //         let j = this.formCreateTrainer.schedule_list.map(function (e) { return e.day; }).indexOf(this.listDay[i].name);
  //         this.formCreateTrainer.schedule_list.splice(j, 1)
  //       }

  //     }
  //   }
  //   else {

  //     let input = {
  //       "day": name,
  //       "shift": []
  //     };
  //     if (value.target.checked === true) {
  //       this.formCreateTrainer.schedule_list.push(input)
  //     } else {
  //       let i = this.formCreateTrainer.schedule_list.map(function (e) { return e.day; }).indexOf(name);
  //       this.formCreateTrainer.schedule_list.splice(i, 1)
  //     }
  //   }
  // }


  checked(value, name) {
    if (name === "All") {
      console.log(this.value1)
      if (this.value1 === true) {
        if (this.detailTrainer.day.length < 1) {
          for (let z = 0; z < this.listDay.length; z++) {

            let input = {
              "day": this.listDay[z].name,
              "shift": []
            };
            this.listDay[z].value = true;
            this.detailTrainer.day.push(input)
          }
        }
        else {
          for (let i = 0; i < this.listDay.length; i++) {
            let j = this.detailTrainer.day.map(function (e) { return e.day; }).indexOf(this.listDay[i].name);
            console.log(j)
            if (j == -1) {
              let input = {
                "day": this.listDay[i].name,
                "shift": []
              };
              this.listDay[i].value = true;
              this.detailTrainer.day.push(input)
            }

          }
        }
      } else {

        for (let i = 0; i < this.listDay.length; i++) {
          this.listDay[i].value = false;

          let j = this.detailTrainer.day.map(function (e) { return e.day; }).indexOf(this.listDay[i].name);
          this.detailTrainer.day.splice(j, 1)
        }

      }
    }
    else {

      let input = {
        "name": name,
        "shift": []
      };
      if (value.target.checked === true) {
        this.detailTrainer.day.push(input)
        //DISINI WOY
        // }else if(this.listDay.value === false){
        //
        //   for(let j=0; j<this.listDay.length;j++){
        //     for(let y=0;y<this.detailTrainer.day.length;y++){
        //       for(let z=0; z<this.detailTrainer.day[y].shift.length;z++){
        //         if(this.listDay[j].name === this.detailTrainer.day[y].name){
        //           console.log(this.listDay[j].name)
        //           this.listDay[j].value = true;
        //         }
        //       }
        //     }
        //   }
      } else {
        let i = this.detailTrainer.day.map(function (e) { return e.name; }).indexOf(name);

        this.detailTrainer.day.splice(i, 1)
      }
    }
  }
  addComponent(value, start, end) {
    console.log(value)
    console.log(start)
    console.log(end)
    if (start === undefined) {
      this.snotifyService.error('Please Input Start Time First', this.getConfig());
    } else if (end === undefined) {
      this.snotifyService.error('Please Input End Time First', this.getConfig());

    }
    else if (start.hour < 7 || start.hour > 21) {

      this.snotifyService.error('Start Time Must Between Range 7 AM - 8 PM', this.getConfig());

    }
    else if (end.hour < 8 || end.hour > 22) {

      this.snotifyService.error('End Time Must Between Range 8 AM - 10 PM', this.getConfig());

    } else if (end.hour < start.hour) {
      this.snotifyService.error('End Time Must Greater Than Start Time', this.getConfig());

    }
    else {
      for (let i = 0; i < this.detailTrainer.day.length; i++) {
        if (this.detailTrainer.day[i].name === value) {
          this.shift_range = end.hour - start.hour;
          let selMinit = end.minute - start.minute;
          if (selMinit >= 30) {
            this.shift_range += 1
          }
          console.log(this.shift_range);
          console.log("masuk")
          this.detailTrainer.day[i].shift.push({ 'start_time': start, 'end_time': end, 'shift_range': this.shift_range });
          this.detailTrainer.day[i].start_time = undefined;
          this.detailTrainer.day[i].end_time = undefined;
        }
      }
    }
  }
  getConfig(): SnotifyToastConfig {
    this.snotifyService.setDefaults({
      global: {
        newOnTop: this.newTop,
        maxAtPosition: this.blockMax,
        maxOnScreen: this.dockMax,
      }
    });
    return {
      bodyMaxLength: this.bodyMaxLength,
      titleMaxLength: this.titleMaxLength,
      backdrop: -1,
      position: SnotifyPosition.centerTop,
      timeout: this.timeout,
      showProgressBar: false,
      closeOnClick: true,
      pauseOnHover: false
    };

  }

  cancel(){
    const id = this.route.snapshot.paramMap.get('id');
    this.router.navigate(['/trainer/profile/' + id]);
  }

  updateContact() {
    // const id = this.route.snapshot.paramMap.get('id');
    // this.trainerService.updateTrainer(this.formCreateTrainer, this.detailTrainer, id)
    //   .subscribe()
    // this.detailTrainer.trainer_speciality
    this.detailTrainer.speciality_list = [];
    this.detailTrainer.schedule_list = [];
    let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (this.detailTrainer.name == '') {
      this.snotifyService.error('Please Input Trainer Name', this.getConfig());
    } else if (this.detailTrainer.name.length < 3) {
      this.snotifyService.error('Trainer Name Must Be Between 3 - 50 Characters', this.getConfig());
    } else if (this.detailTrainer.email == '') {
      this.snotifyService.error('Please Input Trainer Email', this.getConfig());
    } else if (!re.test(String(this.detailTrainer.email).toLowerCase())) {
      this.snotifyService.error('Please Input Valid Email', this.getConfig());
    } else if (this.detailTrainer.gender == '') {
      this.snotifyService.error('Please Choose Trainer Gender', this.getConfig());
    } else if (this.detailTrainer.address == '') {
      this.snotifyService.error('Please Input Trainer Address', this.getConfig());
    } else if (this.detailTrainer.address.length < 15) {
      this.snotifyService.error('Trainer Address is too short', this.getConfig());
    } else if (this.detailTrainer.address.length > 50) {
      this.snotifyService.error('Trainer Address is too long', this.getConfig());
    } else if (this.detailTrainer.phone == '') {
      this.snotifyService.error('Please Input Trainer Phone Number', this.getConfig());
    } else if (this.detailTrainer.no_ktp == '') {
      this.snotifyService.error('Please Input KTP Trainer', this.getConfig());
    } else if (this.detailTrainer.no_ktp.length < 16) {
      this.snotifyService.error('No KTP Must 16 Digit', this.getConfig());
    } else {
      for (let i = 0; i < this.detailTrainer.trainer_speciality.length; i++) {
        this.detailTrainer.trainer_speciality[i].price = parseInt(this.detailTrainer.trainer_speciality[i].price)
        console.log(this.detailTrainer.trainer_speciality[i].id_speciality)
        let request2;
        if(this.detailTrainer.trainer_speciality[i].id_speciality ==undefined){
          request2= {
          "id": this.detailTrainer.trainer_speciality[i].id,
          "version": this.detailTrainer.trainer_speciality[i].version,
          "price": this.detailTrainer.trainer_speciality[i].price,
          "name": this.detailTrainer.trainer_speciality[i].name
        };
        }else {
          request2 = {
          "id": this.detailTrainer.trainer_speciality[i].id_speciality,
          "version": this.detailTrainer.trainer_speciality[i].version,
          "price": this.detailTrainer.trainer_speciality[i].price,
          "name": this.detailTrainer.trainer_speciality[i].name
        };
        }
        
        this.detailTrainer.speciality_list.push(request2)
      }
      for (let j = 0; j < this.detailTrainer.day.length; j++) {

        delete this.detailTrainer.day[j].start_time;
        delete this.detailTrainer.day[j].end_time;
        console.log(this.detailTrainer.day[j])
        for (let z = 0; z < this.detailTrainer.day[j].shift.length; z++) {
          delete this.detailTrainer.day[j].shift[z].id;
          delete this.detailTrainer.day[j].shift[z].version;
          console.log(this.detailTrainer.day[j].shift[z])
        }

        let request1 = {
          "id": this.detailTrainer.day[j].id,
          "version": this.detailTrainer.day[j].version,
          "day": this.detailTrainer.day[j].name,
          "shift": this.detailTrainer.day[j].shift
        }

        this.detailTrainer.schedule_list.push(request1)
      }

      let request =
        {
          "no_rek": this.detailTrainer.no_rek,
          "bank": this.detailTrainer.bank.name,
          "address": this.detailTrainer.address,
          "email": this.detailTrainer.email,
          "gender": this.detailTrainer.gender,
          "name": this.detailTrainer.name,
          "no_ktp": this.detailTrainer.no_ktp,
          "no_npwp": this.detailTrainer.no_npwp,
          "phone": this.detailTrainer.phone,
          "schedule_list": this.detailTrainer.schedule_list,
          "trainer_speciality": this.detailTrainer.speciality_list,
          "id": this.detailTrainer.id,
          "version": this.detailTrainer.version,
          "url_photo_trainer": this.detailTrainer.url_photo_trainer
        }
      console.log(request)
      const id = this.route.snapshot.paramMap.get('id');

      this.trainerService.updateTrainer(request, id).subscribe(data => {
        if (data.message != 'ERROR') {

          this.snotifyService.success('Update Trainer Success!', this.getConfig());


          // alert("Berhasil login!");
          setTimeout(() => {
            const id = this.route.snapshot.paramMap.get('id');
            this.router.navigate(['/trainer/profile/' + id])
          }, 4000);
          // this.router.navigateByUrl('/dashboard');
        } else {
          this.snotifyService.error(data.result, this.getConfig());
        }
      })
    }



  }

  formCreate = new FormGroup({
    email: new FormControl('', [
      Validators.required,
      Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$')
    ]),
    emailT: new FormControl('', [
      Validators.required,
      Validators.pattern(/[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,5}$/)
    ]),
    fullName: new FormControl('', [
      Validators.pattern(/^[a-z A-Z]*$/),
      Validators.required,
      Validators.minLength(3),
      Validators.maxLength(50)
    ]),
    nameT: new FormControl('', [
      Validators.required,
      Validators.minLength(3),
      Validators.maxLength(50),
      Validators.pattern(/^[a-z A-Z]*$/),
    ]),
    username: new FormControl('', [
      Validators.required,
      Validators.minLength(3),
      Validators.maxLength(50)
    ]),
    password: new FormControl('', [
      Validators.required,
      Validators.minLength(8),
    ]),
    address: new FormControl('', [
      Validators.required,
      Validators.minLength(15),
      Validators.maxLength(50)
    ]),
    addressT: new FormControl('', [
      Validators.required,
      Validators.minLength(15),
      Validators.maxLength(50)
    ]),
    noKtp: new FormControl('', [
      Validators.required,
      Validators.pattern('^[0-9]+$'),
      Validators.minLength(16),
      Validators.maxLength(16)
    ]),
    noKtpT: new FormControl('', [
      Validators.required,
      Validators.pattern('^[0-9]+$'),
      Validators.minLength(16),
      Validators.maxLength(16)
    ]),
    noNpwp: new FormControl('', [
      Validators.required,
      Validators.pattern('^[0-9]+$'),
      Validators.minLength(15),
      Validators.maxLength(15)
    ]),
    noNpwpT: new FormControl('', [
      Validators.required,
      Validators.pattern('^[0-9]+$'),
      Validators.minLength(15),
      Validators.maxLength(15)
    ]),
    phoneT: new FormControl('', [
      Validators.required,
      Validators.pattern('[0][8][0-9]{8,11}'),
      Validators.minLength(10),
      Validators.maxLength(15)
    ]),
    noRekT: new FormControl('', [
      Validators.required,
      Validators.pattern('^[0-9]+$'),
      Validators.minLength(10),
      Validators.maxLength(15)
    ]),
    // price : new FormControl('',[
    //   Validators.required,
    //   Validators.pattern('[0][8][0-9]{8,11}'),
    //   Validators.minLength(10),
    //   Validators.maxLength(15)
    // ]),

  })

  get email() {
    return this.formCreate.get('email');
  }
  get emailT() {
    return this.formCreate.get('emailT');
  }
  get fullName() {
    return this.formCreate.get('fullName');
  }
  get nameT() {
    return this.formCreate.get('nameT');
  }
  get username() {
    return this.formCreate.get('username');
  }
  get password() {
    return this.formCreate.get('password');
  }
  get address() {
    return this.formCreate.get('address');
  }
  get addressT() {
    return this.formCreate.get('addressT');
  }
  get noKtp() {
    return this.formCreate.get('noKtp');
  }
  get noNpwp() {
    return this.formCreate.get('noNpwp');
  }
  get noKtpT() {
    return this.formCreate.get('noKtpT');
  }
  get noNpwpT() {
    return this.formCreate.get('noNpwpT');
  }
  get phoneT() {
    return this.formCreate.get('phoneT');
  }
  get noRekT() {
    return this.formCreate.get('noRekT');
  }
}
