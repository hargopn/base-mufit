import { TrainerService } from './../../../services/trainer.service';
import { Trainer } from './../../../class/trainer';
import { CallService } from "./../../../services/base-services/call.service";
import { Component, OnInit, Input } from "@angular/core";
@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  public idTrainer;

  public page: number = 1; //page
  public limit: number = 25; //itemsPerPage
  public totalItems: number; //elements

  public search="";

  public previousPage: any;

  trainers: Trainer[];

  constructor(private service: CallService, private trainerService: TrainerService) {}

  ngOnInit() {
    window.scrollTo(0,0);
    this.loadData();
  }
  
  loadData() {
    this.trainerService.getPaginate(this.page - 1, this.limit, this.search).subscribe(posts => {
      this.trainers = posts["result"];
      this.totalItems = posts["elements"];
      console.log(this.trainers, this.totalItems);
    });
  }

  loadPage(page: number) {
    if (page !== this.previousPage) {
      this.previousPage = page;
      this.loadData();
    }
  }

  //Show limit list
  getSelect(limitnya) {
    this.trainerService.getPaginate(this.page, limitnya, this.search).subscribe(posts => {
      this.limit = limitnya;
      this.loadData();
    });
  }

  onChange(deviceValue) {
    this.getSelect(deviceValue);
  }

  //Search
  getSearch(query){
    this.search = query;
    this.loadData();
  }
}