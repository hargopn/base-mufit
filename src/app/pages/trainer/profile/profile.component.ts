import { Component, OnInit, Input } from '@angular/core';
import { NgModel, NgForm } from '@angular/forms';
import { Configuration } from '../../../services/base-services/configuration';
import { TrainerService } from '../../../services/trainer.service';
import { SnotifyService, SnotifyPosition, SnotifyToastConfig } from 'ng-snotify';
import { Router } from '@angular/router';
import * as moment from 'moment';
import { NgbTimepickerConfig } from '@ng-bootstrap/ng-bootstrap';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {


timeout = 3000;
newTop = true;
dockMax = 4;
blockMax = 3;
titleMaxLength = 30;
bodyMaxLength = 100;

  public detailTrainer = {
    "bank": {
      "id": "",
      "version": "",
      "name": ""
    },
    "no_rek":"",
    "address": "",
    "email": "",
    "gender": "",
    "name": "",
    "rating": "",
    "no_ktp": "",
    "no_npwp": "",
    "phone": "",
    "trainer_specialitys": "",
    "schedule_list": [],
    "url_photo_trainer": "",
    "day": [
      {
        "id": "",
        "name": "",
        "shift": [
          {
            "id": "",
            "start_time": {
              "hour": "",
              "minute": "",
              "second": ""
            },
            "end_time": {
              "hour": "",
              "minute": "",
              "second": ""
            },
            "shift_range":""
          }
        ]
      }
    ],
    "trainer_speciality": [
      {
        "id": "",
        "version": "",
        "price": "",
        "name": ""
      }
    ]
  }
  // public detailTrainerRequest = {
  //   "address": "",
  //   "email": "",
  //   "gender": "",
  //   "name": "",
  //   "no_ktp": "",
  //   "no_npwp": "",
  //   "phone": "",
  //   "schedule_list": [],
  //   "speciality_list": [
  //     {
  //       "id": "",
  //       "version": "",
  //       "price": "",
  //       "name": ""
  //     }
  //   ]
  // }
  public data: any;
  public id: any;

  public listDay = [
    {
      "name": "Monday", "value": false
    },
    {
      "name": "Tuesday", "value": false
    },
    {
      "name": "Wednesday", "value": false
    },
    {
      "name": "Thursday", "value": false
    },
    {
      "name": "Friday", "value": false
    },
    {
      "name": "Saturday", "value": false
    },
    {
      "name": "Sunday", "value": false
    }]
  endTime: any;
  startTime: any;
  price: number;
  public value1 = false;

  constructor(private route: ActivatedRoute, private trainerService: TrainerService, private snotifyService: SnotifyService, private router: Router, private config: NgbTimepickerConfig) {
  }

  ngOnInit() {
    // this.getAll();
    window.scrollTo(0,0);
    this.getTrainer();
    // this.getData
  }
  getTrainer(): void {
    const id = this.route.snapshot.paramMap.get('id');
    console.log(id)
    // this.trainerService.getDetailTrainer(id)
    //   .subscribe(post => this.detailTrainer = post.result);

      this.trainerService.getDetailTrainer(id).subscribe(data => {
        if(data.message != 'ERROR') {

        this.detailTrainer = data.result;
        if(this.detailTrainer.url_photo_trainer=='' ){
          this.detailTrainer.url_photo_trainer = 'https://file.mufit.id/trainer/2dded528-779f-4ab6-9804-283611af19de.png';
        }
          // this.router.navigateByUrl('/dashboard');
        } else {
          this.snotifyService.error(data.result,this.getConfig());
        }
      console.log(this.detailTrainer)

      })
  }


  goToEditDetail(){

    const id = this.route.snapshot.paramMap.get('id');
    this.router.navigate(['/trainer/edit/'+id])
  }
  getConfig(): SnotifyToastConfig {
    this.snotifyService.setDefaults({
      global: {
        newOnTop: this.newTop,
        maxAtPosition: this.blockMax,
        maxOnScreen: this.dockMax,
      }
    });
    return {
      bodyMaxLength: this.bodyMaxLength,
      titleMaxLength: this.titleMaxLength,
      backdrop: -1,
      position: SnotifyPosition.centerTop,
      timeout: this.timeout,
      showProgressBar: false,
      closeOnClick: true,
      pauseOnHover: false
    };

  }

  // getData(){

  //   const id= this.router.get('id');
  //   console.log(id);
  //   this.trainerService.getTrainerById(id).subscribe(post=> this.detailTrainer = post.result);
  // }

}
