import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {HttpModule} from '@angular/http';
import {SharedModule} from '../../../shared/shared.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CurrencyMaskModule} from 'ng2-currency-mask';
import {BrowserModule} from '@angular/platform-browser';
import {ProfileComponent} from './profile.component'

import { TrainerService } from '../../../services/trainer.service';

export const TrainerProfileRoutes: Routes = [
  {
    path: '',
    component:ProfileComponent,
    data: {
      breadcrumb: 'Create Trainer',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(TrainerProfileRoutes),
    FormsModule,
    HttpModule,
    SharedModule,
    CurrencyMaskModule,
    ReactiveFormsModule
  ],
  declarations: [ProfileComponent],
  providers: [TrainerService],
  entryComponents: [],
  exports: [],
  bootstrap: []
})
export class ProfileModule { }
