import { AdminService } from './../../../services/admin.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {HttpModule} from '@angular/http';
import {SharedModule} from '../../../shared/shared.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CurrencyMaskModule} from 'ng2-currency-mask';
import {BrowserModule} from '@angular/platform-browser';
import { CreateComponent } from './create.component';

import { TrainerService } from '../../../services/trainer.service';


export const RoleCreateRoutes: Routes = [
  {
    path: '',
    component:CreateComponent,
    data: {
      breadcrumb: 'Create Trainer',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(RoleCreateRoutes),
    FormsModule,
    HttpModule,
    SharedModule,
    CurrencyMaskModule,
    ReactiveFormsModule
  ],
  declarations: [CreateComponent],
  providers: [TrainerService,AdminService],
  entryComponents: [],
  exports: [],
  bootstrap: []
})
export class CreateModule { }
