import { AdminService } from './../../../services/admin.service';
import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { NgModel, NgForm, FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Configuration } from '../../../services/base-services/configuration';
import { TrainerService } from '../../../services/trainer.service';
import { UserService } from '../../../services/user.service';
import { SnotifyService, SnotifyPosition, SnotifyToastConfig } from 'ng-snotify';
import { Router } from '@angular/router';
import * as moment from 'moment';
import { NgbTimepickerConfig } from '@ng-bootstrap/ng-bootstrap';
import { animate, style, transition, trigger } from '@angular/animations';
import { subscribeOn } from 'rxjs/operator/subscribeOn';
import { FileUploader } from 'ng2-file-upload';
import { NgbTabsetConfig } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css'],
  animations: [
    trigger('fadeInOutTranslate', [
      transition(':enter', [
        style({ opacity: 0 }),
        animate('400ms ease-in-out', style({ opacity: 1 }))
      ]),
      transition(':leave', [
        style({ transform: 'translate(0)' }),
        animate('400ms ease-in-out', style({ opacity: 0 }))
      ])
    ])
  ]
})

export class CreateComponent implements OnInit {

  public fileKtp: any;
  public fileNpwp: any;
  public filePhoto: any;

  public trainer: any;
  public fileImg: any;




  public formCreateTrainer = {
    "no_rek": "",
    "bank": "",
    "address": "",
    "email": "",
    "gender": "",
    "name": "",
    "no_ktp": "",
    "no_npwp": "",
    "phone": "",
    "schedule_list": [],
    "speciality_list": [],
    "url_photo_trainer": "https://file.mufit.id/trainer/2dded528-779f-4ab6-9804-283611af19de.png"

  }
  public formCreateTrainerRequest = {
    "no_rek": "",
    "bank": "",
    "address": "",
    "email": "",
    "gender": "",
    "name": "",
    "no_ktp": "",
    "no_npwp": "",
    "phone": "",
    "schedule_list": [],
    "speciality_list": [],
    "url_photo_trainer": "https://file.mufit.id/trainer/2dded528-779f-4ab6-9804-283611af19de.png"
  }

  public formCreateAdmin = {
    "username": "",
    "address": "",
    "password": "",
    "email": "",
    "full_name": "",
    "no_ktp": "",
    "no_npwp": "",
    "url_ktp": "https://file.mufit.id/trainer/2dded528-779f-4ab6-9804-283611af19de.png",
    "url_npwp": "https://file.mufit.id/trainer/2dded528-779f-4ab6-9804-283611af19de.png",
  }
  public formCreateAdminRequest = {
    "username": "",
    "password": "",
    "address": "",
    "email": "",
    "full_name": "",
    "no_ktp": "",
    "no_npwp": "",
    "url_ktp": "https://file.mufit.id/trainer/2dded528-779f-4ab6-9804-283611af19de.png",
    "url_npwp": "https://file.mufit.id/trainer/2dded528-779f-4ab6-9804-283611af19de.png",
  }

  public formCreateCustomer = {
    "id": "",
    "version": "",
    "email": "",
    "full_name": "",
    "address": "",
    "phone": "",
    "date_created": "",
    "photo_selfie": "https://file.mufit.id/trainer/2dded528-779f-4ab6-9804-283611af19de.png"

  }
  public data: any;
  public bank: any;

  public id: any;

  public listDay = [
    {
      "name": "Monday", "value": false
    },
    {
      "name": "Tuesday", "value": false
    },
    {
      "name": "Wednesday", "value": false
    },
    {
      "name": "Thursday", "value": false
    },
    {
      "name": "Friday", "value": false
    },
    {
      "name": "Saturday", "value": false
    },
    {
      "name": "Sunday", "value": false
    }]
  endTime: any;
  startTime: any;
  price: number;
  public value1 = false;
  public fileCust: any;

  shift_range: any;

  _ref: any;

  timeout = 3000;
  newTop = true;
  dockMax = 4;
  blockMax = 3;
  titleMaxLength = 30;
  bodyMaxLength = 100;

  public photo_selfiex: any;
  customerForm: FormGroup;

  constructor(
    private trainerService: TrainerService,
    private AdminService: AdminService,
    private UserService: UserService,
    private snotifyService: SnotifyService,
    private router: Router,
    private config: NgbTimepickerConfig,
    // config2: NgbTabsetConfig,
    private FormBuilder: FormBuilder,
    private cd: ChangeDetectorRef,
  ) {

    // config2.justify = 'center';
    // config2.type = 'pills';

    this.config.seconds = false;
    this.config.spinners = true;

    this.customerForm = FormBuilder.group({
      'full_name': [
        null,
        Validators.compose([
          Validators.required,
          Validators.minLength(3),
          Validators.maxLength(50),
          Validators.pattern(/^[a-z A-Z]*$/)
        ])

      ],
      'email': [
        null,
        Validators.compose([
          Validators.required,
          Validators.pattern(/[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,5}$/)
        ])
      ],
      'address': [
        null,
        Validators.compose([
          Validators.required,
          Validators.minLength(15),
          Validators.maxLength(50)
        ])
      ],
      'phone': [
        null,
        Validators.compose([
          Validators.required,
          Validators.minLength(10),
          Validators.maxLength(13),
          Validators.pattern(/[0][8][0-9]{8,11}$/)
        ])
      ],
    });
  }



  getConfig(): SnotifyToastConfig {
    this.snotifyService.setDefaults({
      global: {
        newOnTop: this.newTop,
        maxAtPosition: this.blockMax,
        maxOnScreen: this.dockMax,
      }
    });
    return {
      bodyMaxLength: this.bodyMaxLength,
      titleMaxLength: this.titleMaxLength,
      backdrop: -1,
      position: SnotifyPosition.centerTop,
      timeout: this.timeout,
      showProgressBar: false,
      closeOnClick: true,
      pauseOnHover: false
    };

  }

  removeObjectX(index): void {

    this.formCreateTrainer.schedule_list.splice(index, 1);
  }

  removeObject(value1, value2, test): void {
    for (let i = 0; i < this.formCreateTrainer.schedule_list.length; i++) {
      if (this.formCreateTrainer.schedule_list[i].day === test.day) {
        let j = this.formCreateTrainer.schedule_list[i].shift.map(function (e) { return e.start_time; }).indexOf(value1);
        let k = this.formCreateTrainer.schedule_list[i].shift.map(function (e) { return e.end_time; }).indexOf(value2);
        if (j === k) {
          this.formCreateTrainer.schedule_list[i].shift.splice(j, 1);
        }
        // }
      }
    }
  }
  checked(value, name) {
    if (name === "All") {
      console.log(this.value1)
      if (this.value1 === true) {
        if (this.formCreateTrainer.schedule_list.length < 1) {
          for (let z = 0; z < this.listDay.length; z++) {

            let input = {
              "day": this.listDay[z].name,
              "shift": []
            };
            this.listDay[z].value = true;
            this.formCreateTrainer.schedule_list.push(input)
          }
        }
        else {
          for (let i = 0; i < this.listDay.length; i++) {
            let j = this.formCreateTrainer.schedule_list.map(function (e) { return e.day; }).indexOf(this.listDay[i].name);
            console.log(j)
            if (j == -1) {
              let input = {
                "day": this.listDay[i].name,
                "shift": []
              };
              this.listDay[i].value = true;
              this.formCreateTrainer.schedule_list.push(input)
            }

          }
        }
      } else {
        for (let i = 0; i < this.listDay.length; i++) {
          this.listDay[i].value = false;

          let j = this.formCreateTrainer.schedule_list.map(function (e) { return e.day; }).indexOf(this.listDay[i].name);
          this.formCreateTrainer.schedule_list.splice(j, 1)
        }

      }
    }
    else {

      let input = {
        "day": name,
        "shift": []
      };
      if (value.target.checked === true) {
        this.formCreateTrainer.schedule_list.push(input)
      } else {
        let i = this.formCreateTrainer.schedule_list.map(function (e) { return e.day; }).indexOf(name);
        this.formCreateTrainer.schedule_list.splice(i, 1)
      }
    }
  }
  checkedSpeciality(value, name) {
    console.log(value)
    if (value.target.checked === true) {
      let input = {
        "id": name.id,
        "price": name.price,
        "version": name.version,
        "name": name.name
      }
      this.formCreateTrainer.speciality_list.push(input)

    } else {
      let i = this.formCreateTrainer.speciality_list.map(function (e) { return e.id; }).indexOf(name.id);
      this.formCreateTrainer.speciality_list.splice(i, 1)
    }
    console.log(this.formCreateTrainer)
  }

  addComponent(value, start, end) {
    console.log(value)
    console.log(start)
    console.log(end)
    if (start === undefined) {
      this.snotifyService.error('Please Input Start Time First', this.getConfig());
    } else if (end === undefined) {
      this.snotifyService.error('Please Input End Time First', this.getConfig());

    }
    else if (start.hour < 7 || start.hour > 21) {

      this.snotifyService.error('Start Time Must Between Range 7 AM - 8 PM', this.getConfig());

    }
    else if (end.hour < 8 || end.hour > 22) {

      this.snotifyService.error('End Time Must Between Range 8 AM - 10 PM', this.getConfig());

    } else if (end.hour < start.hour) {
      this.snotifyService.error('End Time Must Greater Than Start Time', this.getConfig());

    }
    else {
      for (let i = 0; i < this.formCreateTrainer.schedule_list.length; i++) {
        if (this.formCreateTrainer.schedule_list[i].day === value) {
          console.log(this.shift_range);
          console.log(start.minute);
          console.log(end.minute);
          this.shift_range = end.hour - start.hour;
          let selMinit = end.minute - start.minute;
          // for(this.shift_range = 0 ;this.shift_range = this.formCreateTrainer.schedule_list.length ; this.shift_range++){
          if (selMinit >= 30) {
            this.shift_range += 1
          }
          // }

          console.log(this.shift_range);

          console.log("masuk")
          this.formCreateTrainer.schedule_list[i].shift.push({ 'start_time': start, 'end_time': end, 'shift_range': this.shift_range });
          this.formCreateTrainer.schedule_list[i].start_time = undefined;
          this.formCreateTrainer.schedule_list[i].end_time = undefined;
          console.log(this.formCreateTrainer)

          /* if(this.formCreateTrainer.schedule_list[i].end.minute - this.formCreateTrainer.schedule_list[i].start.minute != 0){
            this.shift_range += 1
          } */
        }
      }
    }
  }

  ngOnInit() {

    window.scrollTo(0,0);
    this.getAll();
    this.getBank();

  }

  uploadPhotoTrainer(event) {
    this.filePhoto = event.target.files[0];
    if (this.checkFileExtTr()) {
      this.snotifyService.error('Image must be in .jpg / .png / .jpeg', this.getConfig());
      this.filePhoto = null;
    } else {
      let fd = new FormData();
      fd.append('file', this.filePhoto);
      this.trainerService.uploadImg(fd).subscribe(post => {
        if (post.message != 'ERROR') {
          this.formCreateTrainer.url_photo_trainer = post["result"];
        } else {
          this.snotifyService.error(post.result, this.getConfig());
        }
      });
    }
  }

  checkFileExtTr() {
    let nameX = this.filePhoto.name.split(".");
    let ext = nameX[nameX.length - 1];
    if (!(ext == 'jpg' || ext == 'JPG' || ext == 'png' || ext == 'PNG' || ext == 'jpeg' || ext == 'JPEG')) {
      return true;
    } else {
      return false;
    }
  }

  uploadImgNpwp(event) {
    this.fileNpwp = event.target.files[0];
    if (this.checkFileExtNpwp()) {
      this.snotifyService.error('Image must be in .jpg / .png / .jpeg', this.getConfig());
      this.fileNpwp = null;
    } else {
      let fd = new FormData();
      fd.append('file', this.fileNpwp);
      this.AdminService.uploadImg(fd).subscribe(post => {
        if (post.message != 'ERROR') {
          this.formCreateAdmin.url_npwp = post["result"];
        } else {
          this.snotifyService.error(post.result, this.getConfig());
        }
      });
    }
  }


  checkFileExtNpwp() {
    let nameX = this.fileNpwp.name.split(".");
    let ext = nameX[nameX.length - 1];
    if (!(ext == 'jpg' || ext == 'JPG' || ext == 'png' || ext == 'PNG' || ext == 'jpeg' || ext == 'JPEG')) {
      return true;
    } else {
      return false;
    }
  }

  uploadImgKtp(event) {
    this.fileKtp = event.target.files[0];
    if (this.checkFileExtKtp()) {
      this.snotifyService.error('Image must be in .jpg / .png / .jpeg', this.getConfig());
      this.fileKtp = null;
    } else {
      let fd = new FormData();
      fd.append('file', this.fileKtp);
      this.AdminService.uploadImg(fd).subscribe(post => {
        if (post.message != 'ERROR') {
          this.formCreateAdmin.url_ktp = post["result"];
        } else {
          this.snotifyService.error(post.result, this.getConfig());
        }
      });
    }
  }

  checkFileExtKtp() {
    let nameX = this.fileKtp.name.split(".");
    let ext = nameX[nameX.length - 1];
    if (!(ext == 'jpg' || ext == 'JPG' || ext == 'png' || ext == 'PNG' || ext == 'jpeg' || ext == 'JPEG')) {
      return true;
    } else {
      return false;
    }
  }

  getAll() {
    this.trainerService.getSpecialities().subscribe(posts => {
      this.data = posts.result;
    })
  }

  getBank() {
    this.trainerService.getBank().subscribe(posts => {
      this.bank = posts.result;
    })
  }



  createContact() {
    let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    console.log(this.formCreateTrainer)
    if (this.formCreateTrainer.name == '') {
      this.snotifyService.error('Please Input Trainer Name', this.getConfig());
    } else if (this.formCreateTrainer.name.length < 3) {
      this.snotifyService.error('Trainer Name Must Be Between 3 - 50 Characters', this.getConfig());
    } else if (this.formCreateTrainer.email == '') {
      this.snotifyService.error('Please Input Trainer Email', this.getConfig());
    } else if (!re.test(String(this.formCreateTrainer.email).toLowerCase())) {
      this.snotifyService.error('Please Input Valid Email', this.getConfig());
    }
    else if (this.formCreateTrainer.gender == '') {
      this.snotifyService.error('Please Choose Trainer Gender', this.getConfig());
    } else if (this.formCreateTrainer.address == '') {
      this.snotifyService.error('Please Input Trainer Address', this.getConfig());
    } else if (this.formCreateTrainer.address.length < 15) {
      this.snotifyService.error('Trainer Address is too short', this.getConfig());
    } else if (this.formCreateTrainer.address.length > 200) {
      this.snotifyService.error('Trainer Address is too long', this.getConfig());
    } else if (this.formCreateTrainer.phone == '') {
      this.snotifyService.error('Please Input Trainer Phone Number', this.getConfig());
    } else if (this.formCreateTrainer.no_ktp == '') {
      this.snotifyService.error('Please Input KTP Trainer', this.getConfig());
    } else if (this.formCreateTrainer.no_ktp.length < 16) {
      this.snotifyService.error('No KTP Must 16 Digit', this.getConfig());
    } else {
      for (let z = 0; z < this.formCreateTrainer.speciality_list.length; z++) {
        delete this.formCreateTrainer.speciality_list[z].name;
        delete this.formCreateTrainer.speciality_list[z].version;
      }
      console.log(this.formCreateTrainer)

      this.trainerService.addTrainer(this.formCreateTrainer).subscribe(data => {
        if (data.message != 'ERROR') {

          this.snotifyService.success('Create Trainer Success!', this.getConfig());


          // alert("Berhasil login!");
          setTimeout(() => {
            this.router.navigate(['/trainer/list'])
          }, 4000);
          // this.router.navigateByUrl('/dashboard');
        } else {
          this.snotifyService.error(data.result, this.getConfig());
        }
      })

    }
  }

    isNumber(event){
    var keycode = event.keyCode;
    console.log(keycode);
    if(keycode>=48 && keycode<=57){
      return true;
    } else {
      return false;
    }
  }

  uploadFileCust(event) {
    this.fileCust = event.target.files[0];
    if (this.checkFileExtCust()) {
      this.snotifyService.error('Image must be in .jpg / .png / .jpeg', this.getConfig());
      this.fileCust = null;
    } else {
      let fd = new FormData();
      fd.append('file', this.fileCust);
      this.UserService.uploadImg(fd).subscribe(post => {
        if (post.message != 'ERROR') {
          this.formCreateCustomer.photo_selfie = post["result"];
        } else {
          this.snotifyService.error(post.result, this.getConfig());
        }
      });
    }
  }

  checkFileExtCust() {
    let nameX = this.fileCust.name.split(".");
    let ext = nameX[nameX.length - 1];
    if (!(ext == 'jpg' || ext == 'JPG' || ext == 'png' || ext == 'PNG' || ext == 'jpeg' || ext == 'JPEG')) {
      return true;
    } else {
      return false;
    }
  }

  createCustomerX() {
    let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    let x = /^[a-z A-Z]+$/;

    console.log(this.formCreateCustomer)
    if (this.formCreateCustomer.full_name == '') {
      this.snotifyService.error('Please Input Customer Name', this.getConfig());
    } else if (!this.formCreateCustomer.full_name.match(x)) {
      this.snotifyService.error('Customer Name Must Be In Alphabet', this.getConfig());
    } else if (this.formCreateCustomer.full_name.length < 3 || this.formCreateCustomer.full_name.length > 50) {
      this.snotifyService.error('Customer Name Must Be Between 3 - 50 Characters', this.getConfig());
    } else if (this.formCreateCustomer.email == '') {
      this.snotifyService.error('Please Input Customer Email', this.getConfig());
    } else if (!re.test(String(this.formCreateCustomer.email).toLowerCase())) {
      this.snotifyService.error('Please Input Valid Email', this.getConfig());
    } else if (this.formCreateCustomer.address == '') {
      this.snotifyService.error('Please Input Customer Address', this.getConfig());
    } else if (this.formCreateCustomer.address.length < 15) {
      this.snotifyService.error('Customer Address Must Be Between 15 - 200 Characters', this.getConfig());
    } else if (this.formCreateCustomer.address.length > 200) {
      this.snotifyService.error('Customer Address Must Be Between 15 - 200 Characters', this.getConfig());
    } else if (this.formCreateCustomer.phone.length < 10 || this.formCreateCustomer.phone.length > 14) {
      this.snotifyService.error('Phone Number Must Be Between 10 - 14 Characters', this.getConfig());
    } else {
      this.UserService.addUser(this.formCreateCustomer).subscribe(data => {
        if (data.message != 'ERROR') {
          this.snotifyService.success('Create Customer Success!', this.getConfig());
          setTimeout(() => {
            this.router.navigate(['/users/list'])
          }, 4000);
          // this.router.navigateByUrl('/dashboard');
        } else {
          this.snotifyService.error(data.result, this.getConfig());
        }
      })
    }

  }

  createAdmin() {
    let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (this.formCreateAdmin.email == '') {
      this.snotifyService.error('Please Input Admin Email', this.getConfig());
    } else if (!re.test(String(this.formCreateAdmin.email).toLowerCase())) {
      this.snotifyService.error('Please Input Valid Email', this.getConfig());
    } else if (this.formCreateAdmin.full_name == '') {
      this.snotifyService.error('Please Input Admin Name', this.getConfig());
    } else if (this.formCreateAdmin.full_name.length < 3 || this.formCreateAdmin.full_name.length > 50) {
      this.snotifyService.error('Admin Name Must Be Between 3 - 50 Characters', this.getConfig());
    } else if (this.formCreateAdmin.full_name == '') {
      this.snotifyService.error('Please Input Admin Username', this.getConfig());
    } else if (this.formCreateAdmin.username.length < 3 || this.formCreateAdmin.username.length > 20) {
      this.snotifyService.error('Admin Username Must Be Between 3 - 20 Characters', this.getConfig());
    } else if (this.formCreateAdmin.address == '') {
      this.snotifyService.error('Please Input Admin Address', this.getConfig());
    } else if (this.formCreateAdmin.address.length < 15) {
      this.snotifyService.error('Admin Address is too Short', this.getConfig());
    } else if (this.formCreateAdmin.address.length > 200) {
      this.snotifyService.error('Admin Address is too Long', this.getConfig());
    } else if (this.formCreateAdmin.no_ktp == '') {
      this.snotifyService.error('Please Input No Ktp', this.getConfig());
    } else if (this.formCreateAdmin.no_ktp.length < 16) {
      this.snotifyService.error('No KTP Must 16 Digit', this.getConfig());
    } else if (this.formCreateAdmin.no_ktp.length > 16) {
      this.snotifyService.error('No KTP Must 16 Digit', this.getConfig());
    } else {
      let request =
        {
          "address": this.formCreateAdmin.address,
          "full_name": this.formCreateAdmin.full_name,
          "no_ktp": this.formCreateAdmin.no_ktp,
          "no_npwp": this.formCreateAdmin.no_npwp,
          "url_ktp": this.formCreateAdmin.url_ktp,
          "url_npwp": this.formCreateAdmin.url_npwp,
          "username": this.formCreateAdmin.username,
          "password": "welcome1",
          "email": this.formCreateAdmin.email
        }
      console.log(request)
      this.AdminService.addAdmin(request).subscribe(data => {
        if (data.message != 'ERROR') {

          this.snotifyService.success('Create Admin Success!', this.getConfig());
          setTimeout(() => {
            this.router.navigate(['/admin/list'])
          }, 4000);
        } else {
          this.snotifyService.error(data.result, this.getConfig());
        }
        console.log(this.formCreateAdmin)
      })
    }
  }

  formCreate = new FormGroup({
    email: new FormControl('', [
      Validators.required,
      Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$')
    ]),
    emailT: new FormControl('', [
      Validators.required,
      Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$')
    ]),
    fullName: new FormControl('', [
      Validators.pattern(/^[a-z A-Z]*$/),
      Validators.required,
      Validators.minLength(3),
      Validators.maxLength(50)
    ]),
    nameT: new FormControl('', [
      Validators.required,
      Validators.minLength(3),
      Validators.maxLength(50)
    ]),
    username: new FormControl('', [
      Validators.required,
      Validators.minLength(3),
      Validators.maxLength(50)
    ]),
    password: new FormControl('', [
      Validators.required,
      Validators.minLength(8),
    ]),
    address: new FormControl('', [
      Validators.required,
      Validators.minLength(15),
      Validators.maxLength(200)
    ]),
    addressT: new FormControl('', [
      Validators.required,
      Validators.minLength(15),
      Validators.maxLength(200)
    ]),
    noKtp: new FormControl('', [
      Validators.required,
      Validators.pattern('^[0-9]+$'),
      Validators.minLength(16),
      Validators.maxLength(16)
    ]),
    noKtpT: new FormControl('', [
      Validators.required,
      Validators.pattern('^[0-9]+$'),
      Validators.minLength(16),
      Validators.maxLength(16)
    ]),
    noNpwp: new FormControl('', [
      // Validators.required,
      Validators.pattern('^[0-9]+$'),
      Validators.minLength(15),
      Validators.maxLength(15)
    ]),
    noNpwpT: new FormControl('', [
      Validators.required,
      Validators.pattern('^[0-9]+$'),
      Validators.minLength(15),
      Validators.maxLength(15)
    ]),
    phoneT: new FormControl('', [
      Validators.required,
      Validators.pattern('[0][8][0-9]{8,11}'),
      Validators.minLength(10),
      Validators.maxLength(15)
    ]),
    noRekT: new FormControl('', [
      Validators.required,
      Validators.pattern('^[0-9]+$'),
      Validators.minLength(10),
      Validators.maxLength(15)
    ]),
    // price : new FormControl('',[
    //   Validators.required,
    //   Validators.pattern('[0][8][0-9]{8,11}'),
    //   Validators.minLength(10),
    //   Validators.maxLength(15)
    // ]),

  })

  get email() {
    return this.formCreate.get('email');
  }
  get emailT() {
    return this.formCreate.get('emailT');
  }
  get fullName() {
    return this.formCreate.get('fullName');
  }
  get nameT() {
    return this.formCreate.get('nameT');
  }
  get username() {
    return this.formCreate.get('username');
  }
  get password() {
    return this.formCreate.get('password');
  }
  get address() {
    return this.formCreate.get('address');
  }
  get addressT() {
    return this.formCreate.get('addressT');
  }
  get noKtp() {
    return this.formCreate.get('noKtp');
  }
  get noNpwp() {
    return this.formCreate.get('noNpwp');
  }
  get noKtpT() {
    return this.formCreate.get('noKtpT');
  }
  get noNpwpT() {
    return this.formCreate.get('noNpwpT');
  }
  get phoneT() {
    return this.formCreate.get('phoneT');
  }
  get noRekT() {
    return this.formCreate.get('noRekT');
  }


}




export class shiftObject {
  start_time: number;
  end_time: number;
}
export class scheduleObject {
  day: string;
  shift: shiftObject[];
}
export class specialityObject {
  id: string;
  price: number;
  version: number;
}
