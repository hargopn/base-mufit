import { AdminService } from './../../../services/admin.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {HttpModule} from '@angular/http';
import {SharedModule} from '../../../shared/shared.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CurrencyMaskModule} from 'ng2-currency-mask';
import {BrowserModule} from '@angular/platform-browser';
import { RatingReviewComponent } from './rating-review.component';
import { RatingService } from '../../../services/rating.service';



export const RatingReviewRoutes: Routes = [
  {
    path: '',
    component: RatingReviewComponent,
    data: {
      breadcrumb: 'rating-review Trainer',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(RatingReviewRoutes),
    FormsModule,
    HttpModule,
    SharedModule,
    CurrencyMaskModule,
    ReactiveFormsModule
  ],
  declarations: [RatingReviewComponent],
  providers: [RatingService],
  entryComponents: [],
  exports: [],
  bootstrap: []
})
export class RatingReviewModule { }
