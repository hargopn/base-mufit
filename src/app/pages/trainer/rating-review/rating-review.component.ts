import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import {transition, trigger, style, animate} from '@angular/animations';
import { RatingService } from './../../../services/rating.service';
import { Rating } from './../../../class/rating';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import swal from 'sweetalert2';


@Component({
  selector: 'app-rating-review',
  templateUrl: './rating-review.component.html',
  styleUrls: ['./rating-review.component.css',
  '../../../../../node_modules/sweetalert2/dist/sweetalert2.min.css',],
  encapsulation: ViewEncapsulation.None,
  animations: [
    trigger('fadeInOutTranslate', [
      transition(':enter', [
        style({opacity: 0}),
        animate('400ms ease-in-out', style({opacity: 1}))
      ]),
      transition(':leave', [
        style({transform: 'translate(0)'}),
        animate('400ms ease-in-out', style({opacity: 0}))
      ])
    ])
  ]
})
export class RatingReviewComponent implements OnInit {

  public status;
  public id;

  rating: Rating[];
  reject: Rating[];
  approve: Rating[];
  constructor(private router: Router,private route: ActivatedRoute, private RatingService: RatingService) { }

  ngOnInit() {
    window.scrollTo(0,0);
    this.loadData();
    this.loadReject();
    this.loadApprove();
  }

  loadData() {
    this.status = "pending";    
    this.RatingService.getPaginate(this.status).subscribe(posts => {
      this.rating = posts["result"];
      console.log(this.rating);
    });
  }

  loadApprove() {
    this.status = "approved";    
    this.RatingService.getPaginate(this.status).subscribe(posts => {
      this.approve = posts["result"];
      console.log(this.rating);
    });
  }

  loadReject(){
    this.status = "rejected";
    this.RatingService.getPaginate(this.status).subscribe(posts => {
      this.reject = posts["result"];
      console.log(this.reject);
    });
  }

  openSwalR(id){
    swal({
      title: 'Are you sure?',
      text: 'You wont be able to revert',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, reject it!'
    }).then(()=> { 
      this.status = "rejected"
      this.RatingService.approvalRating(id,this.status).subscribe(data =>{
        console.log(data)
        if(data.message== 'OK'){
          swal(
            'Rejected!',
            'Data has been deleted.',
            'success'
          );
          this.loadReject()
          this.loadData()
          this.loadApprove()
          this.router.navigate(['/trainer/rating-review'])
        }
      })
    }).catch(swal.noop); 
  }

  openSwalA(id){
    swal({
      title: 'Are you sure?',
      text: 'You wont be able to revert',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, approve it!'
    }).then(()=> {
      this.status = "approved"
      this.RatingService.approvalRating(id,this.status).subscribe(data =>{
        console.log(data)
        if(data.message== 'OK'){
          swal(
            'Approved!',
            'Data has been approved.',
            'success'
          );
  
          this.loadReject()
          this.loadData()
          this.loadApprove()
          this.router.navigate(['/trainer/rating-review'])
        }
      })
    }).catch(swal.noop);
    
  }

}

// approve(data, id){
//   let token = this.getToken();
//   let request = {"access_token": token, "id": id, "status": "approved"}
//   return this.getDataParam(Configuration.BASE_URL + Configuration.APRROVE, request)
// }

