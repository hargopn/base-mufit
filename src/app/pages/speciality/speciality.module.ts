import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {SharedModule} from '../../shared/shared.module';

export const SpecialityRoutes: Routes = [
  {
    path: '',
    data: {
      breadcrumb: '',
      status: false
    },
    children: [
      {
        path: 'list',
        loadChildren: './speciality-list/speciality-list.module#SpecialityListModule'
      }
    ]
  }
];
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(SpecialityRoutes),
    SharedModule
  ],
  declarations: []
})
export class SpecialityModule { }
