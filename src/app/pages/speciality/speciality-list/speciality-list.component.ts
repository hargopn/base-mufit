import { Component, OnInit, Input, ViewEncapsulation } from '@angular/core';
import { NgModel, NgForm } from '@angular/forms';
import { Configuration } from '../../../services/base-services/configuration';
import { SpecialityService } from '../../../services/speciality.service';
import { Router } from '@angular/router';
import * as moment from 'moment';
import { NgbTimepickerConfig } from '@ng-bootstrap/ng-bootstrap';
import { ActivatedRoute } from '@angular/router';
import { SnotifyService, SnotifyPosition, SnotifyToastConfig } from 'ng-snotify';
import swal from 'sweetalert2';


@Component({
  selector: 'app-list',
  templateUrl: './speciality-list.component.html',
  styleUrls: ['./speciality-list.component.css',
  '../../../../../node_modules/sweetalert2/dist/sweetalert2.min.css'],
  encapsulation: ViewEncapsulation.None
})
export class SpecialityListComponent implements OnInit {

  speciality ={
    "name": "",
    "cover":""
  }
  public data: any;
  public resultX: any = false;
  timeout = 3000;
  newTop = true;
  dockMax = 4;
  blockMax = 3;
  titleMaxLength = 30;
  bodyMaxLength = 100;
  public fileCover:any;

  constructor(
    private service: SpecialityService,
    private router: Router,
    private snotifyService: SnotifyService,
    private route: ActivatedRoute

  ) { }

  ngOnInit() {
    window.scrollTo(0,0);
    this.getList();
  }

  getList(){
    this.service.getListSpeciality().subscribe(posts => {
      this.data = posts.result;
    })
  }

  clickSubmit(event){
    let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (this.speciality.name == '') {
      this.snotifyService.error('Please Input Trainer Speciality Name', this.getConfig());
    } else if (this.speciality.name.length < 3) {
      this.snotifyService.error('Trainer Speciality Name Must Be Between 3 - 50 Characters', this.getConfig());
    }else{
      this.service.addSpeciality(this.speciality).subscribe(data => {
        if (data.message != 'ERROR') {
          this.snotifyService.success('Create Trainer Speciality Success!', this.getConfig());
          // setTimeout(() => {
            this.speciality.cover = "";
            this.fileCover = undefined;
            this.getList();
            // this.router.navigate(['speciality/list']);
            this.closeMyModal(event);
          // }, 3000);
        } else {
          this.snotifyService.error(data.result, this.getConfig());
        }
      })
  }
  }



  uploadCover(event) {
    // console.log(event)
    this.fileCover = event.target.files[0];
    if (this.checkFileExtKtp()) {
      this.snotifyService.error('Image must be in .jpg / .png / .jpeg', this.getConfig());
      this.fileCover = null;
    } else {
      let fd = new FormData();
      fd.append('file', this.fileCover);
      this.service.uploadImg(fd).subscribe(post => {
        if (post.message != 'ERROR') {
          this.speciality.cover = post["result"];
        } else {
          this.snotifyService.error(post.result, this.getConfig());
        }
      });
    }
  }
  checkFileExtKtp() {
    let nameX = this.fileCover.name.split(".");
    let ext = nameX[nameX.length - 1];
    if (!(ext == 'jpg' || ext == 'JPG' || ext == 'png' || ext == 'PNG' || ext == 'jpeg' || ext == 'JPEG')) {
      return true;
    } else {
      return false;
    }
  }
  openMyModal(event) {
    this.speciality.name = "";
    document.querySelector('#' + event).classList.add('md-show');
  }

  closeMyModal(event) {
    ((event.target.parentElement.parentElement).parentElement).classList.remove('md-show');
  }

  removeObject(id, event){
    swal({
      title: 'Are you sure?',
      text: 'You wont be able to revert',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
    }).then(()=> {
      this.service.deleteSpeciality(id, false).subscribe(data =>{
        console.log(data)
        if(data.message== 'OK' && data.result == true){
          swal(
            'Deleted!',
            'Trainer speciality has been deleted.',
            'success'
          );
          this.getList();
        }else{
          swal({
            title: 'Are you sure?',
            text: 'This speciality is owned by trainer',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!',
          }).then(()=> {
              this.service.deleteSpeciality(id, true).subscribe(data=> {
                console.log(data)
                swal(
                  'Deleted!',
                  'Trainer speciality has been deleted.',
                  'success'
                );
                this.getList();
              })
          })
        }
      })

    }).catch(swal.noop);
  }

  getConfig(): SnotifyToastConfig {
    this.snotifyService.setDefaults({
      global: {
        newOnTop: this.newTop,
        maxAtPosition: this.blockMax,
        maxOnScreen: this.dockMax,
      }
    });
    return {
      bodyMaxLength: this.bodyMaxLength,
      titleMaxLength: this.titleMaxLength,
      backdrop: -1,
      position: SnotifyPosition.centerTop,
      timeout: this.timeout,
      showProgressBar: false,
      closeOnClick: true,
      pauseOnHover: false
    };

  }

}
