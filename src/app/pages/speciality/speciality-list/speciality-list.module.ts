import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {HttpModule} from '@angular/http';
import {SharedModule} from '../../../shared/shared.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CurrencyMaskModule} from 'ng2-currency-mask';
import {BrowserModule} from '@angular/platform-browser';
import {SpecialityListComponent} from './speciality-list.component';
import { SpecialityService } from '../../../services/speciality.service';

export const SpecialityListRoutes: Routes = [
  {
    path: '',
    component:SpecialityListComponent,
    data: {
      breadcrumb: 'List Speciality',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(SpecialityListRoutes),
    FormsModule,
    HttpModule,
    SharedModule,
    CurrencyMaskModule,
    ReactiveFormsModule
  ],
  declarations: [SpecialityListComponent],
  providers: [SpecialityService],
  entryComponents: [],
  exports: [],
  bootstrap: []
})
export class SpecialityListModule { }
