import {DataTableModule} from 'angular2-datatable';
import {FormsModule} from '@angular/forms';
import { SharedModule } from './../../../shared/shared.module';
import { RouterModule } from '@angular/router';
import { CreateContentComponent } from './../create-content/create-content.component';
import { Routes } from '@angular/router';
import { ListContentComponent } from './list-content.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminService } from '../../../services/admin.service';

export const listContentRoutes: Routes = [
  {
    path: '',
    component: ListContentComponent,
      data: {
    breadcrumb: 'Create Content',
    icon: 'icofont-social-blogger bg-c-green',
      status: false
  } }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(listContentRoutes),
    SharedModule,
    FormsModule,
    DataTableModule
  ],
  declarations: [ListContentComponent],
  providers: [AdminService]
})
export class ListContentModule { }
