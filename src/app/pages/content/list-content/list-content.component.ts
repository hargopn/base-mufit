import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { AdminService } from './../../../services/admin.service';
import { Cover } from './../../../class/cover';
import swal from 'sweetalert2';

@Component({
  selector: 'app-list-content',
  templateUrl: './list-content.component.html',
  styleUrls: ['./list-content.component.css',
  '../../../../../node_modules/sweetalert2/dist/sweetalert2.min.css'],
  encapsulation: ViewEncapsulation.None
})
export class ListContentComponent implements OnInit {

  public page: number = 1; //page
  public limit: number = 25; //itemsPerPage
  public totalItems: number; //elements

  public search="";

  public previousPage: any;

  cover : Cover[];

  constructor(private adminService : AdminService) { }

  ngOnInit() {
    this.loadData();
  }

  loadData() {
    this.adminService.getListCover(this.page - 1, this.limit, this.search).subscribe(posts => {
      this.cover = posts["result"];
      this.totalItems = posts["elements"];
    });
  }

  loadPage(page: number) {
    if (page !== this.previousPage) {
      this.previousPage = page;
      this.loadData();
    }
  }

  //Show limit list
  getSelect(limitnya) {
    this.adminService.getListCover(this.page, limitnya, this.search).subscribe(posts => {
      this.limit = limitnya;
      this.loadData();
    });
  }

  onChange(deviceValue) {
    this.getSelect(deviceValue);
  }

  //Search
  getSearch(query){
    this.search = query;
    this.loadData();
  }

  deleteImg(id){

    swal({
      title: 'Are you sure?',
      text: 'You wont be able to revert',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!', 
    }).then(()=> {
      this.adminService.deleteCover(id).subscribe(posts => {
        console.log(posts)        
        if(posts.message== 'OK'){
          swal(
            'Deleted!',
            'Your file has been deleted.',
            'success'
          );

          this.loadData();       
        }
      })
    }).catch(swal.noop); 
    
  }

}
