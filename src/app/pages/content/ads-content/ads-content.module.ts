import { AdminService } from './../../../services/admin.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdsContentComponent } from './ads-content.component';

import {RouterModule, Routes} from '@angular/router';
import {SharedModule} from '../../../shared/shared.module';

import {DataTableModule} from 'angular2-datatable';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {ScrollToModule} from '@nicky-lenaers/ngx-scroll-to';



export const berandaRoutes: Routes = [
  {
    path: '',
    component: AdsContentComponent,
      data: {
    breadcrumb: 'Create Content',
    icon: 'icofont-social-blogger bg-c-green',
      status: false
  } }
];



@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(berandaRoutes),
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    DataTableModule
  ],
  declarations: [AdsContentComponent], providers: [AdminService]
})

export class AdsContentModule { }


