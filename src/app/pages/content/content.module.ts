import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {SharedModule} from '../../shared/shared.module';
import { ListContentComponent } from './list-content/list-content.component';
import { AdsContentComponent } from './ads-content/ads-content.component';

export const UserRoutes: Routes = [
  {
    path: '',
    data: {
      breadcrumb: '',
      status: false
    },
    children: [
      {
        path: 'add-cover',
        loadChildren: './create-content/create-content.module#CreateContentModule'
      },
      {
        path: 'list',
        loadChildren: './list-content/list-content.module#ListContentModule'
      },
      {
        path: 'add-advertisement',
        loadChildren: './ads-content/ads-content.module#AdsContentModule'
      }
    ]
  }
];
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(UserRoutes),
    SharedModule
  ],
  declarations: []
})
export class ContentModule { }



