import { AdminService } from './../../../services/admin.service';
import { Component, OnInit } from '@angular/core';
import {SnotifyService, SnotifyPosition, SnotifyToastConfig} from 'ng-snotify';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-content',
  templateUrl: './create-content.component.html',
  styleUrls: ['./create-content.component.css']
})
export class CreateContentComponent implements OnInit {

  public fileCover: any;

  public formUploadCover = {
    "description" : "",
    "header" : "",
    "id" : "",
    "url_cover" : "",
    "version" : ""
  }

  constructor(private adminService : AdminService, private snotifyService : SnotifyService, private router:Router) { }

  timeout = 3000;
  newTop = true;
  dockMax = 4;
  blockMax = 3;
  titleMaxLength = 30;
  bodyMaxLength = 100;
  
  getConfig(): SnotifyToastConfig {
    this.snotifyService.setDefaults({
      global: {
        newOnTop: this.newTop,
        maxAtPosition: this.blockMax,
        maxOnScreen: this.dockMax,
      }
    });
    return {
      bodyMaxLength: this.bodyMaxLength,
      titleMaxLength: this.titleMaxLength,
      backdrop: -1,
      position: SnotifyPosition.centerTop,
      timeout: this.timeout,
      showProgressBar: false,
      closeOnClick: true,
      pauseOnHover: false
    };

  }

  ngOnInit() {
  }

  cekFileExtCover(){
    let name = this.fileCover.name.split(".");
    let ext = name[name.length - 1];
    if(!(ext == 'jpg' || ext == 'JPG' || ext == 'png' || ext == 'PNG' || ext == 'jpeg' || ext == 'JPEG')){
      return true;
    } else {
      return false;
    }
  }

  uploadCover(event){
    console.log(event);
    this.fileCover = event.target.files[0];

    if(this.cekFileExtCover()){
      this.snotifyService.error('Image must be in .jpg/ .png/ .jpeg', this.getConfig());
      this.fileCover == null;
    } else {
      let fd = new FormData();
      fd.append('file', this.fileCover);
      
      this.adminService.uploadImg(fd).subscribe(posts=>{
        console.log(posts.result);
        if(posts.message != 'ERROR'){
          this.formUploadCover.url_cover = posts["result"];
          console.log(this.formUploadCover.url_cover);
          console.log(posts);
        } else {
          this.snotifyService.error(posts.result, this.getConfig());
        }
      })
    }

    
    /* if(this.fileCover.size > 1000000){
      console.log("salah");
    } else {
      console.log("benar");
    } */
  }

  form = new FormGroup({
    header : new FormControl('', [
      Validators.required,
      Validators.minLength(3),
      Validators.maxLength(100)
    ])
  })

  get header(){
    return this.form.get('header');
  }

  submitCover(){
    if(this.formUploadCover.url_cover == ""){
      this.snotifyService.error("Please insert Image Cover", this.getConfig());
    } else if(this.formUploadCover.header == ""){
      this.snotifyService.error("Please input Header", this.getConfig());
    } else if (this.formUploadCover.header.length < 3){
      this.snotifyService.error("Header must be 3-100 characters", this.getConfig());
    } else if (this.formUploadCover.header.length > 100){
      this.snotifyService.error("Header must be 3-100 characters", this.getConfig());
    } else {
      // this.snotifyService.success("", this.getConfig());
      this.adminService.addCover(this.formUploadCover).subscribe(posts=>{
         this.router.navigate(['/content/list']);
      })
    }
  }

}
