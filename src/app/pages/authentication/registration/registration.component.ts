import { Component, OnInit } from '@angular/core';
import {AuthenticationService} from '../../../services/authentication.service';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

  constructor(private router: Router, private authService: AuthenticationService) { }

  create(form: NgForm) {
    console.log(form)
    if(form.value.email == '') {
      alert("Email harus diisi");
    } else if(form.value.full_name == '') {
      alert("Nama Lengkap harus diisi");
    } else if(form.value.phone == 0 || form.value.phone == '') {
      alert("Nomor Telefon harus diisi");
    } else if(form.value.password == '') {
      alert("Password harus diisi");
    } else if(form.value.confirm_password == '') {
      alert("Konfirmasi Password harus diisi");
    } else if(form.value.password != form.value.confirm_password) {
      alert("Konfirmasi Password harus sama dengan Password");
    } else {
      this.authService.createClient(form.value).subscribe(data => {
        if(data.message != 'ERROR') {
          alert("Data berhasil disimpan");
          this.authService.sendEmail(data.result.id).subscribe(data => {
            if(data.message != 'ERROR') {
              alert("Email aktivasi telah terkirim");
            } else {
              alert(data.result);
            }
          })
          this.router.navigateByUrl("/authentication/login");
        } else {
          alert(data.result);
        }
      });
    }
  } 

  ngOnInit() {
  }

}
