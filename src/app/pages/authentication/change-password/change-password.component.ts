import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../../../services/authentication.service';
import { Router, ActivatedRoute } from '@angular/router';
import { NgForm, FormControl, FormGroup, FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { SnotifyService, SnotifyPosition, SnotifyToastConfig } from 'ng-snotify';


@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {

  timeout = 3000;
  newTop = true;
  dockMax = 4;
  blockMax = 3;
  titleMaxLength = 30;
  bodyMaxLength = 100;
  public post: any;
  public new_password: any;
  public new_password_confirmation: any;

  constructor(
    private authService: AuthenticationService,
    private router: Router,
    private route: ActivatedRoute,
    private snotifyService: SnotifyService,
    private formBuilder: FormBuilder,
   ) {}

   formPass ={
     "new_password": "",
     "new_password_confirmation": "",
     "url_code": ""
   }

   formNewPass = new FormGroup({
     new_password: new FormControl('', [
       Validators.required,
       Validators.minLength(8),
       Validators.pattern(/^.*(?=.{8,})(?=.*\s)(?=.*[a-zA-Z])(?=.*\d).*$/)
       // ! @ # $ % & ? - + = _
     ]),
     new_password_confirmation: new FormControl('', [
       Validators.required,
       this.passwordConfirming
     ])
   })

    passwordConfirming(c: AbstractControl): any {
        if(!c.parent || !c) return;
        const pwd = c.parent.get('new_password');
        const cpwd= c.parent.get('new_password_confirmation')

        if(!pwd || !cpwd) return ;
        if (pwd.value !== cpwd.value) {
            return { invalid: true };
    }
}


  ngOnInit() {
    this.route.params.subscribe(params => {
      this.formPass.url_code = params['url_code'];
    })
    console.log(this.formPass.url_code)
  }

  changePass(){
    this.authService.changePassword(this.formPass).subscribe(data=> {
      if(data.message != 'ERROR') {
        this.snotifyService.success('Change Password Success!', this.getConfig());
        setTimeout(()=> {
          this.router.navigate(['/success-change-password'])
        }, 3000);
      }else {
        this.snotifyService.error(data.result, this.getConfig());
      }
    })
    console.log(this.formPass)
  }

getConfig(): SnotifyToastConfig {
  this.snotifyService.setDefaults({
    global: {
      newOnTop: this.newTop,
      maxAtPosition: this.blockMax,
      maxOnScreen: this.dockMax,
    }
  });
  return {
    bodyMaxLength: this.bodyMaxLength,
    titleMaxLength: this.titleMaxLength,
    backdrop: -1,
    position: SnotifyPosition.centerTop,
    timeout: this.timeout,
    showProgressBar: false,
    closeOnClick: true,
    pauseOnHover: false
  };

}
}
