import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChangePasswordComponent } from './change-password.component';
import {RouterModule, Routes} from '@angular/router';
import {SharedModule} from '../../../shared/shared.module';
import { AuthenticationService } from '../../../services/authentication.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

export const changePasswordRoutes: Routes = [
  {
    path: '',
    component: ChangePasswordComponent,
    data: {
      breadcrumb: 'Ganti Kata Sandi'
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(changePasswordRoutes),
    SharedModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [ AuthenticationService ],
  declarations: [ChangePasswordComponent]
})
export class ChangePasswordModule { }
