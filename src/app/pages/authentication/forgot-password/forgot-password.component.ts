import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AuthenticationService } from '../../../services/authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {

  constructor(private authService: AuthenticationService, private router: Router) {}

  ngOnInit() {
  }

  sendEmail(form: NgForm) {
    if(form.value.email == '') {
      alert("Masukkan email anda");
    } else {
      this.authService.sendEmailForgot(form.value).subscribe(response => 
        {
          if(response.message != 'ERROR') {
            alert("Email sudah terkirim");
            this.router.navigateByUrl("/login");
          } else {
            alert(response.result);
          }
        }
      )
    }
  }

}
