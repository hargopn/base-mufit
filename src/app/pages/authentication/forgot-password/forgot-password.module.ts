import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ForgotPasswordComponent } from './forgot-password.component';
import {RouterModule, Routes} from '@angular/router';
import {SharedModule} from '../../../shared/shared.module';
import { AuthenticationService } from '../../../services/authentication.service';

export const forgotPasswordRoutes: Routes = [
  {
    path: '',
    component: ForgotPasswordComponent,
    data: {
      breadcrumb: 'Lupa Kata Sandi'
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(forgotPasswordRoutes),
    SharedModule
  ],
  providers: [AuthenticationService],
  declarations: [ForgotPasswordComponent]
})
export class ForgotPasswordModule { }
