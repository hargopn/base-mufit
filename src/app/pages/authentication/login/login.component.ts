import { Component, OnInit } from '@angular/core';
import {AuthenticationService} from '../../../services/authentication.service';
import {LocalStorageService, SessionStorageService} from 'ngx-webstorage';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import {SnotifyService, SnotifyPosition, SnotifyToastConfig} from 'ng-snotify';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private authService: AuthenticationService, private router: Router, private localStorageService: LocalStorageService, private snotifyService : SnotifyService) { }
  
  timeout = 3000;
  newTop = true;
  dockMax = 4;
  blockMax = 3;
  titleMaxLength = 30;
  bodyMaxLength = 100;

  getConfig(): SnotifyToastConfig {
    this.snotifyService.setDefaults({
      global: {
        newOnTop: this.newTop,
        maxAtPosition: this.blockMax,
        maxOnScreen: this.dockMax,
      }
    });
    return {
      bodyMaxLength: this.bodyMaxLength,
      titleMaxLength: this.titleMaxLength,
      backdrop: -1,
      position: SnotifyPosition.centerTop,
      timeout: this.timeout,
      showProgressBar: false,
      closeOnClick: true,
      pauseOnHover: false
    };

  }

  login(form: NgForm) {
    if(form.value.username == '') {
      // alert("Email harus diisi");
      this.snotifyService.error('Username is required',this.getConfig());
    } else if(form.value.password == '') {
      // alert("Password harus diisi")
      this.snotifyService.error('Password is required',this.getConfig());
    } else {
      this.authService.login(form.value).subscribe(data => {
        if(data.message != 'ERROR') {

          this.localStorageService.store('token', data.result.accessToken);
          this.localStorageService.store('fullName', data.result.fullName);
          // alert("Berhasil login!");
          this.router.navigate(['/booking/list'])
          // this.router.navigateByUrl('/dashboard');
        } else {
          this.snotifyService.error(data.result,this.getConfig());
        }
      })
    }
  }

  ngOnInit() {
    if(this.localStorageService.retrieve('token') != null) {
      this.router.navigate(['/dashboard']);
    }
  }

}
