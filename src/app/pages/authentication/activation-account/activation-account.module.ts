import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ActivationAccountComponent } from './activation-account.component';
import {RouterModule, Routes} from '@angular/router';
import {SharedModule} from '../../../shared/shared.module';
import { AuthenticationService } from '../../../services/authentication.service';

export const activationAccountRoutes: Routes = [
  {
    path: '',
    component: ActivationAccountComponent,
    data: {
      breadcrumb: 'Aktivasi Akun'
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(activationAccountRoutes),
    SharedModule
  ],
  providers: [AuthenticationService],
  declarations: [ActivationAccountComponent]
})
export class ActivationAccountModule { }
