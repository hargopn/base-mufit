import { Component, OnInit, AfterViewChecked } from '@angular/core';
import { AuthenticationService } from '../../../services/authentication.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { animate, style, transition, trigger } from '@angular/animations';
import { AuthService } from '../../../services/base-services/auth.service';


@Component({
  selector: 'app-activation-account',
  templateUrl: './activation-account.component.html',
  styleUrls: ['./activation-account.component.css'],
  animations: [
    trigger('fadeInOutTranslate', [
      transition(':enter', [
        style({ opacity: 0 }),
        animate('1000ms ease-in-out', style({ opacity: 1 }))
      ]),
      transition(':leave', [
        style({ transform: 'translate(0)' }),
        animate('1000ms ease-in-out', style({ opacity: 0 }))
      ])
    ]),
    trigger('fadeInOutTranslate2', [
      transition(':enter', [
        style({ opacity: 0 }),
        animate('2500ms ease-in-out', style({ opacity: 1 }))
      ]),
      transition(':leave', [
        style({ transform: 'translate(0)' }),
        animate('2500ms ease-in-out', style({ opacity: 0 }))
      ])
    ]),
    trigger('fadeInOutTranslate3', [
      transition(':enter', [
        style({ opacity: 0 }),
        animate('300ms ease-in-out', style({ opacity: 1 }))
      ]),
      transition(':leave', [
        style({ transform: 'translate(0)' }),
        animate('300ms ease-in-out', style({ opacity: 0 }))
      ])
    ])

  ]
})
export class ActivationAccountComponent implements AfterViewChecked, OnInit {

  header: String;
  header2: String;
  id: String;

  registrationAccount ={
    "url_code":""
    
  }

  constructor(private router: Router, private activatedRoute: ActivatedRoute, private authenticationService: AuthenticationService, private authService: AuthService) {
  }

  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {
      this.registrationAccount.url_code = params['url_code'];
      console.log(this.registrationAccount.url_code)
    });
    this.authenticationService.registrationAcc(this.registrationAccount).subscribe(response => {
      if (response.message != 'ERROR') {
        this.header = "Your account has been successfully registered";
        this.header2 = "Please open the Mufit app to login and continue"
        // setTimeout(() => {    //<<<---    using ()=> syntax
          // this.router.navigate(["/login"]);
        // }, 4000);
      } else {
        this.header = response.result;
        // setTimeout(() => {    //<<<---    using ()=> syntax
        //   this.router.navigate(["/login"]);
        // }, 4000);
      }
    });
    // console.log(this.registrationAccount.url_code)
  }

  ngAfterViewChecked() {
    //   if(this.wrongLink) {
    //     setTimeout(this.goRegist(), 5000);
    //   } else if(!this.wrongLink) {
    //     setTimeout(this.goLogin(), 5000);
    //   }
  }

  // goLogin() {
  //   this.router.navigateByUrl('/login');
  // }

  // goRegist() {
  //   this.router.navigateByUrl('/registration');
  // }

}
