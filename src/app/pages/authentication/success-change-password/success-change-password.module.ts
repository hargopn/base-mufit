import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SuccessChangePasswordComponent } from './success-change-password.component';
import {RouterModule, Routes} from '@angular/router';
import {SharedModule} from '../../../shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

export const SuccessChangePasswordRoutes: Routes = [
  {
    path: '',
    component: SuccessChangePasswordComponent,
    data: {
      breadcrumb: 'Ganti Kata Sandi'
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(SuccessChangePasswordRoutes),
    SharedModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [SuccessChangePasswordComponent]
})
export class SuccessChangePasswordModule { }
