import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {SharedModule} from '../../shared/shared.module';
import { AdminComponent } from './admin.component';

export const AdminRoutes: Routes = [
  {
    path: '',
    data: {
      breadcrumb: '',
      status: false
    },
    children: [
      {
        path: 'edit/:id',
        loadChildren: './admin-edit/admin-edit.module#AdminEditModule'
      },
      {
        path: 'list',
        loadChildren: './admin-list/admin-list.module#AdminListModule'
      },
      {
        path: 'detail/:id',
        loadChildren: './admin-detail/admin-detail.module#AdminDetailModule'
      }
    ]
  }
];
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AdminRoutes),
    SharedModule
  ],
  declarations: [AdminComponent]
})
export class AdminModule { }
