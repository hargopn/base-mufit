import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {HttpModule} from '@angular/http';
import {SharedModule} from '../../../shared/shared.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CurrencyMaskModule} from 'ng2-currency-mask';
import {BrowserModule} from '@angular/platform-browser';
import {AdminEditComponent} from './admin-edit.component'

import { AdminService } from '../../../services/admin.service';

export const AdminEditRoutes: Routes = [
  {
    path: '',
    component:AdminEditComponent,
    data: {
      breadcrumb: 'Edit Admin',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AdminEditRoutes),
    FormsModule,
    HttpModule,
    SharedModule,
    CurrencyMaskModule,
    ReactiveFormsModule
  ],
  declarations: [AdminEditComponent],
  providers: [AdminService],
  entryComponents: [],
  exports: [],
  bootstrap: []
})
export class AdminEditModule { }
