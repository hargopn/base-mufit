import { AdminService } from "./../../../services/admin.service";
import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgModel, NgForm, FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SnotifyService, SnotifyPosition, SnotifyToastConfig } from 'ng-snotify';




@Component({
  selector: 'app-admin-edit',
  templateUrl: './admin-edit.component.html',
  styleUrls: ['./admin-edit.component.css']
})
export class AdminEditComponent implements OnInit {

  timeout = 3000;
  newTop = true;
  dockMax = 4;
  blockMax = 3;
  titleMaxLength = 30;
  bodyMaxLength = 100;
  public data: any;
  public fileKtp : any;
  public fileNpwp: any;
  public trainer: any;
  public admin =
    {
  "id":"",
  "version":"",
  "address": "",
  "email": "",
  "full_name": "",
  "group": {
    "accessLevel": "",
    "id": "",
    "name": "",
    "privilegeList": [
      {
        "description": "",
        "id": "",
        "name": "",
        "version": ""
      }
    ],
    "version": ""
  },
  "lastUpdatedDate": "",
  "no_ktp": "",
  "no_npwp": "",
  "password": "",
  "url_ktp": "",
  "url_npwp": "",
  "username": ""
}

  constructor(
    private service: AdminService,
    private route: ActivatedRoute,
    private snotifyService: SnotifyService,
    private router: Router,
    private formBuilder: FormBuilder,
  ) {}

  adminForm = new FormGroup({
    full_name: new FormControl('', [
      Validators.required,
      Validators.minLength(3),
      Validators.maxLength(50),
      Validators.pattern(/^[a-z A-Z]*$/)
    ]),
    email: new FormControl('', [
      // Validators.required,
      // Validators.pattern(/[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,5}$/)
    ]),
    address: new FormControl('', [
      Validators.required,
      Validators.minLength(15),
      Validators.maxLength(50)
    ]),
    no_ktp: new FormControl('', [
      Validators.required,
      Validators.minLength(16),
      Validators.maxLength(16),
      Validators.pattern(/^[0-9]*$/)
    ]),
    no_npwp: new FormControl('', [
      Validators.minLength(15),
      Validators.maxLength(15),
      Validators.pattern(/^[0-9]*$/)
    ]),
    url_ktp: new FormControl('', [
      // Validators.required
    ]),
    url_npwp: new FormControl('', [
      // Validators.required
    ])
  })

  ngOnInit() {
    window.scrollTo(0,0);
    this.getAdmin();
  }

  getAdmin(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.service.getAdminData(id).subscribe( data=> {
        if(data.message != 'ERROR'){
          this.admin=data.result;
          if(this.admin.url_ktp=='' || this.admin.url_ktp == null){
            this.admin.url_ktp = 'https://file.mufit.id/trainer/2dded528-779f-4ab6-9804-283611af19de.png';
          }
          if(this.admin.url_npwp=='' || this.admin.url_npwp==null){
            this.admin.url_npwp = 'https://file.mufit.id/trainer/2dded528-779f-4ab6-9804-283611af19de.png';
          }
        }else{
          this.snotifyService.error(data.result, this.getConfig());
        }
      })
  }

  getConfig(): SnotifyToastConfig {
    this.snotifyService.setDefaults({
      global: {
        newOnTop: this.newTop,
        maxAtPosition: this.blockMax,
        maxOnScreen: this.dockMax,
      }
    });
    return {
      bodyMaxLength: this.bodyMaxLength,
      titleMaxLength: this.titleMaxLength,
      backdrop: -1,
      position: SnotifyPosition.centerTop,
      timeout: this.timeout,
      showProgressBar: false,
      closeOnClick: true,
      pauseOnHover: false
    };
  }

  cancel(){
    const id = this.route.snapshot.paramMap.get('id');
    this.router.navigate(['/admin/detail/'+id])
  }

  checkFileExtKtp() {
    let name = this.fileKtp.name.split(".");
    let ext = name[name.length - 1];
    if(!(ext == 'jpg' || ext == 'JPG' || ext == 'png' || ext == 'PNG' || ext == 'jpeg' || ext == 'JPEG')) {
      return true;
    } else {
      return false;
    }
  }

  checkFileExtNpwp() {
    let name = this.fileNpwp.name.split(".");
    let ext = name[name.length - 1];
    if(!(ext == 'jpg' || ext == 'JPG' || ext == 'png' || ext == 'PNG' || ext == 'jpeg' || ext == 'JPEG')) {
      return true;
    } else {
      return false;
    }
  }

  uploadFileKtp(event){
    this.fileKtp = event.target.files[0];
    if(this.checkFileExtKtp()){
      this.snotifyService.error('Image must be in .jpg / .png / .jpeg', this.getConfig());
      this.fileKtp = null;
    }else{
      let fd = new FormData();
      fd.append('file', this.fileKtp);
      this.service.uploadImg(fd).subscribe( post => {
        if(post.message != 'ERROR'){
          this.admin.url_ktp = post["result"];
        }else{
          this.snotifyService.error(post.result, this.getConfig());
        }
      });
    }
  }

  uploadFileNpwp(event){
    this.fileNpwp = event.target.files[0];
    if(this.checkFileExtNpwp()){
      this.snotifyService.error('Image must be in .jpg / .png / .jpeg', this.getConfig());
      this.fileNpwp = null;
    }else{
      let fd = new FormData();
      fd.append('file', this.fileNpwp);
      this.service.uploadImg(fd).subscribe( post => {
        if(post.message != 'ERROR'){
          this.admin.url_npwp = post["result"];
        }else{
          this.snotifyService.error(post.result, this.getConfig());
        }
      });
    }
  }

  updateAdmin(){
    let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    let x = /^[a-z A-z]*$/

    if (this.admin.full_name ==''){
      this.snotifyService.error('Please Input Admin Name', this.getConfig());
    }else if (!this.admin.full_name.match(x)){
      this.snotifyService.error('Admin Name Must Be In Characters', this.getConfig());
    }else if (this.admin.full_name.length < 3 || this.admin.full_name.length > 50){
      this.snotifyService.error('Admin Name Must Be Between 3 - 50 Characters', this.getConfig());
    }else if (this.admin.address == ''){
      this.snotifyService.error('Please Input Admin Address', this.getConfig());
    }else if(this.admin.address.length  < 15){
          this.snotifyService.error('Trainer Admin is too Short',this.getConfig());
    }else if(this.admin.address.length  > 50){
          this.snotifyService.error('Trainer Admin is too Long',this.getConfig());
    }else if(this.admin.no_ktp == ''){
          this.snotifyService.error('Please Input KTP Trainer',this.getConfig());
    }else if(this.admin.no_ktp.length < 16){
          this.snotifyService.error('No KTP Must 16 Digit',this.getConfig());
    }else if(this.admin.no_ktp.length > 16){
          this.snotifyService.error('No KTP Must 16 Digit',this.getConfig());
    }else if(this.admin.no_npwp.length != 15 && this.admin.no_npwp.length != 0){
          this.snotifyService.error('No NPWP Must 15 Digit',this.getConfig());
    }else{
      let request =
      {
        "address": this.admin.address,
        "full_name": this.admin.full_name,
        "no_ktp": this.admin.no_ktp,
        "no_npwp": this.admin.no_npwp,
        "id": this.admin.id,
        "version": this.admin.version,
        "url_ktp": this.admin.url_ktp,
        "url_npwp": this.admin.url_npwp
      }
      console.log(request)
      const id = this.route.snapshot.paramMap.get('id');
      this.service.updateAdmin(request,id).subscribe(data =>{
        if(data.message != ' ERROR'){
          this.snotifyService.success('Update Admin Success!', this.getConfig());
          setTimeout(() => {
            const id = this.route.snapshot.paramMap.get('id');
            this.router.navigate(['/admin/detail/'+id])
          }, 4000);
        }else{
          this.snotifyService.error(data.result, this.getConfig());
        }
      })

    }
  }


}
