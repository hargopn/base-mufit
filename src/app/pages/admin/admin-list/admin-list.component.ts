import { Admin } from './../../../class/admin';
import { AdminService } from './../../../services/admin.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-admin-list',
  templateUrl: './admin-list.component.html',
  styleUrls: ['./admin-list.component.css']
})
export class AdminListComponent implements OnInit {

  public page: number = 1; //page
  public limit: number = 25; //itemsPerPage
  public totalItems: number; //elements

  public search="";

  public previousPage: any;

  admins : Admin[];

  constructor(private adminService: AdminService) { }

  ngOnInit() {
    window.scrollTo(0,0);
    this.loadData();
  }

  loadData() {
    this.adminService.getPaginate(this.page - 1, this.limit, this.search).subscribe(posts => {
      this.admins = posts["result"];
      this.totalItems = posts["elements"];
      console.log(this.admins, this.totalItems);
    });
  }

  loadPage(page: number) {
    if (page !== this.previousPage) {
      this.previousPage = page;
      this.loadData();
    }
  }

  //Show limit list
  getSelect(limitnya) {
    this.adminService.getPaginate(this.page, limitnya, this.search).subscribe(posts => {
      this.limit = limitnya;
      this.loadData();
    });
  }

  onChange(deviceValue) {
    this.getSelect(deviceValue);
  }

  //Search
  getSearch(query){
    this.search = query;
    this.loadData();
  }

}
