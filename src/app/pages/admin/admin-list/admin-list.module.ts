import { AdminService } from './../../../services/admin.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {RouterModule, Routes} from '@angular/router';
import {SharedModule} from '../../../shared/shared.module';

import {DataTableModule} from 'angular2-datatable';
import {FormsModule} from '@angular/forms';
import { AdminListComponent } from './admin-list.component';

export const AdminListRoutes: Routes = [
  {
    path: '',
    component: AdminListComponent,
      data: {
    breadcrumb: 'Booking List',
    icon: 'icofont-social-blogger bg-c-green',
      status: false
  } }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AdminListRoutes),
    SharedModule,
    FormsModule,
  ],
  declarations: [AdminListComponent],
  providers: [AdminService]
})
export class AdminListModule { }
