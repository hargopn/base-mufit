import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-admin',
  template: '<router-outlet><app-spinner></app-spinner></router-outlet>'

})
export class AdminComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
