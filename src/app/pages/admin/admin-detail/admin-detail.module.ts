import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {HttpModule} from '@angular/http';
import {SharedModule} from '../../../shared/shared.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CurrencyMaskModule} from 'ng2-currency-mask';
import {BrowserModule} from '@angular/platform-browser';
import {AdminDetailComponent} from './admin-detail.component'

import { AdminService } from '../../../services/admin.service';

export const AdminDetailRoutes: Routes = [
  {
    path: '',
    component:AdminDetailComponent,
    data: {
      breadcrumb: 'Detail Admin',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AdminDetailRoutes),
    FormsModule,
    HttpModule,
    SharedModule,
    CurrencyMaskModule,
    ReactiveFormsModule
  ],
  declarations: [AdminDetailComponent],
  providers: [AdminService],
  entryComponents: [],
  exports: [],
  bootstrap: []
})
export class AdminDetailModule { }
