import { AdminService } from "./../../../services/admin.service";
import { Component, OnInit, Input } from "@angular/core";
import { ActivatedRoute, Router } from '@angular/router';
import { NgModel, NgForm } from '@angular/forms';
import { SnotifyService, SnotifyPosition, SnotifyToastConfig } from 'ng-snotify';



@Component({
  selector: 'app-admin-detail',
  templateUrl: './admin-detail.component.html',
  styleUrls: ['./admin-detail.component.css']
})
export class AdminDetailComponent implements OnInit {

  timeout = 3000;
  newTop = true;
  dockMax = 4;
  blockMax = 3;
  titleMaxLength = 30;
  bodyMaxLength = 100;
  public data: any;

  public admin =
    {
  "address": "",
  "email": "",
  "full_name": "",
  "group": {
    "accessLevel": "",
    "id": "",
    "name": "",
    "privilegeList": [
      {
        "description": "",
        "id": "",
        "name": "",
        "version": ""
      }
    ],
    "version": ""
  },
  "lastUpdatedDate": "",
  "no_ktp": "",
  "no_npwp": "",
  "password": "",
  "url_ktp": "",
  "url_npwp": "",
  "username": ""
}



  constructor(
    private service: AdminService,
    private route: ActivatedRoute,
    private snotifyService: SnotifyService,
    private router: Router
  ) { }

  ngOnInit() {
    window.scrollTo(0,0);
    this.getAdmin();

  }

  getAdmin(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.service.getAdminData(id).subscribe( data=> {
        if(data.message != 'ERROR'){
          this.admin=data.result;
          if(this.admin.url_ktp=='' || this.admin.url_ktp ==null){
            console.log(this.admin.url_ktp)
            this.admin.url_ktp = 'https://file.mufit.id/trainer/2dded528-779f-4ab6-9804-283611af19de.png';
          }
          if(this.admin.url_npwp=='' ||this.admin.url_npwp==null){
            this.admin.url_npwp = 'https://file.mufit.id/trainer/2dded528-779f-4ab6-9804-283611af19de.png';
          }
        }else{
          this.snotifyService.error(data.result, this.getConfig());
        }
      })
  }

  editDetail(){
    const id = this.route.snapshot.paramMap.get('id');
    this.router.navigate(['/admin/edit/'+id])
  }

  getConfig(): SnotifyToastConfig {
    this.snotifyService.setDefaults({
      global: {
        newOnTop: this.newTop,
        maxAtPosition: this.blockMax,
        maxOnScreen: this.dockMax,
      }
    });
    return {
      bodyMaxLength: this.bodyMaxLength,
      titleMaxLength: this.titleMaxLength,
      backdrop: -1,
      position: SnotifyPosition.centerTop,
      timeout: this.timeout,
      showProgressBar: false,
      closeOnClick: true,
      pauseOnHover: false
    };

  }


}
