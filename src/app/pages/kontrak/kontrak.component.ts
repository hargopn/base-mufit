import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-kontrak',
  templateUrl: './kontrak.component.html',
  styleUrls: ['./kontrak.component.css']
})
export class KontrakComponent implements OnInit {

  step2: any = {
    showNext: true,
    showPrev: true
  };

  step3: any = {
    showSecret: false
  };

  isCompleted = false;
  type:String;
  omset: String[];
  jenisKontrak: String[];
  bidangIndustri: String[];
  nilaiPembayaran: String[];
  caraPembayaran: String[];
  jenisKontrakSelect: String;
  omsetSelect: String;
  bidangIndustriSelect: String;
  nilaiPembayaranSelect: String;
  caraPembayaranSelect: String;
  create: String;
  review: String;
  constructor() {
    this.omset = [
      "0 s/d Rp250 Juta",
      "Rp251 Juta s/d Rp750 Juta",
      "Diatas Rp750 Juta"
    ];
    this.jenisKontrak = [
      "Perjanjian Kerja Sama",
      "Perjanjian Kerahasiaan",
      "Perjanjian Kerja",
      "Perjanjian Investasi",
      "Syarat dan Ketentuan",
      "Kebijakan Privasi",
      "Perjanjian Pemegang Saham",
      "Lain-lain"
    ];
    this.bidangIndustri = [
      "Kuliner",
      "Fashion",
      "Otomotif",
      "Agrobisnis",
      "Teknologi Internet",
      "Jasa",
      "Media",
      "Periklanan dan Pemasaran",
      "Lain-lain"
    ];
    this.nilaiPembayaran = [
      "< 1 Milyar",
      "> 1 Milyar"
    ];
    this.caraPembayaran = [
      "Transfer ATM",
      "Cash"
    ];
    this.caraPembayaranSelect = this.caraPembayaran[0];
    this.nilaiPembayaranSelect = this.nilaiPembayaran[0];
    this.bidangIndustriSelect = this.bidangIndustri[0];
    this.jenisKontrakSelect = this.jenisKontrak[0];
    this.omsetSelect = this.omset[0];
  }
  
  ngOnInit() {
  
  }

  onStep1Next(event) {

  }

  onStep2Next(event) {

  }

  onStep3Next(event) {

  }
  
  onStepChanged(step) {

  }
}
