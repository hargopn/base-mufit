import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { KontrakComponent } from './kontrak.component';

import {RouterModule, Routes} from '@angular/router';
import {SharedModule} from '../../shared/shared.module';

import {FormsModule} from '@angular/forms';

import {ScrollToModule} from '@nicky-lenaers/ngx-scroll-to';
import {FormWizardModule} from 'angular2-wizard';

export const kontrakRoutes: Routes = [
  {
    path: '',
    component: KontrakComponent,
    data: {
      breadcrumb: 'Buat Kontrak'
  } }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(kontrakRoutes),
    SharedModule,
    FormsModule,
    FormWizardModule
  ],
  declarations: [KontrakComponent]
})
export class KontrakModule { }