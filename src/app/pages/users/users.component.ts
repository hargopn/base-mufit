import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-users',
  template: '<router-outlet><app-spinner></app-spinner></router-outlet>'
})
export class UsersComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
