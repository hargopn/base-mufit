import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListUsersComponent } from './list-users.component';

import {RouterModule, Routes} from '@angular/router';
import {HttpModule} from '@angular/http';
import {SharedModule} from '../../../shared/shared.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CurrencyMaskModule} from 'ng2-currency-mask';
import {BrowserModule} from '@angular/platform-browser';

import { UserService } from '../../../services/user.service';
export const UserListRoutes: Routes = [
  {
    path: '',
    component:ListUsersComponent,
    data: {
      breadcrumb: 'Default',
      icon: 'icofont-home bg-c-blue',
      status: false
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(UserListRoutes),
    FormsModule,
    HttpModule,
    SharedModule,
    CurrencyMaskModule,
    ReactiveFormsModule
  ],
  declarations: [ListUsersComponent],
  providers: [UserService],
  entryComponents: [],
  exports: [],
  bootstrap: []
})
export class ListUsersModule { }
