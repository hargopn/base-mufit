import { User } from './../../../class/user';
import { UserService } from "./../../../services/user.service";
import { Component, OnInit, Input } from "@angular/core";
import * as moment from 'moment';


@Component({
  selector: 'app-list-users',
  templateUrl: './list-users.component.html',
  styleUrls: ['./list-users.component.css']
})
export class ListUsersComponent implements OnInit {

    public page: number = 1; //page
    public limit: number = 25; //itemsPerPage
    public totalItems: number; //elements

    public search="";

    public previousPage: any;

    user: User[];
    constructor(private UserService: UserService) { }

    ngOnInit() {
    window.scrollTo(0,0);
      this.loadData();
    }

    loadData() {
      this.UserService.getPaginate(this.page - 1, this.limit, this.search).subscribe(posts => {
        this.user = posts["result"];
        this.totalItems = posts["elements"];
        for(let i=0; i< this.user.length; i++){
        this.user[i].date_created = moment(this.user[i].date_created).format('LL');
      }
        console.log(this.user, this.totalItems);
      });
    }

    loadPage(page: number) {
      if (page !== this.previousPage) {
        this.previousPage = page;
        this.loadData();
      }
    }

    //Show limit list
    getSelect(limitnya) {
      this.UserService.getPaginate(this.page, limitnya, this.search).subscribe(posts => {
        this.limit = limitnya;
        this.loadData();
      });
    }

    onChange(deviceValue) {
      this.getSelect(deviceValue);
    }

    //Search
    getSearch(query){
      this.search = query;
      this.loadData();
    }

}
