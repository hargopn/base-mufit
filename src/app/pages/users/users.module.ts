import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {RouterModule, Routes} from '@angular/router';
import {SharedModule} from '../../shared/shared.module';
import { UserDetailComponent } from './user-detail/user-detail.component';

import { UsersComponent } from './users.component';
import { UserEditComponent } from './user-edit/user-edit.component';

export const UserRoutes: Routes = [
  {
    path: '',
    data: {
      breadcrumb: '',
      status: false
    },
    children: [
      {
        path: 'list',
        loadChildren: './list-users/list-users.module#ListUsersModule'
      },
      {
        path: 'detail/:id',
        loadChildren: './user-detail/user-detail.module#UserDetailModule'
      },
      {
        path: 'edit/:id',
        loadChildren: './user-edit/user-edit.module#UserEditModule'
      }
    ]
  }
];
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(UserRoutes),
    SharedModule
  ],
  declarations: [UsersComponent]
})
export class UsersModule { }
