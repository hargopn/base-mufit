import { UserService } from './../../../services/user.service';
import { Router, ActivatedRoute } from '@angular/router';
import { NgModel, NgForm } from '@angular/forms';
import { Component, OnInit, Input, ViewChild } from '@angular/core';
import * as moment from 'moment';
import { SnotifyService, SnotifyPosition, SnotifyToastConfig } from 'ng-snotify';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormGroup, FormControl, Validators } from '@angular/forms';
const URL = 'http://dev.nostratech.com:20024/api/image/upload';

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.css']
})
export class UserEditComponent implements OnInit {

  timeout = 3000;
  newTop = true;
  dockMax = 4;
  blockMax = 3;
  titleMaxLength = 30;
  bodyMaxLength = 100;

  public foto: any;
  public data: any;
  public user = {

      "id": "",
      "version": "",
      "email": "",
      "full_name": "",
      "address": "",
      "phone": "",
      "date_created": "",
      "status": "",
      "photo_selfie": ""

  }
  public since: any;

  constructor(
    private route: ActivatedRoute,
    private service: UserService,
    private snotifyService: SnotifyService,
    private router: Router,
  ) {
   }

  ngOnInit() {
    window.scrollTo(0,0);
    this.getAll();
  }

  getAll(){
    const id = this.route.snapshot.paramMap.get('id');

    this.service.getUserData(id)
      .subscribe( data=>{
        if(data.message != 'ERROR'){
          this.user=data.result;
          this.user.date_created = moment(this.user.date_created).format('LL');
          if(this.user.photo_selfie==''||this.user.photo_selfie==null){
            this.user.photo_selfie= 'https://file.mufit.id/trainer/2dded528-779f-4ab6-9804-283611af19de.png';
          }
        }else{
          this.snotifyService.error(data.result,this.getConfig());
        }

      });
  }

  getConfig(): SnotifyToastConfig {
    this.snotifyService.setDefaults({
      global: {
        newOnTop: this.newTop,
        maxAtPosition: this.blockMax,
        maxOnScreen: this.dockMax,
      }
    });
    return {
      bodyMaxLength: this.bodyMaxLength,
      titleMaxLength: this.titleMaxLength,
      backdrop: -1,
      position: SnotifyPosition.centerTop,
      timeout: this.timeout,
      showProgressBar: false,
      closeOnClick: true,
      pauseOnHover: false
    };
  }

  clickCancel(){
    const id = this.route.snapshot.paramMap.get('id');

    this.router.navigate(['/users/detail/' + id]);
  }

  checkFileExt() {
    let name = this.foto.name.split(".");
    let ext = name[name.length - 1];
    if(!(ext == 'jpg' || ext == 'JPG' || ext == 'png' || ext == 'PNG' || ext == 'jpeg' || ext == 'JPEG')) {
      return true;
    } else {
      return false;
    }
  }

  uploadFileCust(event){
    this.foto = event.target.files[0];
    if(this.checkFileExt()){
      this.snotifyService.error('Image must be in .jpg / .png / .jpeg', this.getConfig());
      this.foto = null;
    }else{
      let fd = new FormData();
      fd.append('file', this.foto);
      this.service.uploadImg(fd).subscribe( post => {
        if(post.message != 'ERROR'){
          this.user.photo_selfie = post["result"];
        }else{
          this.snotifyService.error(post.result, this.getConfig());
        }
      });
    }
  }

  updateUser(){
    const id = this.route.snapshot.paramMap.get('id');

    let request =
      {

        "id": this.user.id,
        "version": this.user.version,
        "email": this.user.email,
        "full_name": this.user.full_name,
        "address": this.user.address,
        "phone": this.user.phone,
        "date_created": this.user.date_created,
        "photo_selfie": this.user.photo_selfie

      }
      let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      let x = /^[a-z A-Z]+$/;

    if (this.user.full_name == '') {
      this.snotifyService.error('Please Input Customer Name', this.getConfig());
    } else if(!this.user.full_name.match(x) ) {
      this.snotifyService.error('Customer Name Must Be In Alphabet', this.getConfig());
    } else if (this.user.full_name.length < 3 || this.user.full_name.length > 50) {
      this.snotifyService.error('Customer Name Must Be Between 3 - 50 Characters', this.getConfig());
    } else if (this.user.email == '') {
      this.snotifyService.error('Please Input Customer Email', this.getConfig());
    } else if (!re.test(String(this.user.email).toLowerCase())) {
      this.snotifyService.error('Please Input Valid Email', this.getConfig());
    } else if (this.user.address == '') {
      this.snotifyService.error('Please Input Customer Address', this.getConfig());
    } else if (this.user.address.length < 15) {
      this.snotifyService.error('Customer Address Must Be Between 15 - 50 Characters', this.getConfig());
    } else if (this.user.address.length > 50) {
      this.snotifyService.error('Customer Address Must Be Between 15 - 50 Characters', this.getConfig());
    } else if (this.user.phone.length < 10 || this.user.phone.length > 14) {
      this.snotifyService.error('Phone Number Must Be Between 10 - 14 Characters', this.getConfig());
    } else {
      this.service.updateUser(request, id).subscribe(posts=>{
        if (posts.message != 'ERROR') {
          this.snotifyService.success('Update Customer Success!', this.getConfig());
          setTimeout(() => {
            this.router.navigate(['/users/list'])
          }, 4000);
          // this.router.navigateByUrl('/dashboard');
        } else {
          this.snotifyService.error(posts.result, this.getConfig());
        }
      });
    }
  }

  formUserEdit = new FormGroup({
    fullName : new FormControl('', [
      Validators.required,
      Validators.minLength(3),
      Validators.maxLength(50),
      Validators.pattern(/^[a-z A-Z]*$/)
    ]),
    address : new FormControl('', [
      Validators.required,
      Validators.minLength(15),
      Validators.maxLength(50)
    ]),
    phone : new FormControl('', [
      Validators.required,
      Validators.minLength(10),
      Validators.maxLength(13),
      Validators.pattern('[0][8][0-9]{8,11}')
    ])
  })

  get fullName(){
    return this.formUserEdit.get('fullName');
  }

  get address(){
    return this.formUserEdit.get('address');
  }

  get phone(){
    return this.formUserEdit.get('phone');
  }

}
