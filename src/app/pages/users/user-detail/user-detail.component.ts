// import { User } from './../user';
import { UserService } from "./../../../services/user.service";
import { Component, OnInit, Input } from "@angular/core";
import { ActivatedRoute, Router } from '@angular/router';
import * as moment from 'moment';
import { SnotifyService, SnotifyPosition, SnotifyToastConfig } from 'ng-snotify';


@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.css']
})
export class UserDetailComponent implements OnInit {

  timeout = 3000;
  newTop = true;
  dockMax = 4;
  blockMax = 3;
  titleMaxLength = 30;
  bodyMaxLength = 100;
  public data: any;
  public user = {

            "id": "",
            "version": "",
            "email": "",
            "full_name": "",
            "address": "",
            "phone": "",
            "date_created": "",
            "photo_selfie": "",
            "status":""

  }
  public since: any;

  constructor(
    private service: UserService,
    private route: ActivatedRoute,
    private snotifyService: SnotifyService,
    private router: Router

  ) { }

  ngOnInit() {
    window.scrollTo(0,0);
    this.getAll();
  }

  getAll(){
    const id = this.route.snapshot.paramMap.get('id');
    this.service.getUserData(id)
      .subscribe( data=>{
        if(data.message != 'ERROR'){
          this.user=data.result;
          this.user.date_created = moment(this.user.date_created).format('LL');
          if(this.user.photo_selfie==''||this.user.photo_selfie==null){
            this.user.photo_selfie= 'https://file.mufit.id/trainer/2dded528-779f-4ab6-9804-283611af19de.png';
          }
          
        }else{
          this.snotifyService.error(data.result,this.getConfig());
        }

      });
  }

  editDetail(){
    const id = this.route.snapshot.paramMap.get('id');
    this.router.navigate(['/users/edit/'+id])
  }

  getConfig(): SnotifyToastConfig {
    this.snotifyService.setDefaults({
      global: {
        newOnTop: this.newTop,
        maxAtPosition: this.blockMax,
        maxOnScreen: this.dockMax,
      }
    });
    return {
      bodyMaxLength: this.bodyMaxLength,
      titleMaxLength: this.titleMaxLength,
      backdrop: -1,
      position: SnotifyPosition.centerTop,
      timeout: this.timeout,
      showProgressBar: false,
      closeOnClick: true,
      pauseOnHover: false
    };

  }

}
