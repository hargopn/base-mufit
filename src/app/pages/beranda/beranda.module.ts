import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BerandaComponent } from './beranda.component';
import {RouterModule, Routes} from '@angular/router';
import {SharedModule} from '../../shared/shared.module';
import {DataTableModule} from 'angular2-datatable';
import {FormsModule} from '@angular/forms';
import {ScrollToModule} from '@nicky-lenaers/ngx-scroll-to';
import {AngularEchartsModule} from 'ngx-echarts';
import { HttpClientModule } from '@angular/common/http';
import { BerandaService } from '../../services/beranda.service';

export const berandaRoutes: Routes = [
  {
    path: '',
    component: BerandaComponent,
      data: {
    breadcrumb: 'Beranda',
    icon: 'icofont-social-blogger bg-c-green',
      status: false
  } }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(berandaRoutes),
    SharedModule,
    FormsModule,
    DataTableModule,
    AngularEchartsModule,
    HttpClientModule
  ],
  declarations: [BerandaComponent],
  providers: [BerandaService]
})
export class BerandaModule { }
