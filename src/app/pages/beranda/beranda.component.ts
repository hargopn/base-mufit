import { Component, OnInit, Injectable } from '@angular/core';
import { CallService } from '../../services/base-services/call.service';
import { BerandaService } from '../../services/beranda.service';
import {OtherDatatableModule} from '../ui-elements/tables/data-table/other-datatable/other-datatable.module';
import { AppError } from '../common/app-error';
import { NotFoundError } from '../common/not-found-error';
import { BadInput } from '../common/bad-input';
import { Chart } from 'chart.js';

import 'd3';
import * as c3 from 'c3';
import 'rxjs/add/operator/map'


@Component({
  selector: 'app-beranda',
  templateUrl: './beranda.component.html',
  styleUrls: ['./beranda.component.css']
})
export class BerandaComponent implements OnInit {

  chart=[];
  public sales_time: any = "daily";
  formDashboard = {
    "activeCustomer": "1000",
    "totalCustomer": "10000",
    "activeTrainer": "500000",
    "totalTrainer": "1000000",
    "totalBooking": "5000000",
    "totalIncome": "5000000000000"
  }

  constructor(
    private service: BerandaService
  ) { }

  ngOnInit() {
    this.getChart();
  }

  getSelect(sales_timex) {
    // this.service.getBeranda(sales_timex).subscribe(posts => {
    //   this.sales_time = sales_timex;
    //   this.getChart();
    // });
  }

  onChange(deviceValue) {
    // this.getSelect(deviceValue);
  }

  getChart(){
    // this.service.getBeranda()
    //   .subscribe(post => {
    //
    //     let sales = post['list'].map(post => post.main.sales)
    //
    //
    //     let time_sales = []
    //     alldates.forEach((post) => {
    //       let jsdate = new Date(res * 1000)
    //       time_sales.push(jsdate.toLocaleTimeString('en', { year: 'numeric', month: 'short', day: 'numeric'}))
    //     })
    //
        // this.chart = new Chart('canvas', {
        //   type: 'line',
        //   data: {
        //     labels: time_sales,
        //     datasets: [
        //       {
        //         data: sales,
        //         borderColor: '#3cba9f',
        //         fill: false
        //       },
        //     ]
        //   },
        //   options: {
        //     legend: {
        //       display: false
        //     },
        //     scales: {
        //       xAxes: [{
        //         display: true
        //       }],
        //       yAxes: [{
        //         display: true
        //       }]
        //     }
        //   }
        // })

        this.chart = new Chart('canvas', {
          type: 'line',
          data: {
            labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
            datasets: [
              {
                data: [500, 200, 123, 500, 400, 500, 121, 111],
                borderColor: '#3cba9f',
                fill: false
              },
              {
                data: [200, 123, 500, 400, 500, 121, 111, 555],
                borderColor: '#ffcc00',
                fill: false
              },
            ]
          },
          options: {
            legend: {
              display: false
            },
            scales: {
              xAxes: [{
                display: true
              }],
              yAxes: [{
                display: true
              }]
            }
          }
        })
      // })
  }


}
