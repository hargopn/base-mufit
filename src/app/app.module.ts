import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {RouterModule} from '@angular/router';
import { FormsModule } from '@angular/forms';

import {AppRoutes} from './app.routing';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { AdminComponent } from './layout/admin/admin.component';
import {ClickOutsideModule} from 'ng-click-outside';
import {SharedModule} from './shared/shared.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {BreadcrumbsComponent} from './layout/admin/breadcrumbs/breadcrumbs.component';
import {TitleComponent} from './layout/admin/title/title.component';
import {AuthComponent} from './layout/auth/auth.component';
import {CallService} from './services/base-services/call.service';
import {AuthService} from './services/base-services/auth.service';
import {AuthenticationService} from './services/authentication.service';
import {AuthGuardService} from './services/base-services/auth-guard.service';
import {Ng2Webstorage} from 'ngx-webstorage';
import {UserService} from './services/user.service';
import { BookingService } from './services/booking.service';
// import {LocalStorageService, SessionStorageService} from 'ngx-webstorage';


@NgModule({
  declarations: [
    AppComponent,
    AdminComponent,
    BreadcrumbsComponent,
    TitleComponent,
    AuthComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(AppRoutes),
    ClickOutsideModule,
    SharedModule,
    HttpModule,
    FormsModule,
    Ng2Webstorage
  ],
  providers: [CallService,AuthService,AuthGuardService,AuthenticationService,UserService,BookingService],
  bootstrap: [AppComponent]
})
export class AppModule { }
