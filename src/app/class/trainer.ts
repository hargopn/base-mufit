export class Trainer {
    
    
    address: string;
    email: string;
    gender: string;
    name: string;
    noKtp: string;
    noNpwp: string;
    phone: string;
    day: string;
    endTime: string;
    startTime: string;
    price:0;
    shift_list: [{}];
    // scheduleList: [
    //     {
    //       createShiftVOS: [
    //         {
    //           endTime: 0,
    //           startTime: 0
    //         }
    //       ],
    //       day: string
    //     }
    //   ];
    // specialityList: [
    //     {
    //     id: string,
    //     price: 0
    //     }
    // ]
    id: string;
    // version: null;
    speciality: string;
}
