export class User{

  name: string;
  id: string;
  full_name: string;
  email:string;
  phone:number;
  address:string;
  date_created:string;
  status:string;
  review:number;

}
