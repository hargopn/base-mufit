export class Voucher{

    id:any;
    code: string;
    description: string;
    end_date: string;
    start_date:string;
    quantity:number;
    type:string;
    value:string;
  }