export class Rating {
    id: string;
    name_trainer: string;
    email_trainer: string;
    rating: any;
    review: string;
    url_photo_trainer: string;
}