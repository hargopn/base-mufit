export class Detail{
    id:string;
    version:number;
    name:string;
    email:string;
    gender:string;
    address:string;
    phone:string;
    no_ktp:string;
    no_npwp:string;
    no_rek:string;
    url_photo_trainer:string;
    trainer_specialitys:string;

    day: {
        id: string,
        version: number,
        name: string,
        shift: [
          {
            id: string,
            version: number,
            start_time: {
              hour: number,
              minute: number,
              second: number
            },
            end_time: {
              hour: number,
              minute: number,
              second: number
            },
            shift_range: number,
            status: boolean
          }
        ]
      }

    /* day: [
        {
            id: string,
            name: string,
                shift: [
                    {
                        id: string,
                        start_time: {
                            hour: number,
                            minute: number,
                            second: number
                        },
                        end_time: {
                            hour: number,
                            minute: number,
                            second: number
                        },
                        shift_range : number;
                    }
                ]

        }
    ] */
    trainer_speciality: [
        {
            id:string;
            version:string;
            price:number;
            name:string;
            counter:number;
            value:boolean;
            totalPrice:number;
            quantity:number;
        }
    ]
    
}