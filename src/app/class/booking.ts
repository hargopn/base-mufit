export class Booking{
    id : string;
    version : number;
    customer_name : string;
    date_booking : string;
    customer_phone : string;
    address : string;
    class_booking : string;
    bill : string;
}