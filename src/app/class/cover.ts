export class Cover{
    id : string;
    version : number;
    header : string;
    url_cover : string;
    description : string;
}